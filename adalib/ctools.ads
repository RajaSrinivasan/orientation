package ctools is

    function Image( value : integer ; format : string ) return String ;
    function Image( value : float ; format : string ) return String ;

    procedure sprintf( str : in out string ;
                         format : string ;
                         value : integer ;
                         charswritten : out natural );

    procedure sprintf( str : in out string ;
                       format : string ;
                       value : float ;
                       charswritten : out natural );

    procedure sprintf( str : in out string ;
                       format : string ;
                       value : string ;
                       charswritten : out natural );

end ctools ;
