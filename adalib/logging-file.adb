with gnat.calendar ;

package body logging.file is

  function GenerateFileName ( name : string ) return String is
    Year : ada.calendar.Year_Number ;
    Month : ada.calendar.Month_Number ;
    Day : ada.calendar.Day_Number ;
    Hour : gnat.calendar.hour_number ;
    Minute : gnat.calendar.minute_number ;
    second : gnat.calendar.second_number ;
    sub_second : gnat.calendar.second_duration ;
  begin
    gnat.calendar.split( ada.calendar.clock , Year,Month,Day,Hour,Minute,Second,Sub_Second);
    return name & "_" &
           numimages( integer(month)) &
           numimages( integer(day)) &
           numimages( integer(hour) ) &
           ".log";
  end GenerateFileName ;

    function Create( name : string ) return Destination_Access_Type is
      newdest : FileDestination_Access_Type := new FileDestination_Type ;
    begin
      newdest.logname := to_unbounded_string( name );
      newdest.currentfile := new Ada.Text_Io.File_Type ;
      declare
        logfilename : string := GenerateFileName(name);
      begin
        Ada.Text_Io.Create( newdest.currentfile.all , Ada.Text_Io.Out_File , logfilename );
      end ;
      return Destination_Access_Type(newdest) ;
    end Create ;

    procedure Send( message : Log_Message_Type ;
                    destination : FileDestination_Type ) is
    begin
      Put( destination.currentfile.all , Image( message.ts )); Put(destination.currentfile.all , " ");
      Put( destination.currentfile.all , message.Source ); Put(destination.currentfile.all , ":");
      Put( destination.currentfile.all , message.class ); Put(destination.currentfile.all , ":") ;
      Put( destination.currentfile.all , Message_Level_Type'Image( message.level )); Put(destination.currentfile.all , " ");
      Put_Line( destination.currentfile.all , to_string(message.txt) );
      Flush( destination.currentfile.all ) ;
    end Send ;

    overriding
    procedure Finalize( Object : in out FileDestination_Type ) is
    begin
      Ada.Text_Io.Close( Object.currentfile.all );
    end Finalize ;

    overriding
    procedure PeriodicCleanup( destination : FileDestination_Type ) is
    begin
      Ada.Text_Io.Close( destination.currentfile.all );
      declare
        newlogfilename : string := GenerateFileName( to_string(destination.logname) );
      begin
        Ada.Text_Io.Create( destination.currentfile.all ,
                            Ada.Text_Io.Out_File ,
                            newlogfilename );
      end ;
    end PeriodicCleanup ;

end logging.file ;
