with ada.strings.unbounded ; use ada.strings.unbounded ;
with ada.Text_Io; use Ada.Text_Io;

package logging.file is

  type FileDestination_Type is new Destination_Type with
       record
           logname : unbounded_string ;
           currentfile : access ada.text_io.File_Type ;
       end record ;
  overriding
  procedure Finalize( Object : in out FileDestination_Type );
  type FileDestination_Access_Type is access all FileDestination_Type ;
  function Create( name : string ) return Destination_Access_Type ;
  procedure Send( message : Log_Message_Type ;
                  destination : FileDestination_Type )  ;
  overriding
  procedure PeriodicCleanup( destination : FileDestination_Type );
  function GenerateFileName(name : string) return String ;

end logging.file ;
