generic
    type context_type is private ;
procedure dirwalk( context : context_type ;
                   dirname : string ;
                   pattern : string ;
                   Tree : Boolean ;
                   processor : access procedure ( context : context_type ;
                                                  filename : in string )
                   ) ;
