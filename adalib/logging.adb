with Ada.Strings.Fixed ;
with Ada.Real_Time ;
with Ada.Real_Time.Timing_Events ;
with gnat.calendar ;
with ctools ;
package body logging is


  function Image( ts : Ada.Calendar.Time ) return String is

    Year : ada.calendar.Year_Number ;
    Month : ada.calendar.Month_Number ;
    Day : ada.calendar.Day_Number ;
    Hour : gnat.calendar.hour_number ;
    Minute : gnat.calendar.minute_number ;
    second : gnat.calendar.second_number ;
    sub_second : gnat.calendar.second_duration ;

  begin
    gnat.calendar.split( ts , Year,Month,Day,Hour,Minute,Second,Sub_Second);
    return
                 ada.calendar.Year_Number'image( year ) & "-" &
                 numimages(integer(month)) & "-" &
                 numimages(integer(day)) & " " &
                 numimages(integer(hour)) & ":" &
                 numimages(integer(minute)) & ":" &
                 numimages(integer(second)) & "." &
                 ctools.image( integer( 1000.0 * sub_second ) , "%03d" );
  end Image ;

 Secondly : Ada.Real_Time.Time_Span ;
 Hourly : Ada.Real_Time.Time_Span ;

 EmptyMessageQueueEvent : Ada.Real_TIme.Timing_Events.Timing_Event ;
 PeriodicCleanupEvent : Ada.Real_Time.Timing_Events.Timing_Event ;

 Messages : LogQueue_Pkg.Implementation.List_Type (1024) ;
 procedure SetSource( source : string ) is
 begin
   Ada.Strings.Fixed.Move( source , Current_Source , drop => Ada.Strings.Right );
 end SetSource ;

 procedure SetDestination( destination : Destination_Access_Type ) is
 begin
   Current_Destination := destination ;
 end SetDestination ;

 procedure Log( Level : message_level_type ;
                  class : string := Default_Message_Class ;
                  message : string ) is
    logmsg : Log_Message_Type ;
  begin
    logmsg.ts := Ada.Calendar.Clock ;
    logmsg.level := level ;
    logmsg.Source := Current_Source ;
    Ada.Strings.Fixed.Move( class , logmsg.Class , drop => Ada.Strings.Right );
    logmsg.txt := to_unbounded_string(message) ;
    LogQueue_Pkg.Implementation.Enqueue( Messages , logmsg );
  end Log ;
  procedure Log( message : Log_Message_Type ) is
  begin
    LogQueue_Pkg.Implementation.Enqueue( Messages , message );
  end Log ;

  protected body MessageChannel is

      procedure Send( event : in out Ada.Real_Time.Timing_Events.Timing_Event ) is
         use Ada.Containers ;
      begin
        while LogQueue_Pkg.Implementation.Length( Messages ) > 0
        loop
          LogQueue_Pkg.Implementation.Dequeue( Messages , msg );
          Send( msg , Current_Destination.all );
        end loop;
        Ada.Real_Time.Timing_Events.Set_Handler(Event => EmptyMessageQueueEvent,
                                                In_Time => Secondly ,
                                                Handler => MessageChannel.Send'Access);
      end Send ;
      procedure Periodic( event : in out Ada.Real_Time.Timing_Events.Timing_Event ) is
      begin
        PeriodicCleanup( Current_Destination.all );
      end Periodic ;
  end MessageChannel ;

  procedure PeriodicCleanup( destination : Destination_Type ) is
  begin
    null ;
  end PeriodicCleanup ;

begin
  Secondly := Ada.Real_Time.Seconds( 1 );
  Ada.Real_Time.Timing_Events.Set_Handler(Event => EmptyMessageQueueEvent ,
                                          In_Time => Secondly ,
                                          Handler => MessageChannel.Send'Access);
  Hourly := Ada.Real_Time.Seconds( 3600 );
  Ada.Real_Time.Timing_Events.Set_Handler(Event => PeriodicCleanupEvent ,
                                          In_Time => Hourly ,
                                          Handler => MessageChannel.Periodic'Access);

end logging ;
