with interfaces.c ;

package body ctools is

    function sprintf(str : in out interfaces.c.char_array ;
                      format : interfaces.c.char_array ;
                      value : integer ) return integer ;
    pragma Import(C,sprintf);

    function fsprintf(str : in out interfaces.c.char_array ;
                      format : interfaces.c.char_array ;
                      value : long_float ) return integer ;
    pragma Import(C,fsprintf,"sprintf");
    function ssprintf(str : in out interfaces.c.char_array ;
                      format : interfaces.c.char_array ;
                      value : interfaces.c.char_array ) return integer ;
    pragma Import(C,ssprintf,"sprintf");

   procedure sprintf( str : in out string ;
                       format : string ;
                       value : integer ;
                       charswritten : out natural ) is
       result : Interfaces.C.Char_Array(1..128) ;
       temp : integer ;
    begin
      temp := sprintf( result ,
                               interfaces.c.to_c( format ) ,
                               value );
      Interfaces.C.To_Ada( result , str , charswritten );
    end sprintf ;

    procedure sprintf( str : in out string ;
                        format : string ;
                        value : float ;
                        charswritten : out natural ) is
        result : Interfaces.C.Char_Array(1..128) ;
        temp : integer ;
     begin
       temp := fsprintf( result ,
                        interfaces.c.to_c( format ) ,
                        long_float(value) );
       Interfaces.C.To_Ada( result , str , charswritten );
     end sprintf ;
    procedure sprintf( str : in out string ;
                       format : string ;
                       value : string ;
                       charswritten : out natural ) is
        result : Interfaces.C.Char_Array(1..128) ;
        temp : integer ;
     begin
       temp := ssprintf( result ,
                        interfaces.c.to_c( format ) ,
                        interfaces.c.to_c( value ) ) ;
       Interfaces.C.To_Ada( result , str , charswritten );
     end sprintf ;

     function Image( value : integer ; format : string ) return String is
       result : string (1..64) ;
       resultlen : natural ;
     begin
       sprintf( result , format , value , resultlen );
       return result(1..resultlen);
     end Image ;

     function Image( value : float ; format : string ) return String is
       result : string (1..64) ;
       resultlen : natural ;
     begin
       sprintf( result , format , value , resultlen );
       return result(1..resultlen);
     end Image ;

end ctools ;
