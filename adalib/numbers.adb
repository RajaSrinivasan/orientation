with Ada.Numerics.Elementary_Functions ;
with Ada.Text_Io; use Ada.Text_Io;

package body Numbers is

   procedure Show( Vec : Num_Pkg.Vector ) is
     elemsleft : Integer_Type := Integer_Type(Num_Pkg.Length(Vec));
     procedure Show( value : Num_Pkg.Cursor ) is
     begin
         Put(Integer_Type'Image(Num_Pkg.Element(value)));
         elemsleft := elemsleft - 1 ;
         if elemsleft > 0
         then
           Put(", ");
         else
           New_Line ;
         end if ;
     end Show ;
   begin
     Num_Pkg.Iterate( Vec , Show'Access) ;
   end Show ;

   function DigitsOf( Num : Integer_Type ; Base : Integer := 10) return Num_Pkg.Vector is
      Digs : Num_Pkg.Vector ;
      Num_Now : Integer_Type := Num ;
   begin
      while Num_Now > 0
      loop
         Num_Pkg.Prepend( Digs , Num_Now mod Integer_Type(Base) ) ;
         Num_Now := Num_Now / Integer_Type(Base) ;
      end loop ;
      return Digs ;
   end DigitsOf ;

   function ValueOfDigits( Digs : Num_Pkg.Vector ; Base : Integer := 10 ) return Integer_Type is
      Result : Integer_Type := 0 ;
      procedure Accumulate( Value : Num_Pkg.Cursor ) is
      begin
         Result := Result * Integer_Type(Base) + Num_Pkg.Element(Value) ;
      end Accumulate ;
   begin
      Num_Pkg.Iterate( Digs , Accumulate'Access ) ;
      return Result ;
   end ValueOfDigits ;

   function Divisors( Num : Integer_Type ) return Num_Pkg.Vector is
      Ubound : Float := 1.0 + Ada.Numerics.Elementary_Functions.Sqrt( Float(Num) ) ;
      Uboundmax : Integer_Type := Integer_Type(Ubound) ;
      Divs : Num_Pkg.Vector ;
      use Num_Pkg ;
      procedure Add( Num : Integer_Type ) is
      begin
         if Num_Pkg.Find( Divs , Num) = Num_Pkg.No_Element
         then
            Num_Pkg.Append( Divs , Num ) ;
         end if ;
      end Add ;

   begin
      Num_Pkg.Append( Divs , 1 ) ;
      for Candidate in 2..Uboundmax
      loop
         if Num mod Candidate = 0
         then
            Add( Candidate ) ;
            Add( Num / Candidate ) ;
         end if ;
      end loop ;
      Add( Num ) ;
      NumSort_Pkg.Sort( Divs ) ;
      return Divs ;
   end Divisors ;

   function Factors( Num : Integer_Type ) return Num_Pkg.Vector is
      Facs : Num_Pkg.Vector ;
      Divs : Num_Pkg.Vector := Divisors(Num) ;
      NumCurr : Integer_Type := Num ;
      procedure Add( NumPtr : Numbers.Num_Pkg.Cursor ) is
         Candidate : Integer_Type := Num_Pkg.Element(NumPtr) ;
      begin
         if Candidate /= 1
         then
            while NumCurr > 1
            loop
               if NumCurr mod Candidate = 0
               then
                  Facs.Append( Candidate ) ;
                  NumCurr := NumCurr / Candidate ;
               else
                  exit ;
               end if ;
            end loop ;
         end if ;
      end Add ;
   begin
      Num_Pkg.Iterate( Divs , Add'Access ) ;
      return Facs ;
   end Factors ;

   function ValueOfFactors( Facs : Num_Pkg.Vector ) return Integer_Type is
      Result : Integer_Type := 1 ;
      procedure Multiply( Value : Num_Pkg.Cursor ) is
      begin
         Result := Result * Num_Pkg.Element(Value) ;
      end Multiply ;
   begin
      Num_Pkg.Iterate( Facs , Multiply'Access ) ;
      return Result ;
   end ValueOfFactors ;

   function Prime( Num : Integer_Type ) return Boolean is
      use Ada.Containers ;
      Divs : Num_Pkg.Vector := Divisors( Num ) ;
   begin
      if Num <= 1
      then
         return False ;
      end if ;
      if Num_Pkg.Length(Divs) = 2
      then
         return True ;
      end if ;
      return False ;
   end Prime ;
   function Perfect( Num : Integer_Type ) return Boolean is
      Divs : Num_Pkg.Vector := Divisors(Num) ;
      Sumdiv : Integer_Type := Sum(Divs) ;
   begin
      if Sumdiv / 2 = Num
      then
         return True ;
      end if ;
      return False ;
   end Perfect ;

   function ValueOfDigits(Digs : Num_Pkg.Vector ; From , To : Integer ) return Integer_Type is
      Result : Integer_Type := 0 ;
   begin
      for Idx in From .. To
      loop
         Result := Result * Integer_Type(10) + Num_Pkg.Element( Digs , Idx - 1 ) ;
      end loop ;
      return Result ;
   end ValueOfDigits ;

   function Kaprekar( Num : Integer_Type ) return Boolean is
      Numsq : Integer_Type := Num * Num ;
      NumSqDigits : Num_Pkg.Vector := DigitsOf( NumSq ) ;
      Left, Right : Integer_Type ;
      use Ada.Containers ;
   begin
      for Part in 1..Num_Pkg.Length(NumSqDigits)-1
      loop
         Left := ValueOfDigits(NumSqDigits,1,Integer(Part)) ;
         Right := ValueOfDigits(NumSqDigits,Integer(Part+1),Integer(Num_Pkg.Length(NumSqDigits)));
         if Right /= 0 and then Left + Right = Num
         then
            return True ;
         end if ;
      end loop ;
      return False ;
   end Kaprekar ;

   function Trimorphic( Number : Integer_Type  ) return Boolean is
      Numdigits : Num_Pkg.Vector ;
      Numcubedigits : Num_Pkg.Vector ;
      Numcubed : Integer_Type := Number ** 3 ;
      Lennumdigits, Lennumcubedigits : Integer ;
   begin
      Numdigits := DigitsOf( Number ) ;
      Numcubedigits := DigitsOf( Numcubed ) ;
      Lennumdigits := Integer(Num_Pkg.Length(Numdigits)) ;
      Lennumcubedigits := Integer(Num_Pkg.Length( Numcubedigits )) ;
      for Idx in 0..Lennumdigits-1
      loop
         if Num_Pkg.Element( Numdigits , Integer(Idx) ) /=
            Num_Pkg.Element( Numcubedigits , Lennumcubedigits - lennumdigits + Idx )
         then
            return False;
         end if ;
      end loop ;
      return True ;
   end Trimorphic ;
   function CoPrimes( Num : Integer_Type ) return Num_Pkg.Vector is
     result : Num_Pkg.Vector ;
   begin
     for cp in 2..num-1
     loop
        if CoPrime(cp,num)
        then
          result.Append(cp);
        end if ;
     end loop ;
     return result ;
   end CoPrimes ;

   function Sum( Nums : Num_Pkg.Vector ) return Integer_Type is
      Result : Integer_Type := 0 ;
      procedure Sum( Num : Num_Pkg.Cursor ) is
      begin
         Result := Result + Num_Pkg.Element(Num) ;
      end Sum ;
   begin
      Num_Pkg.Iterate( Nums, Sum'Access ) ;
      return Result ;
   end Sum;


   function Gcd( Num1, Num2 : Integer_Type ) return Integer_Type is
   begin
      if Num1 > Num2
      then
         return Gcd( Num2 , Num1 ) ;
      elsif Num1 = Num2
      then
         return Num1 ;
      end if ;
      if Num2 mod Num1 = 0
      then
         return Num1;
      end if ;
      return Gcd( Num1 , Num2 mod Num1 ) ;
   end Gcd ;

   function Lcm( Num1 , Num2 : Integer_TYpe ) return Integer_Type is
      use Num_Pkg ;
      lcm_candidate : Integer_Type := Num1 ;
      found_multiple : boolean := false ;
   begin
      if Num1 = Num2
      then
         return Num1 ;
      end if ;

      declare
        num1_divs : Num_Pkg.Vector := Factors(Num1);
        num2_divs : Num_Pkg.Vector := Factors(Num2);  
        num1_fac, num2_fac : Num_Pkg.Cursor ;
        lcm_divs : Num_Pkg.Vector ;
      begin
        num1_fac := Num_Pkg.First( num1_divs );
        num2_fac := Num_Pkg.First( num2_divs );
        Calc_Loop:
        loop
           if Num_Pkg.Element(num1_fac) = Num_Pkg.Element(num2_fac)
           then
              Num_Pkg.Append( lcm_divs , Num_Pkg.Element(num1_fac));
              num1_fac := Num_Pkg.Next( num1_fac );
              num2_fac := Num_Pkg.Next( num2_fac );
           elsif Num_Pkg.Element(num1_fac) > Num_Pkg.Element(num2_fac)
           then
              Num_Pkg.Append( lcm_divs , Num_Pkg.Element(num2_fac));
              num2_fac := Num_Pkg.Next( num2_fac );
           else
              Num_Pkg.Append( lcm_divs , Num_Pkg.Element(num1_fac));
              num1_fac := Num_Pkg.Next( num1_fac );          
           end if ;
           if num1_fac = Num_Pkg.No_Element
           then
              while num2_fac /= Num_Pkg.No_Element
              loop
                Num_Pkg.append( lcm_divs , Num_Pkg.Element(Num2_fac));
                num2_fac := Num_Pkg.Next(num2_fac);
              end loop ;
              exit Calc_Loop;
           elsif num2_fac = Num_Pkg.No_Element
           then
              while num1_fac /= Num_Pkg.No_Element
              loop
                  Num_Pkg.append( lcm_divs , Num_Pkg.Element(Num1_fac));
                  num1_fac := Num_Pkg.Next(num1_fac);
              end loop ;
              exit Calc_Loop;           
           end if ;
        end loop Calc_Loop;
        lcm_candidate := ValueOfFactors(lcm_divs);
      end ;
      return lcm_candidate ;
   end Lcm ;

   function CoPrime( Num1, Num2 : Integer_Type ) return Boolean is
   begin
      if Gcd(Num1,Num2) = 1
      then
         return True ;
      end if ;
      return False ;
   end CoPrime ;

   function Amicable( Num1, Num2 : Integer_Type ) return Boolean is
     divs1 : Num_Pkg.Vector := Divisors(Num1);
     divs1sum : Integer_Type := Sum(divs1) - num1 ;
     divs2 : Num_Pkg.Vector := Divisors(Num2);
     divs2sum : Integer_Type := Sum(divs2) - num2 ;
   begin
     if divs1sum = num2 and then
        divs2sum = Num1
        then
          return true ;
        end if ;
     return false ;
   end Amicable ;

   function Totient( Num : Integer_Type ) return Integer_Type is
   begin
     return Integer_Type(Num_Pkg.Length(CoPrimes(num)) );
   end totient ;

   function DivisorsSigma( Num : Integer_Type ; K : Integer := 1 ) return Integer_Type is
      ResultSum : Integer_Type := 0;
      Divs : Num_Pkg.Vector := Divisors(Num) ;
      procedure Accumulate( NumPtr : Num_Pkg.Cursor ) is
      begin
         ResultSum := ResultSum + Num_Pkg.Element(NumPtr) ** K ;
      end Accumulate ;
   begin
      Num_Pkg.Iterate( Divs , Accumulate'Access ) ;
      return ResultSum ;
   end DivisorsSigma ;

end Numbers ;
