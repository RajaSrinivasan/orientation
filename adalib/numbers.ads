with Ada.Containers.Vectors ;
generic 
   type Integer_Type is range <> ;
package Numbers is
   package Numbers_Vectors_Pkg is new Ada.Containers.Vectors(Natural,Integer_Type) ;
   package Numbers_Vectors_Sort_Pkg is new Numbers_Vectors_Pkg.Generic_Sorting ( "<" ) ;

   package Num_Pkg renames Numbers_Vectors_Pkg ;
   package NumSort_Pkg renames Numbers_Vectors_Sort_Pkg ;

   procedure Show(vec : Num_Pkg.Vector);
   function DigitsOf( Num : Integer_Type ; Base : Integer := 10) return Num_Pkg.Vector ;
   function ValueOfDigits( Digs : Num_Pkg.Vector ; Base : Integer := 10 ) return Integer_Type ;

   function Divisors( Num : Integer_Type ) return Num_Pkg.Vector ;
   function Factors( Num : Integer_Type ) return Num_Pkg.Vector ;
   function ValueOfFactors( Facs : Num_Pkg.Vector ) return Integer_Type ;

   function Prime( Num : Integer_Type ) return Boolean ;
   function Perfect( Num : Integer_Type ) return Boolean ;
   function Trimorphic( Number : Integer_Type ) return Boolean ;
   function Kaprekar( Num : Integer_Type ) return Boolean ;

   function CoPrimes( Num : Integer_Type ) return Num_Pkg.Vector ;
   function Totient( Num : Integer_Type ) return Integer_Type ;

   function Gcd( Num1 , Num2 : Integer_Type ) return Integer_Type ;
   function Lcm( Num1 , Num2 : Integer_TYpe ) return Integer_Type ;
   function CoPrime( Num1, Num2 : Integer_Type ) return Boolean ;
   function Amicable( Num1, Num2 : Integer_Type ) return Boolean ;
   
   type Rational_Number is
      record
         Numerator : Integer_Type ;
         Denominator : Integer_Type ;
      end record ;
   function DivisorsSigma( Num : Integer_Type ; K : Integer := 1 ) return Integer_Type ;
private
   function Sum( Nums : Num_Pkg.Vector ) return Integer_Type ;
end Numbers ;
