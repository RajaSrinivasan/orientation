with Ada.Text_Io; use Ada.Text_Io ;

package body Getcmdline is
   function Get_Line( Prompt : String := "cmd" )
                    return String is
      Command : String (1..128) ;
      Command_Len : Integer := 0 ;
   begin
      Ada.Text_Io.Put(Prompt) ;
      Ada.Text_Io.Flush ;
      delay 0.1;
      Ada.Text_Io.Get_Line( Command , Command_Len ) ;
      return Command(1..Command_Len) ;
   end Get_Line ;
end Getcmdline ;
