with Ada.Calendar ;
with Ada.Strings.Fixed;
with ada.strings.unbounded ; use Ada.Strings.Unbounded ;
with Ada.Finalization ;
with Ada.Containers.Synchronized_Queue_Interfaces ;
with Ada.Containers.Bounded_Synchronized_Queues ;
with Ada.Real_Time ;
with Ada.Real_Time.Timing_Events ;

package logging is

   type message_level_type is
     (
        CRITICAL ,
        ERROR ,
        WARNING ,
        INFORMATIONAL ,
        DEBUG
     ) ;

   subtype Message_Source_Type is String(1..6) ;
   Default_Message_Source_Type : Message_Source_Type := (others => '.');
   procedure SetSource( source : string ) ;

   subtype Message_Class_Type is String (1..3);
   Default_Message_Class : Message_Class_Type := (others => '.');

   type Log_Message_Type is
     record
        ts : Ada.Calendar.Time ;
        level : Message_Level_Type ;
        Source : Message_Source_Type ;
        Class : Message_Class_Type ;
        txt : Unbounded_String ;
      end record ;
   procedure Log( Level : message_level_type ;
                  class : string := Default_Message_Class ;
                  message : string ) ;
  procedure Log( message : Log_Message_Type ) ;

   package LogMsg_Pkg is new Ada.Containers.Synchronized_Queue_Interfaces(
        Log_Message_Type
   ) ;
   package LogQueue_Pkg is new Ada.Containers.Bounded_Synchronized_Queues(
        LogMsg_Pkg , 1024
   ) ;

   type Destination_Type is abstract new Ada.Finalization.Controlled with null record;
   type Destination_Access_Type is access all Destination_Type'Class ;

   procedure SetDestination( destination : Destination_Access_Type ) ;
   procedure Send( message : Log_Message_Type ;
                   destination : Destination_Type ) is abstract ;
   procedure PeriodicCleanup( destination : Destination_Type );

   protected MessageChannel is
       procedure Send( event : in out Ada.Real_Time.Timing_Events.Timing_Event );
       procedure Periodic( event : in out Ada.Real_Time.Timing_Events.Timing_Event );
       private
          msg : Log_Message_Type ;
   end MessageChannel ;

   function Image (ts : Ada.Calendar.Time) return string ;
   numimages : array (0..60) of String(1..2)
   := (
       "00" , "01" , "02" , "03" , "04" , "05" , "06" , "07" , "08" , "09" ,
       "10" , "11" , "12" , "13" , "14" , "15" , "16" , "17" , "18" , "19" ,
       "20" , "21" , "22" , "23" , "24" , "25" , "26" , "27" , "28" , "29" ,
       "30" , "31" , "32" , "33" , "34" , "35" , "36" , "37" , "38" , "39" ,
       "40" , "41" , "42" , "43" , "44" , "45" , "46" , "47" , "48" , "49" ,
       "50" , "51" , "52" , "53" , "54" , "55" , "56" , "57" , "58" , "59" ,
       "60" );
private
   Current_Destination : Destination_Access_Type ;
   Current_Source : Message_Source_Type := Default_Message_Source_Type ;

end logging;
