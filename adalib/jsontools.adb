with System.Storage_Elements ;
with Ada.Text_Io ; use Ada.Text_Io ;
with Ada.Integer_Text_Io ; use Ada.Integer_Text_Io ;
with Ada.Float_Text_Io ; use Ada.Float_Text_Io ;
with Ada.Directories ;
with Ada.Streams.Stream_IO ;

package body jsontools is

    procedure Save( value : GNATCOLL.json.json_value ;
                    filename : string ) is
       ppfile : file_type ;
       outputstr : string := JSON.Write(value);
       margin : Count := 1 ;
       strmode : boolean := false ;
    begin
        Create(ppfile,out_file,filename);
        Set_Col( ppfile , margin) ;
        for c of outputstr
        loop
          if not strmode
          then
              case c is
                when '{' |
                     '[' =>
                  put( ppfile , ' ') ;
                  put( ppfile , c );
                  New_Line( ppfile );
                  margin := margin + default_indentation ;
                  Set_Col( ppfile , margin );
                when '}' |
                     ']' =>
                  New_Line( ppfile );
                  margin := margin - default_indentation ;
                  Set_Col( ppfile , margin );
                  put( ppfile , c );
                  Set_Col( ppfile , margin );
                when ',' |
                     ';' =>
                  put( ppfile , c );
                  New_Line( ppfile );
                  Set_Col( ppfile , margin ) ;
                when '"' =>
                  put( ppfile , c );
                  strmode := true ;
                when others =>
                  put( ppfile , c );
              end case ;
            elsif c = '"' then
              put( ppfile , c );
              strmode := false ;
            else
              put( ppfile , c );
            end if ;
        end loop;
        Close(ppfile);
    end Save ;

    procedure Load( value : out GNATCOLL.json.json_value ;
                    filename : string ) is
       filesize : Natural := Natural (Ada.Directories.Size (filename));
       file   : Ada.Streams.Stream_IO.File_Type;
       stream : Ada.Streams.Stream_IO.Stream_Access;
       buffer : Ada.Streams.Stream_Element_Array (1 .. Ada.Streams.Stream_Element_Offset (filesize));
       bufferlen    : Ada.Streams.Stream_Element_Offset;
       filecontents : String (1 .. filesize);
       for filecontents'Address use buffer'Address;
    begin
      Ada.Streams.Stream_IO.Open
        (file,
         Ada.Streams.Stream_IO.In_File,
         filename);
      stream := Ada.Streams.Stream_IO.Stream (file);
      stream.Read (buffer, bufferlen);
      Ada.Streams.Stream_IO.Close (file);
      value := GNATCOLL.json.Read( filecontents );
    end Load ;

end jsontools ;
