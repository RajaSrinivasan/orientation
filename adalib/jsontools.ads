with Ada.Text_Io; use Ada.Text_Io;
with GNATCOLL.JSON ; use GNATCOLL.JSON ;
package jsontools is
  package JSON renames GNATCOLL.JSON ;
  default_indentation : Count := 3 ;
  procedure Save( value : GNATCOLL.json.json_value ;
                  filename : string );
  pragma Precondition (Kind (value) = JSON_Object_Type );

  procedure Load( value : out GNATCOLL.json.json_value ;
                  filename : string ) ;
  pragma Postcondition (Kind (value) = JSON_Object_Type );

end jsontools ;
