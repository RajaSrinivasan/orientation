with Ada.Command_Line;    use Ada.Command_Line;
with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

procedure Lister is
   Inpfile : Ada.Text_IO.File_Type;
   Line    : String (1 .. 256);
   Linelen : Natural;
   Lineno  : Integer := 0;
begin
   if Ada.Command_Line.Argument_Count < 1 then
      Put_Line ("usage: lister file1 file2 ...");
      return;
   end if;

   for arg in 1..Ada.Command_Line.Argument_Count
   loop
        Open (Inpfile, In_File, Ada.Command_Line.Argument (Arg));
        Put("------ File : ");
        Put(Ada.Command_Line.Argument(Arg));
        Put_Line("------------------------------------------");
        lineno := 0 ;
        while not End_Of_File (Inpfile) loop
            Get_Line (Inpfile, Line, Linelen);
            Lineno := Lineno + 1;
            Put (Lineno, Width => 4);
            Put (" : ");
            Put_Line (Line (1 .. Linelen));
        end loop;
        Ada.Text_IO.Close (Inpfile);
        Put("------");
        Put(LineNo);
        Put_Line(" lines-----------------------------------------");
   end loop ;
end Lister;