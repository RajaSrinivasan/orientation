# Project: search

## Objective
Explore: directory and file processing
         regular expressions

## Usage:
```
../bin/search -h
Search V01 2017-11-11 16:10:25
Usage: search Search

 -v, --verbose[ARG] Output extra verbose information
 -r, --regexp ARG   Regular expression to search for
 -s, --string ARG   string to search for
 -t, --dir-tree     search the directory tree
 -n, --nameARG      filename pattern

Search the files provided for either the string or the regular expression.
Use the name argument to filter file names based on file type or base name.
```

## Extra Credit:

Modify Projects Proj1 and Proj2 to accept a command line with the switches
-t, -n similar to this project to filter the file arguments 

