with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with search_cli ;                                          -- [cli/$_cli] ;
package search_pkg is
    procedure Initialize ;
    procedure Search( Subject  : String ) ;
end search_pkg ;                                            -- [pkg/$_pkg]
