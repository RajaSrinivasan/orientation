with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;
with Ada.Strings.Bounded ;
package body search_cli is                          -- [cli/$_cli]

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
       GNAT.Command_Line.Set_Usage( Config ,
                                   Help => NAME & " " &
                                      VERSION & " " &
                                      Compilation_ISO_Date & " " &
                                      Compilation_Time ,
                                    Usage => "Search");
        GNAT.Command_Line.Define_Switch (Config,
                       verbose'access ,
                       Switch => "-v?",
                       Long_Switch => "--verbose?",
                       Help => "Output extra verbose information");

        GNAT.Command_Line.Define_Switch (Config,
                                      output => findregexp'access,
                                      Switch => "-r:",
                                      Long_Switch => "--regexp:",
                                      Help => "Regular expression to search for");

        GNAT.Command_Line.Define_Switch (Config,
                                      output => findstring'access,
                                      Switch => "-s:",
                                      Long_Switch => "--string:",
                                      Help => "string to search for");
        GNAT.Command_Line.Define_Switch (Config,
                                      output => recursive'access,
                                      Switch => "-t",
                                      Long_Switch => "--dir-tree",
                                      Help => "search the directory tree");

        GNAT.Command_Line.Define_Switch (Config,
                                      output => filenamepattern'access,
                                      Switch => "-n!",
                                      Long_Switch => "--name!",
                                      Help => "filename pattern");

        GNAT.Command_Line.Getopt(config,SwitchHandler'access);

    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GNAT.Command_Line.Get_Argument(Do_Expansion => False ) ;
    end GetNextArgument ;

    procedure ShowCommandLineArguments is
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
       Put("findregexp "); Put(Findregexp.all); New_Line ;
       Put("findstring "); Put(Findstring.all); New_Line ;
       Put("filenamepattern "); Put(Filenamepattern.all) ; New_Line ;
    end ShowCommandLineArguments ;
begin
    Filenamepattern.all := "*" ;
end search_cli ;                                   -- [cli/$_cli]
