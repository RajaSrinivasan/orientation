with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with Gnat.Command_Line ;
with GNAT.Source_Info ;

with search_cli ;                            -- [cli/$_cli]
with Search_Pkg ;

procedure search is                  -- [clitest/$]
begin
   search_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if search_cli.Verbose                     -- [cli/$_cli]
   then
      search_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;
   Search_Pkg.Initialize ;
   loop
      declare
         Arg : String := Search_Cli.GetNextArgument ;
      begin
         if Arg'Length >= 1
         then
            Search_Pkg.Search( Arg ) ;
         else
            exit ;
         end if ;
      end ;
   end loop ;
exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
        return ;
end search ;                         -- [clitest/$]
