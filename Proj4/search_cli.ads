with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package search_cli is                                       -- [cli/$_cli]

   VERSION : string := "V01" ;
   NAME : String := "Search" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose

   findregexp : aliased Gnat.strings.string_access ;
   findstring : aliased Gnat.strings.string_access ;
   filenamepattern : aliased Gnat.strings.string_access := new String(1..1) ;
   Recursive : aliased Boolean := False ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

end search_cli ;                                            -- [cli/$_cli]
