with Text_Io; use Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

with Ada.Directories ;
with Ada.Strings.Fixed ;

with Gnat.Os_Lib ;
with Gnat.Regpat ;
with Gnat.Regexp ;

with Dirwalk ;

package body search_Pkg is        -- [Pkg/$_Pkg]
   use Gnat.Strings ;

   type Search_Context_Type is new Integer ;
   procedure Search_File( Context : Search_Context_Type ; Filename : String ) ;
   procedure Search_Tree is new Dirwalk( Search_Context_Type ) ;

   Search_Pattern : Gnat.Regpat.Pattern_Matcher (1024) ;
   -- Search_Pattern : Gnat.Regpat.Pattern_Matcher := Gnat.Regpat.Never_Match ;

   procedure Search_File( Context : Search_Context_Type ; Filename : String ) is
      Filename_Shown : Boolean := False ;
      Line_No : Integer := 0 ;
      procedure Show_File_Name is
      begin
         if not Filename_Shown
         then
            Put_Line(Filename);
            Filename_Shown := True ;
         end if ;
      end Show_File_Name ;
      Inp_File : Text_Io.File_Type ;
      Inp_Line : String( 1..1024 ) ;
      Inp_Line_Len : Natural ;
      procedure Show_Line is
      begin
         Show_File_Name ;
         Ada.Integer_Text_Io.Put( Line_No , Width => 4 ) ;
         Text_Io.Put(" : ");
         Text_Io.Put_Line(Inp_Line(1..Inp_Line_Len)) ;
      end Show_Line ;
   begin
      if Search_Cli.Verbose
      then
         Show_File_Name ;
      end if ;
      Text_Io.Open( Inp_File , Text_Io.In_File , Filename ) ;
      while not Text_Io.End_Of_File( Inp_File )
      loop
         Text_Io.Get_Line(Inp_File,Inp_Line,Inp_Line_Len) ;
         Line_No := Line_No + 1 ;
         if Search_Cli.Findregexp.all'Length > 0
         then
            if Gnat.RegPat.Match( Search_Pattern , Inp_Line(1..Inp_Line_Len))
            then
               Show_Line ;
            end if ;
         else
            if Ada.Strings.fixed.Index(Inp_Line(1..Inp_Line_Len), Search_Cli.Findstring.all) >= 1
            then
               Show_Line ;
            end if ;
         end if ;
      end loop ;
      Text_Io.Close( Inp_File );
   exception
      when others => null ;
   end Search_File ;

   procedure Initialize is
   begin
      if Search_Cli.Findstring.all'Length > 0 and
         Search_Cli.Findregexp.all'Length > 0
      then
         Put_Line("Cannot find a string as well as a regular expression");
         return ;
      end if ;
      if Search_Cli.Findstring.all'Length = 0 and
         Search_Cli.Findregexp.all'Length = 0
      then
         Put_Line("Please provide a string or a regular expression to search for");
         return ;
      end if ;

      if Search_Cli.Findregexp.all'Length > 0
      then
         Gnat.Regpat.Compile( Search_Pattern , Search_Cli.Findregexp.all ) ;
      end if ;
   end Initialize ;

   procedure Search( Subject : String ) is
   begin
      if Search_Cli.Verbose
      then
         Put("-----");
         Put(Subject);
         Put_Line("-----------------------------------------------");
      end if ;
      if Gnat.Os_Lib.Is_Directory( Subject )
      then
         Search_Tree( 0 , Subject , Search_Cli.Filenamepattern.all , Search_Cli.Recursive , Search_File'Access ) ;
      else
         Search_File( 1 , Ada.Directories.Full_Name(Subject) );
      end if ;
   end Search ;
end search_Pkg ;                  -- [Pkg/$_Pkg]
