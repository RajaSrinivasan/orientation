with logging_Tests ;                         -- [numbers/$]
with logging_Suite ;                         -- [numbers/$]
with AUnit.Test_Suites ; use AUnit.Test_Suites ;
with AUnit.Run ;
with AUnit.Reporter.Text ;

procedure logging_Unit_Test is               -- [numbers/$]
    procedure Run is new AUnit.Run.Test_Runner( logging_Suite.Suite ); -- [numbers/$]
    Reporter : AUnit.Reporter.Text.Text_Reporter ;
begin
    Run(Reporter);
end logging_Unit_Test ;                     -- [numbers/$]
