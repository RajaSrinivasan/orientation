with AUnit ; use AUnit ;
with AUnit.Test_Cases ; use AUnit.Test_Cases ;

package logging_Tests is                              -- [numbers/$]

    type logging_Test is new Test_Cases.Test_Case with null record ;  -- [numbers/$]
    procedure Register_Tests( T : in out logging_Test );              -- [numbers/$]
    function Name( T : logging_Test ) return Message_String ;         -- [numbers/$]
    procedure Test_TimeStamp( T : in out Test_Cases.Test_Case'Class ) ;
    procedure Test_LogFileName( T : in out Test_Cases.Test_Case'Class );
    procedure Test_LoggingBasic( T : in out Test_Cases.Test_Case'Class );
    procedure Test_LoggingTasks( T : in out Test_Cases.Test_Case'Class );
end logging_Tests ;                                    -- [numbers/$]
