
with Ada.Numerics.Elementary_Functions ;

with Ada.Text_Io ; use Ada.Text_Io ;
with Ada.Integer_Text_Io ; use Ada.Integer_Text_Io ;
with Ada.Float_Text_Io ; use Ada.Float_Text_Io ;
with Ada.Calendar ;

with AUnit.Assertions ; use AUnit.Assertions ;

with logging ;                                                      -- [numbers/$]
with logging.file ;

package body logging_tests is                                       -- [numbers/$]

    procedure Register_Tests( T : in out logging_Test ) is          -- [numbers/$]
       use AUnit.Test_Cases.Registration ;
    begin
       Register_Routine( T , Test_TimeStamp'access , "Test Timestamp");
       Register_Routine( T , Test_LogFileName'access , "Test LogFileName");
       Register_Routine( T , Test_LoggingBasic'access , "Test Logging File Basic");
       Register_Routine( T , Test_LoggingTasks'access , "Test Logging File Tasks");
   end Register_Tests ;

    function Name( T : logging_Test ) return Message_String is      -- [numbers/$]
    begin
        return Format("logging Tests") ;                     -- [numbers/$]
    end Name ;

    procedure Test_TimeStamp( T : in out Test_Cases.Test_Case'Class ) is
    begin
        Put_Line( logging.Image( Ada.Calendar.Clock )) ;
    end Test_TimeStamp ;

    procedure Test_LogFileName( T : in out Test_Cases.Test_Case'Class ) is
    begin
        Put_Line( logging.file.GenerateFileName( "logtest" )) ;
    end Test_LogFileName ;

    procedure Test_LoggingBasic( T : in out Test_Cases.Test_Case'Class ) is
    begin
      logging.SetDestination( logging.file.Create("logtest") );
      logging.SetSource("UnitT1") ;
      for i in 1..100
      loop
         logging.Log( logging.CRITICAL , message => "Message No " & integer'image(i));
         delay 0.1 ;
      end loop;
    end Test_LoggingBasic ;

    task type LoggerApp_Task is
      entry Start( id : integer ) ;
    end LoggerApp_Task ;
    type LoggerApp_Task_Acccess is access LoggerApp_Task ;

    task body LoggerApp_Task is
      MyId : Integer ;
    begin
      accept Start(id : integer) do
        MyId := id ;
      end Start ;
      declare
        myname : string := "Task " & Integer'Image(MyId) ;
      begin
        for i in logging.numimages'range
        loop
           Logging.Log( logging.CRITICAL , message => myname & " Message No " &
                                logging.numimages(i));
           delay 0.3 ;
        end loop;
      end ;
    end LoggerApp_Task ;

    procedure Test_LoggingTasks( T : in out Test_Cases.Test_Case'Class ) is
      newtask : LoggerApp_Task_Acccess ;
    begin
        logging.SetSource("UnitT2") ;
        for t in 1..4
        loop
          newtask := new LoggerApp_Task ;
          newtask.Start( t );
          delay(0.1);
        end loop;
        delay(300.0);
    end Test_LoggingTasks ;

end logging_tests ;                                           -- [numbers/$]
