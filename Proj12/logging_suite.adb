with logging_tests ;                                                 -- [numbers/$]

package body logging_suite is                                         -- [numbers/$]
    t : aliased logging_Tests.logging_Test ;                         -- [numbers/$]
    function suite return Access_Test_Suite is
        Ret : constant Access_Test_Suite := new Test_Suite;

    begin
        logging_Tests.Register_Tests( T );                            -- [numbers/$]
        Ret.Add_Test( T'access );
        return Ret ;
    end suite ;
end logging_suite ;                                                    -- [numbers/$]
