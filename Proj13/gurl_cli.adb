with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body gurl_cli is                          -- [cli/$_cli]

    package GCL renames GNAT.Command_Line ;

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin

        GCL.Set_Usage( Config ,
                       Help => NAME & " " &
                                      VERSION & " " &
                                      Compilation_ISO_Date & " " &
                                      Compilation_Time ,
                                      Usage => "Command Line utility");
        GCL.Define_Switch (Config,

                       verbose'access ,
                       Switch => "-v",
                       Long_Switch => "--verbose",
                       Help => "Output extra verbose information");

        GCL.Getopt(config,SwitchHandler'access);

    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GCL.Get_Argument(Do_Expansion => True) ;
    end GetNextArgument ;

    procedure Show(name : string ; val : boolean ) is
    begin
       Put(name) ; Put( " => ");
       put(boolean'image(val));
       new_line ;
     end Show ;
     procedure Show(name : string ; val : GNAT.strings.string_access) is
       use GNAT.Strings ;
     begin
       put(name) ;
       put(" => ");
       if val /= null
       then
          put(val.all) ;
       else
          put("unspecified");
       end if ;
       new_line ;
     end Show ;

    procedure ShowCommandLineArguments is
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
    end ShowCommandLineArguments ;

end gurl_cli ;                                   -- [cli/$_cli]
