with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with GNAT.Source_Info ;

with AWS.Client ;
with AWS.response ;

with gurl_cli ;                            -- [cli/$_cli]
with gurl_Pkg ;                            -- [Pkg/$_Pkg]
procedure gurl is                  -- [clitest/$]
   resp : AWS.Response.Data ;
begin
   gurl_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if gurl_cli.Verbose                     -- [cli/$_cli]
   then
      gurl_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;
   resp := AWS.Client.get("http://www.abiomed.com");
   
end gurl ;                         -- [clitest/$]
