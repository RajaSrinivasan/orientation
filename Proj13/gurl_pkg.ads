with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with gurl_cli ;                                          -- [cli/$_cli] ;
package gurl_pkg is                                       -- [pkg/$_pkg]
    procedure Implementation ;
end gurl_pkg ;                                            -- [pkg/$_pkg]
