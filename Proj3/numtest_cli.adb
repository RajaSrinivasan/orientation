with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body numtest_cli is                          -- [cli/$_cli]
   package GCL renames GNAT.Command_Line ;

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
       GCL.Set_Usage( Config ,
                      Help => NAME & " " &
                        VERSION & " " &
                        Compilation_ISO_Date & " " &
                        Compilation_Time ,
                      Usage => "numtest [tests] num1 [num2]");
       GCL.Define_Switch (Config,
                          verbose'access ,
                          Switch => "-v",
                          Long_Switch => "--verbose",
                          Help => "Output extra verbose information");
       GCL.Define_Prefix(Config,"-show");
       GCL.Define_Alias(Config,"-showA","-showg -showv -showf -showcp") ;
       GCL.Define_Switch (Config,
                          output => ShowDigits'access,
                          Switch => "-showg",
                          Help => "print decimal digits");
       GCL.Define_Switch (Config,
                          output => ShowDivisors'access,
                          Switch => "-showv",
                          Help => "print divisors of");
       GCL.Define_Switch (Config,
                          output => ShowFactors'access,
                          Switch => "-showf" ,
                          Help => "print factors of");
      GCL.Define_Switch (Config,
                         output => ShowCoPrimes'access,
                         Switch => "-showcp" ,
                         Help => "show co primes less than of");

       GCL.Define_Prefix(Config,"-test") ;
       GCL.Define_Alias(Config,"-testA","-testp -testf -testk -testt -testcp");
       GCL.Define_Switch (Config,
                          output => PrimeTest'access,
                          Switch => "-testp",
                          Help => "is number prime");
       GCL.Define_Switch (Config,
                          output => PerfectTest'access,
                          Switch => "-testf",
                          Help => "is number perfect");
       GCL.Define_Switch (Config,
                          output => KaprekarTest'access,
                          Switch => "-testk",
                          Long_Switch => "--kaprekar-test",
                          Help => "is number a kaprekar number");
       GCL.Define_Switch (Config,
                          output => TrimorphicTest'access,
                          Switch => "-testt",
                          Long_Switch => "--trimorphic",
                          Help => "is number trimorphic");
      GCL.Define_Switch (Config,
                         output => CoPrimeTest'access,
                         Switch => "-testcp",
                         Long_Switch => "--test-coprime",
                         Help => "are numbers co prime");

       GCL.Define_Prefix(Config , "-calc") ;
       GCL.Define_Alias( Config , "-calcA" , "-calcg -calcl -calct") ;
       GCL.Define_Switch (Config,
                          output => CalculateGCD'access,
                          Switch => "-calcg",
                          Help => "calculate g => gcd");
       GCL.Define_Switch (Config,
                          output => CalculateLCM'access,
                          Switch => "-calcl",
                          Help => "calculate l => lcm");

       GCL.Define_Switch( Config ,
                          Switch => "-calct" ,
                          Output => CalculateTotient'access ,
                          Help => "Calculate Euler totient" );

       --GCL.Getopt(config,SwitchHandler'access);
       GCL.GetOpt(Config);
    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GNAT.Command_Line.Get_Argument(Do_Expansion => True) ;
    end GetNextArgument ;

    procedure ShowCommandLineArguments is
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
    end ShowCommandLineArguments ;

end numtest_cli ;                                   -- [cli/$_cli]
