with AUnit ; use AUnit ;
with AUnit.Test_Cases ; use AUnit.Test_Cases ;

package numbers_Tests is

    type numbers_Test is new Test_Cases.Test_Case with null record ;
    procedure Register_Tests( T : in out numbers_Test );
    function Name( T : numbers_Test ) return Message_String ;

    procedure Test_Kaprekar( T : in out Test_Cases.Test_Case'Class ) ;
    procedure Test_Amicable( T : in out Test_Cases.Test_Case'Class ) ;

end numbers_Tests ;
