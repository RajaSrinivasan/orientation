with Text_Io ; use Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

with Ada.Command_Line ;

with Integer_Numbers_Pkg ;

procedure gcd_Test is
   Arg1, Arg2 : Integer ;
   procedure Show( NumPtr : Integer_Numbers_Pkg.Num_Pkg.Cursor ) is
   begin
      Put( Integer'Image(Integer_Numbers_Pkg.Num_Pkg.Element(NumPtr))) ;
      Put( " , " ) ;
   end Show ;

   procedure Show( Vec : Integer_Numbers_Pkg.Num_Pkg.Vector ) is
   begin
      Put("[");
      Integer_Numbers_Pkg.Num_Pkg.Iterate( Vec , Show'Access ) ;
      Put_Line("]");
   end Show ;

   procedure Test( Arg1, Arg2 : Integer ) is
      Result : Integer ;
   begin
      Put("------");
      Put(Integer'Image(Arg1)); Put(" & "); Put(Integer'Image(Arg2)) ;
      Put_Line("----------------------------------------------------------");
      Result := Integer_Numbers_Pkg.Gcd(Arg1,Arg2) ;
      Put("GCD "); Put(Integer'Image(result)) ; New_Line ;
      if Result = 1
      then
         Put("Are CoPrime");
      else
         Put("Are not Coprime");
      end if ;
   end Test ;
begin
   Arg1 := Integer'Value( Ada.Command_Line.Argument(1) ) ;
   Arg2 := Integer'Value( Ada.Command_Line.Argument(2) ) ;
   Test(Arg1,Arg2) ;
end gcd_Test ;
