with Ada.Text_Io; use Ada.Text_Io ;
with numtest_cli ;
with Integer_Numbers_Pkg ;
package body numtest_Pkg is        -- [Pkg/$_Pkg]
   procedure Implementation (num : integer ) is
   begin
     Put("----- Number : ");
     Put(Integer'Image(num)) ;
     Put_Line(" ----------------------------------------");
     if numtest_cli.ShowDigits
     then
       declare
         digs : Integer_Numbers_Pkg.Num_Pkg.Vector := Integer_Numbers_Pkg.DigitsOf(num) ;
       begin
         put("DigitsOf => ") ;
         Integer_Numbers_Pkg.Show(digs);
       end ;
     end if ;
     if numtest_cli.ShowDivisors
     then
       declare
         divisors : Integer_Numbers_Pkg.Num_Pkg.Vector := Integer_Numbers_Pkg.Divisors(num);
       begin
         put("DivisorsOf =>");
         Integer_Numbers_Pkg.Show(divisors);
       end ;
     end if ;
     if numtest_cli.ShowFactors
     then
       declare
         factors : Integer_Numbers_Pkg.num_pkg.vector := Integer_Numbers_Pkg.Factors(num);
       begin
         put("FactorsOf =>");
         Integer_Numbers_Pkg.Show(factors);
       end ;
     end if;
     if numtest_cli.ShowCoPrimes
     then
       declare
         CoPrimes : Integer_Numbers_Pkg.num_pkg.vector := Integer_Numbers_Pkg.CoPrimes(num);
       begin
         put("CoPrimesOf =>");
         Integer_Numbers_Pkg.Show(CoPrimes);
       end ;
     end if;
     if numtest_cli.PrimeTest
     then
       Put("Prime ");
       if Integer_Numbers_Pkg.prime(num)
       then
         put("True");
       else
         put("False");
       end if ;
       new_line ;
     end if ;

     if numtest_cli.PerfectTest
     then
       Put("Perfect ");
       if Integer_Numbers_Pkg.perfect(num)
       then
         put("True");
       else
         put("False");
       end if ;
       new_line ;
     end if ;

     if numtest_cli.KaprekarTest
     then
       put("Kaprekar ");
       if Integer_Numbers_Pkg.Kaprekar(num)
       then
         put("True");
       else
         put("False");
       end if ;
       new_line ;
     end if ;

     if numtest_cli.TrimorphicTest
     then
       put("Trimorphic ");
       if Integer_Numbers_Pkg.trimorphic(num)
       then
         put("True");
       else
         put("False") ;
       end if ;
       new_line ;
     end if ;
     if numtest_cli.CalculateTotient
     then
       Put("Totient ");
       Put(Integer'image(Integer_Numbers_Pkg.totient(num)));
       New_line ;
     end if ;
   end Implementation ;
   procedure Implementation (num1 : integer; num2 : integer ) is
   begin
     Put("----- Numbers : ");
     Put(Integer'Image(num1)) ;
     Put(" & ");
     Put(Integer'Image(num2)) ;
     Put_Line(" ----------------------------------------");
     if numtest_cli.CoPrimeTest
     then
       put("CoPrime ");
       if Integer_Numbers_Pkg.CoPrime(num1,num2)
       then
         put("True");
       else
         put("False");
       end if ;
       New_line;
     end if ;

     if numtest_cli.CalculateGCD
     then
       declare
         gcd : integer := Integer_Numbers_Pkg.gcd(num1,num2);
       begin
         Put("GCD ");
         PUT(Integer'image(gcd));
         new_line ;
       end ;
     end if ;
     if numtest_cli.CalculateLCM
     then
       declare
         lcm : integer := Integer_Numbers_Pkg.lcm(num1,num2);
       begin
         Put("LCM ");
         PUT(Integer'image(lcm));
         new_line ;
       end ;
     end if ;

   end Implementation ;
end numtest_Pkg ;                  -- [Pkg/$_Pkg]
