with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package numtest_cli is                                       -- [cli/$_cli]

   VERSION : string := "V01" ;
   NAME : String := "numtest" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose

   ShowDigits,
   ShowDivisors,
   ShowFactors,
   ShowCoPrimes,
   PrimeTest,
   PerfectTest,
   KaprekarTest,
   TrimorphicTest,
   CoPrimeTest,
   CalculateGCD,
   CalculateLCM,
   CalculateTotient : aliased Boolean ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

end numtest_cli ;                                            -- [cli/$_cli]
