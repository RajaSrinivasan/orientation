with numbers_Tests ;
with numbers_Suite ;
with AUnit.Test_Suites ; use AUnit.Test_Suites ;
with AUnit.Run ;
with AUnit.Reporter.Text ;

procedure numbers_Unit_Test is
    procedure Run is new AUnit.Run.Test_Runner( numbers_Suite.Suite );
    Reporter : AUnit.Reporter.Text.Text_Reporter ;
begin
    Run(Reporter);
end numbers_Unit_Test ;
