with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with numtest_cli ;                                          -- [cli/$_cli] ;
package numtest_pkg is                                       -- [pkg/$_pkg]
    procedure Implementation (num : integer ) ;
    procedure Implementation (num1 : integer; num2 : integer );
end numtest_pkg ;                                            -- [pkg/$_pkg]
