with Text_Io ; use Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

with Ada.Command_Line ;

with Integer_Numbers_Pkg ;

procedure Numbers_Test is
   Arg : Integer ;
   procedure Show( NumPtr : Integer_Numbers_Pkg.Num_Pkg.Cursor ) is
   begin
      Put( Integer'Image(Integer_Numbers_Pkg.Num_Pkg.Element(NumPtr))) ;
      Put( " , " ) ;
   end Show ;

   procedure Show( Vec : Integer_Numbers_Pkg.Num_Pkg.Vector ) is
   begin
      Put("[");
      Integer_Numbers_Pkg.Num_Pkg.Iterate( Vec , Show'Access ) ;
      Put_Line("]");
   end Show ;

   procedure Test( Arg : Integer ) is
   begin
      Put("------");
      Put(Integer'Image(Arg));
      Put_Line("----------------------------------------------------------");
      declare
         Digitsof : Integer_Numbers_Pkg.Num_Pkg.Vector := Integer_Numbers_Pkg.DigitsOf( Arg ) ;
         Divisors : Integer_Numbers_Pkg.Num_Pkg.Vector := Integer_Numbers_Pkg.Divisors( Arg ) ;
         Factors : Integer_Numbers_Pkg.Num_Pkg.Vector := Integer_Numbers_Pkg.Factors( Arg ) ;
      begin
         Put("DigitsOf "); Show(DigitsOf) ;
         Put("ValueOfDigits "); Put(Integer'Image(Integer_Numbers_Pkg.ValueOfDigits(DigitsOf))); New_Line ;
         Put("Divisors "); Show(Divisors) ;
         Put("Factors  "); Show(Factors) ;
         Put("ValueOfFactors "); Put(Integer'Image(Integer_Numbers_Pkg.ValueOfFActors(Factors))) ; New_Line ;
         Put("DivisorsSigma 1"); Put(Integer'Image(Integer_Numbers_Pkg.DivisorsSigma(Arg,1))); New_Line ;
         Put("DivisorsSigma 3"); Put(Integer'Image(Integer_Numbers_Pkg.DivisorsSigma(Arg,3))); New_Line ;

         if Integer_Numbers_Pkg.Prime( Arg )
         then
            Put_Line("Is a prime");
         else
            Put_Line("Is not a prime");
         end if ;
         if Integer_Numbers_Pkg.Perfect(Arg)
         then
            Put_Line("Is perfect");
         else
            Put_Line("Is not perfect") ;
         end if ;
         if Integer_Numbers_Pkg.Kaprekar(Arg)
         then
            Put_Line("Is a Kaprekar number");
         else
            Put_Line("Is not a Kaprekar number");
         end if ;
         if Integer_Numbers_Pkg.Trimorphic(Arg)
         then
           Put_Line("Is Trimorphic");
         else
           Put_Line("Is not Trimorphic");
         end if ;
      end ;
   end Test ;
begin
   for Argn in 1..Ada.Command_Line.Argument_Count
   loop
      Arg := Integer'Value( Ada.Command_Line.Argument(Argn) ) ;
      Test(Arg) ;
   end loop ;
end Numbers_Test ;
