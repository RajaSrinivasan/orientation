# Project: numbers

## Objective
Explore: Data Structures
         Integer computation
## Usage:

```

../bin/numtest -h
numtest V01 2017-12-08 14:19:53
Usage: numtest numtest [tests] num1 [num2]

 -v, --verbose           Output extra verbose information
 -showg                  print decimal digits
 -showv                  print divisors of
 -showf                  print factors of
 -showcp                 show co primes less than of
 -testp                  is number prime
 -testf                  is number perfect
 -testk, --kaprekar-test is number a kaprekar number
 -testt, --trimorphic    is number trimorphic
 -testcp, --test-coprime are numbers co prime
 -calcg                  calculate g => gcd
 -calct                  Calculate Euler totient
 -showA                  Equivalent to -showg -showv -showf -showcp
 -testA                  Equivalent to -testp -testf -testk -testt -testcp
 -calcA                  Equivalent to -calcg -calct

'''
## Example
'''
../bin/numtest -calcA -showA -testA 12 17 132
----- Number :  12 ----------------------------------------
DigitsOf =>  1,  2
DivisorsOf => 1,  2,  3,  4,  6,  12
FactorsOf => 2,  2,  3
CoPrimesOf => 5,  7,  11
Prime False
Perfect False
Kaprekar False
Trimorphic False
Totient  3
----- Number :  17 ----------------------------------------
DigitsOf =>  1,  7
DivisorsOf => 1,  17
FactorsOf => 17
CoPrimesOf => 2,  3,  4,  5,  6,  7,  8,  9,  10,  11,  12,  13,  14,  15,  16
Prime True
Perfect False
Kaprekar False
Trimorphic False
Totient  15
----- Numbers :  12 &  17 ----------------------------------------
CoPrime True
GCD  1
----- Number :  132 ----------------------------------------
DigitsOf =>  1,  3,  2
DivisorsOf => 1,  2,  3,  4,  6,  11,  12,  22,  33,  44,  66,  132
FactorsOf => 2,  2,  3,  11
CoPrimesOf => 5,  7,  13,  17,  19,  23,  25,  29,  31,  35,  37,  41,  43,  47,  49,  53,  59,  61,  65,  67,  71,  73,  79,  83,  85,  89,  91,  95,  97,  101,  103,  107,  109,  113,  115,  119,  125,  127,  131
Prime False
Perfect False
Kaprekar False
Trimorphic False
Totient  39
'''
