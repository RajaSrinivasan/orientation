with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with GNAT.Source_Info ;

with numtest_cli ;                            -- [cli/$_cli]
with numtest_Pkg ;                            -- [Pkg/$_Pkg]
procedure numtest is                  -- [clitest/$]
begin
   numtest_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if numtest_cli.Verbose                     -- [cli/$_cli]
   then
      numtest_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;
   loop
      declare
        num1 : String := numtest_cli.GetNextArgument ;
      begin
        numtest_pkg.Implementation( Integer'Value(num1)) ;
        declare
          num2 : String := numtest_cli.GetNextArgument ;
        begin
          numtest_pkg.Implementation( Integer'Value(num2));
          numtest_pkg.Implementation( Integer'Value(num1) , Integer'Value(num2)) ;
        end ;
        Exception
          when others => exit ;
      end ;
   end loop;
end numtest ;                         -- [clitest/$]
