
with Ada.Numerics.Elementary_Functions ;

with Ada.Text_Io ; use Ada.Text_Io ;
with Ada.Integer_Text_Io ; use Ada.Integer_Text_Io ;
with Ada.Float_Text_Io ; use Ada.Float_Text_Io ;

with AUnit.Assertions ; use AUnit.Assertions ;

with Integer_Numbers_Pkg ;

package body numbers_tests is

    procedure Register_Tests( T : in out numbers_Test ) is
       use AUnit.Test_Cases.Registration ;
    begin
        Register_Routine( T , Test_Kaprekar'access , "Test Kaprekar");
        Register_Routine( T , Test_Amicable'access , "Test Amicable ") ;
    end Register_Tests ;

    function Name( T : numbers_Test ) return Message_String is
    begin
        return Format("numbers Tests") ;
    end Name ;

    procedure Test_Kaprekar( T : in out Test_Cases.Test_Case'Class ) is
    begin
      AUnit.Assertions.Assert( Integer_Numbers_Pkg.Kaprekar(7777)
                            , "Kaprekar Number 7777" );
      AUnit.Assertions.Assert( not Integer_Numbers_Pkg.Kaprekar(19999)
                            , "Kaprekar Number 19999" );
      AUnit.Assertions.Assert( Integer_Numbers_Pkg.Kaprekar(22222)
                            , "Kaprekar Number 22222" );

    end Test_Kaprekar ;

    procedure Test_Amicable( T : in out Test_Cases.Test_Case'Class ) is
    begin
        AUnit.Assertions.Assert( Integer_Numbers_Pkg.Amicable(220,284)
             , "Amicable Numbers 220,284 " );
        AUnit.Assertions.Assert( Integer_Numbers_Pkg.Amicable(66928, 66992)
                  , "Amicable Numbers 66928, 66992" );
        AUnit.Assertions.Assert( not Integer_Numbers_Pkg.Amicable(32768, 65536)
                            , "Amicable Numbers 32768, 65536" );
    end Test_Amicable ;

end numbers_tests ;
