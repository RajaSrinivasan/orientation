# Project : jsonutil

## Objective :

      Further exploration of JSON processing

## Requirements :
In this project, we build a tool - a preprocessor framework for JSON files.

### Preprocessor option

```
jsonutil input.json output.json
```
Some projects
(Web development for example in Angular) require configurations in JSON format which may get huge and unmanageable.

This preprocessor scans string values in the JSON file and when they are recognized as
macros, replaces the value with the results of executing the macro. Following macros are
supported:

Macro Spec | Example | Description
------------------------- | ---------------------------- | ------------------------------------------------
$Import(filename) | $Import(file1.json) | The contents of file1.json are inserted in place  


In the above example, input.json is analyzed and output.json is created. Any strings
with a value $Import(a.json) will be replaced with the contents of a.json.

### Split Option


```
jsonutil --split input.json -objects obj1 obj2 obj3
```

This is the inverse of the preprocessor option above. This takes a big json file
and splits into many smaller files. The objects to be extracted are provided as command line
arguments. for each object a json file is created and the entire object in the json
file is replaced with a $Import macro. In the above example input.json is
split into input_top.json input_obj1.json input_obj2.json and input_obj3.json.

## Usage :

```
../bin/jsonutil -h
jsonutil V01 2018-01-13 16:28:12
Usage: jsonutil jsonutil [switches] inputfilename [outputfilename] [-objects obj1 obj2]

 -v, --verbose Output extra verbose information
 -s, --split   Extract nodes into separate json files

```
