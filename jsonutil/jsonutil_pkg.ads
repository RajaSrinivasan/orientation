with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;
with GNATCOLL.JSON ; use GNATCOLL.JSON;
                                        -- [cli/$_cli] ;
package jsonutil_pkg is                                       -- [pkg/$_pkg]
    package JSON Renames GNATCOLL.JSON ;
    Verbose : boolean := false ;
    procedure Impl_Preprocessor (inputfilename : string ;
                                outputfilename : string );
    procedure Impl_Split( inputfilename : string ;
                          outputfilename : string ;
                          objectnames : access GNAT.Strings.String_List );
    procedure Macro( name : string ;
                     arg : string ;
                     result : in out JSON.JSON_Value );
    pragma Precondition( JSON.Kind(result) = JSON.JSON_Object_Type );

end jsonutil_pkg ;                                            -- [pkg/$_pkg]
