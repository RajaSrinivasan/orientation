with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;

with GNAT.Strings ;
with jsonutil_cli ;                            -- [cli/$_cli]
with jsonutil_Pkg ;                            -- [Pkg/$_Pkg]
procedure jsonutil is                  -- [clitest/$]
   objlist : access GNAT.Strings.String_List ;
begin
   jsonutil_cli.ProcessCommandLine ;           -- [cli/$_cli]
   jsonutil_cli.GetInputOutputFilenames ;
   jsonutil_cli.GetObjectNames ;
   if jsonutil_cli.Verbose                     -- [cli/$_cli]
   then
      jsonutil_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;
   if not jsonutil_cli.ValidateInputOutputSpecs
   then
       Put_Line("Please provide an input and an output file name");
       return ;
   end if ;

   jsonutil_pkg.Verbose := jsonutil_cli.Verbose ;
   if jsonutil_cli.SplitOption
   then
     objlist := new GNAT.Strings.String_List'(jsonutil_cli.Objects) ;
     jsonutil_pkg.Impl_Split( jsonutil_cli.InputFileName.all ,
                              jsonutil_cli.OutputFileName.all ,
                              objlist ) ;
   else
     jsonutil_pkg.Impl_Preprocessor( jsonutil_cli.InputFileName.all ,
                                     jsonutil_cli.OutputFileName.all );
   end if ;
end jsonutil ;                         -- [clitest/$]
