with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with GNAT.Strings ;

package jsonutil_cli is                                       -- [cli/$_cli]

   VERSION : string := "V01" ;
   NAME : String := "jsonutil" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose
   SplitOption : aliased boolean := false ;

   MAX_NO_OBJECTS : constant := 16;
   Objects : aliased gnat.strings.String_List(1..MAX_NO_OBJECTS) ;

   InputFileName : GNAT.Strings.String_Access ;
   OutputFileName : GNAT.Strings.String_Access;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;
   procedure GetInputOutputFilenames ;
   procedure GetObjectNames ;
   function ValidateInputOutputSpecs return boolean ;

end jsonutil_cli ;                                            -- [cli/$_cli]
