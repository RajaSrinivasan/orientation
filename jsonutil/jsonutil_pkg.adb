with Ada.Text_Io; use Ada.Text_Io;

with Gnat.Regpat ;
with GNATCOLL.JSON ;
with GNAT.Directory_Operations ;
with ctools ;
with jsontools ;

package body jsonutil_Pkg is        -- [Pkg/$_Pkg]

   use GNAT ;
   package UBS renames Ada.Strings.Unbounded ;

   procedure Process is new JSON.Gen_Map_JSON_Object( JSON.JSON_Value );

   type SplitContext_Type is
     record
         parentName : Unbounded_String ;
         Objects : access GNAT.Strings.String_List ;
         output : JSON.JSON_Value ;
     end record ;

   procedure Split is new JSON.Gen_Map_JSON_Object( SplitContext_Type );
   procedure Process( User_Object : in out JSON.JSON_Value ;
                      Name : JSON.UTF8_String ;
                      Value : JSON.JSON_Value ) ;

   procedure Split( User_Object : in out SplitContext_Type ;
                    Name : JSON.UTF8_String ;
                    Value : JSON.JSON_Value ) ;

   Macro_Pattern : Gnat.Regpat.Pattern_Matcher (1024) ;
   Macro_Regexp : string := "\$(.*)\((.*)\)" ;
   MACRO_ERROR : exception ;
   procedure Macro( name : string ;
                    arg : string ;
                    result : in out JSON.JSON_Value ) is
   begin
     if Verbose
     then
       put("Processing Macro ");
       put(name);
       put(" with argument ");
       put_line(arg);
     end if;
     if name = "Import"
     then
        declare
          imported : JSON.JSON_Value := JSON.Create_Object ;
        begin
          jsontools.Load( imported , arg );
          Process( imported , Process'access , result );
          put("Imported ");
          put_line(arg);
        exception
          when others=>
             put("Failed to import ");
             put_line(arg);
             raise MACRO_ERROR ;
        end ;
     else
         put("Unrecognized macro "); put_line(name);
         raise MACRO_ERROR ;
     end if ;
   end Macro ;

   procedure Process( User_Object : in out JSON.JSON_Value ;
                      Name : JSON.UTF8_String ;
                      Value : JSON.JSON_Value ) is
      use GNATCOLL.JSON ;
      use GNAT.RegPat ;
   begin
     case JSON.Kind (Val => Value) is
        when JSON_Null_Type |
             JSON_Boolean_Type |
             JSON_Int_Type |
             JSON_Float_Type =>
               if Verbose
               then
                 Put_Line("Processing " & Name);
               end if ;
               JSON.Set_Field( User_Object , Name , JSON.Clone(Value) );
        when JSON_String_Type =>
              declare
                matches : RegPat.Match_Array(0 .. RegPat.Paren_Count( Macro_Pattern ));
                val : string := JSON.Get(Value);
                modified : JSON.JSON_Value := JSON.Create_Object ;
              begin
                if Verbose
                then
                  put("String value "); put_line(val);
                end if ;
                Regpat.Match( macro_pattern , val , matches );
                if Matches(0) /= RegPat.No_Match
                then
                  if Verbose
                  then
                      for m in matches'range
                      loop
                        put("Match "); put_line( val(matches(m).first .. matches(m).last) );
                      end loop;
                  end if ;
                  begin
                      Macro( val(matches(1).First .. matches(1).Last) ,
                             val(matches(2).First .. matches(2).Last) ,
                             modified );
                      JSON.Set_Field( User_Object , Name , modified );
                  exception
                    when others =>
                      JSON.Set_Field( User_Object , Name , JSON.Clone(Value) );
                  end ;
                else
                   JSON.Set_Field( User_Object , Name , JSON.Clone(Value) );
                end if ;
              end ;
        when JSON_Array_Type =>
          declare
             A_JSON_Array : constant JSON_Array := Get (Val => Value);
             A_JSON_Value : JSON_Value;
             OUT_JSON_Value : JSON_Value ;
             Array_Length : constant Natural    := Length (A_JSON_Array);
             Out_JSON_Array : JSON_Array ;
          begin
            if Verbose
            then
               Put_Line("Array of length " & natural'image(Array_Length));
             end if;
             for J in 1 .. Array_Length
             loop
                A_JSON_Value := Get (Arr => A_JSON_Array, Index => J);
                if  Kind(A_JSON_Value) = JSON_Object_Type
                then
                   declare
                     newobj : JSON.JSON_Value := JSON.Create_Object ;
                   begin
                      Process( A_JSON_Value , Process'access , newobj );
                      JSON.Append( OUT_JSON_ARRAY , newobj) ;
                   end ;
                else
                   JSON.Append( OUT_JSON_Array , A_JSON_Value );
                end if ;
             end loop;
             JSON.Set_Field(User_Object , Name , OUT_JSON_Array ) ;
          end;
        when JSON_Object_Type =>
          declare
            newobj : JSON.JSON_Value := JSON.Create_Object ;
          begin
            Process( Value , Process'access , newobj );
            JSON.Set_Field( User_Object , name , newobj );
          end ;
        when others =>
        Put_Line("Dont know how to handle " & Name & " type " &
                      JSON.JSON_Value_Type'Image(Kind(Value))) ;
     end case ;
   end Process ;

   procedure Impl_Preprocessor (inputfilename : string ;
                                outputfilename : string ) is
      inp : JSON.JSON_Value ;
      outp : JSON.JSON_Value := JSON.Create_Object ;
   begin
      if Verbose
      then
        Put("Input File : "); Put_Line(inputfilename);
        Put("Output File : "); Put_Line(outputfilename);
      end if ;
      jsontools.Load(  inp , inputfilename );
      Process( inp , Process'access , outp );
      jsontools.Save( outp , outputfilename );

      Put("Saved ");
      Put_Line(outputfilename);

   end Impl_Preprocessor ;
   procedure DetermineFileName( base : string ;
                                available : out unbounded_string ) is
      function Exists( name : string ) return boolean is
        file : file_type ;
      begin
        Open( file , in_file , name );
        close( file );
        return true ;
        exception
          when others => return false ;
      end Exists ;
   begin
      if Exists( base & ".json")
      then
        for i in 1..32
        loop
            declare
              propfilename : string := base & ctools.Image(i,"_%02d.json");
            begin
                if not Exists( propfilename )
                then
                  available := To_Unbounded_String( propfilename );
                  return ;
                end if ;
            end ;
        end loop;
        raise PROGRAM_ERROR ;
      else
        available := To_Unbounded_String( base & ".json");
      end if ;
   end DetermineFileName ;

   procedure Split( User_Object : in out SplitContext_Type ;
                    Name : JSON.UTF8_String ;
                    Value : JSON.JSON_Value ) is
      use GNAT.Strings ;
      function Extract( objname : string ) return boolean is
      begin
          for obj of User_Object.Objects.all
          loop
            if verbose
            then
              Put_Line(obj.all);
            end if ;
            if obj /= null
            then
               if objname = obj.all
               then
                  put_line("Extract " & Name);
                  return true ;
              end if ;
            else
              return false ;
            end if ;
          end loop ;
          return false ;
      end Extract ;
   begin
     case Kind( Value ) is
          when JSON_Null_Type |
               JSON_Boolean_Type |
               JSON_Int_Type |
               JSON_Float_Type |
               JSON_String_Type =>
               JSON.Set_Field( User_Object.Output , Name , JSON.Clone(Value) );
          when JSON_Array_Type =>
               --if not extract(Name)
               --then
               --else
                  declare
                    A_JSON_Array : constant JSON_Array := Get (Val => Value);
                    A_JSON_Value : JSON_Value;
                    OUT_JSON_Value : JSON_Value ;
                    Array_Length : constant Natural    := Length (A_JSON_Array);
                    Out_JSON_Array : JSON_Array ;
                    extracted : SplitContext_Type := User_Object ;
                    replobj : JSON_Value := JSON.Create_Object ;
                    filename : unbounded_string ;
                  begin
                    if Verbose
                    then
                      Put("Array "); Put(Name);
                      Put_Line(" of length " & natural'image(Array_Length));
                    end if;
                      for J in 1 .. Array_Length
                      loop
                          A_JSON_Value := Get (Arr => A_JSON_Array, Index => J);
                          if Kind(A_JSON_Value) = JSON_Array_Type or 
                             Kind(A_JSON_Value) = JSON_Object_Type
                            then
                              extracted.parentName := To_Unbounded_String( to_string(User_Object.parentName)
                                                                          & "_"
                                                                          & Name ) ;
                              extracted.output := JSON.Create_Object ;
                              Split( A_JSON_Value , Split'access , extracted );
                              JSON.Append( OUT_JSON_Array , extracted.output );
                            else
                              JSON.Append( OUT_JSON_Array , A_JSON_Value );
                            end if ; 
                      end loop;
                      JSON.Set_Field(User_Object.output , Name , OUT_JSON_Array ) ;
                  end;
                -- end if ;
          when JSON_Object_Type =>
               if Verbose
               then
                 put("Object "); Put_Line(Name) ;
               end if ;
               if Extract(Name)
               then
                  declare
                    extracted : SplitContext_Type := User_Object ;
                    replobj : JSON_Value := JSON.Create_Object ;
                    filename : unbounded_string ;
                  begin
                    extracted.parentName := To_Unbounded_String( to_string(User_Object.parentName)
                                                                 & "_"
                                                                 & Name ) ;
                    extracted.output := JSON.Create_Object ;
                    Split( Value , Split'access , extracted );
                    DetermineFileName( to_string(extracted.parentName) , filename );
                    jsontools.Save( extracted.output , to_string(filename) );
                    put("Saved ");
                    Put_Line( to_string(filename) );
                    replobj := Create( "$Import(" &
                                       to_string(filename) & ")" );
                    JSON.Set_Field( User_Object.output , Name , replobj );
                  end ;
               else
                 declare
                   extracted : SplitContext_Type := User_Object ;
                   filename : unbounded_string ;
                 begin
                   extracted.parentName := To_Unbounded_String( to_string(User_Object.parentName)
                                                                & "_"
                                                                & Name ) ;
                   extracted.output := JSON.Create_Object ;
                   Split( Value , Split'access , extracted );
                   JSON.Set_Field( User_Object.output , Name , extracted.output );
                 end ;
               end if ;
          when others =>
            Put_Line("Dont know how to handle " & Name & " type " &
                          JSON.JSON_Value_Type'Image(Kind(Value))) ;
     end case ;
   end Split ;

   procedure Impl_Split( inputfilename : string ;
                         outputfilename : string ;
                         objectnames : access GNAT.Strings.String_List ) is
     context : SplitContext_Type ;
     inp : JSON_Value := JSON.Create_Object ;
     outp : JSON_Value := JSON.Create_Object ;
   begin
     context.parentName := UBS.To_Unbounded_String(
                               GNAT.Directory_Operations.base_name(inputfilename,".json"));
     context.objects := objectnames ;
     context.output := outp ;
     jsontools.load( inp , inputfilename );
     Split( inp , Split'access , context );
     jsontools.Save( context.output , outputfilename );

     Put("Saved ");
     Put_Line( OutputFileName );

   end Impl_Split ;
begin
  Regpat.Compile( Macro_Pattern , Macro_Regexp );
end jsonutil_Pkg ;                  -- [Pkg/$_Pkg]
