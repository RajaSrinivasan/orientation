with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body jsonutil_cli is                          -- [cli/$_cli]

    package GCL renames GNAT.Command_Line ;

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
        GCL.Set_Usage( Config ,
                       Help => NAME & " " &
                       VERSION & " " &
                       Compilation_ISO_Date & " " &
                       Compilation_Time ,
                       Usage => "jsonutil [switches] inputfilename [outputfilename] [-objects obj1 obj2]");
        GCL.Define_Switch (Config,
                       verbose'access ,
                       Switch => "-v",
                       Long_Switch => "--verbose",
                       Help => "Output extra verbose information");
        GCL.Define_Switch( Config,
                       SplitOption'access ,
                       Switch => "-s" ,
                       Long_Switch => "--split" ,
                       Help => "Extract nodes into separate json files") ;
        GCL.Define_Section( Config ,
                      "objects" );
        GCL.Getopt(config,SwitchHandler'access);
    exception
        when GCL.EXIT_FROM_COMMAND_LINE =>
             null ;
    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GCL.Get_Argument(Do_Expansion => False) ;
    end GetNextArgument ;

    procedure GetObjectNames is
    begin
       GCL.Goto_Section("objects") ;
       for obj in 1..MAX_NO_OBJECTS
       loop
         Objects(obj) := new String'(GetNextArgument);
       end loop;
    end GetObjectNames ;

    procedure GetInputOutputFilenames is
    begin
        GCL.Goto_Section("") ;
        InputFileName := new String'(GetNextArgument);
        OutputFileName := new String'(GetNextArgument);
    exception
        when others => null ;
    end GetInputOutputFilenames ;

    procedure Show(name : string ; val : boolean ) is
    begin
       Put(name) ; Put( " => ");
       put(boolean'image(val));
       new_line ;
     end Show ;
     procedure Show(name : string ; val : GNAT.strings.string_access) is
       use GNAT.Strings ;
     begin
       put(name) ;
       put(" => ");
       if val /= null
       then
          put(val.all) ;
       else
          put("unspecified");
       end if ;
       new_line ;
     end Show ;

    procedure ShowCommandLineArguments is
    begin
       Show("Verbose" , Verbose);
       Show("SplitOption" , SplitOption);
       Show("InputFileName" , InputFileName );
       Show("OutputFileName" , OutputFileName );
       Put_Line("Objects:");
       for obj in Objects'range
       loop
         Show(Integer'Image(obj) & " => " , Objects(obj));
       end loop;
    end ShowCommandLineArguments ;
   function ValidateInputOutputSpecs return boolean is
     use GNAT.Strings ;
     function validate( str : GNAT.Strings.String_Access ) return boolean is
     begin
       if str = null
       then
         return false ;
       elsif str'length < 1
       then
         return false ;
       end if ;
       return true ;
     end ;
   begin
     if validate(InputFileName)
     and then validate(OutputFileName)
     then
       return true ;
     end if ;
     return false ;
   end ValidateInputOutputSpecs ;
end jsonutil_cli ;                                   -- [cli/$_cli]
