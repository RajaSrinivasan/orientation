with Interfaces.C ; use Interfaces.C ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with bullsncows_cli ;                                          -- [cli/$_cli] ;
package bullsncows_pkg is                                       -- [pkg/$_pkg]
   subtype Digit_Type is Integer range 0..9 ;
   type Puzzle_Digits_Type is array (Positive range <>) of Digit_Type ;
   type Puzzle_Type is access all Puzzle_Digits_Type ;
   function Create(Size : Integer := 4) return Puzzle_Type ;
   type Score_Type is
      record
         Cows : Integer ;
         Bulls : Integer ;
      end record ;
   BAD_SOLUTION : exception ;
   function Check_Solution( Puzzle : Puzzle_Type ;
                            Guess : Puzzle_Digits_Type )
                          return Score_Type ;
   function Check_Solution( Puzzle : Puzzle_Type ;
                            Guess : Unsigned )
                          return Score_Type ;

end bullsncows_pkg ;                                            -- [pkg/$_pkg]
