with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package bullsncows_cli is                                       -- [cli/$_cli]
   VERSION : string := "Template_V01" ;
   NAME : String := "Utility" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose
   Puzzlesize : aliased Integer ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
    procedure ShowCommandLineArguments ;
end bullsncows_cli ;                                            -- [cli/$_cli]
