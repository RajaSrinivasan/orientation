with Interfaces ; use Interfaces ;
with Interfaces.C ; use Interfaces.C ;
with Ada.Text_Io; use Ada.Text_Io ;

with Gnat.Random_Numbers ;
with Bullsncows_Cli ;

package body bullsncows_Pkg is        -- [Pkg/$_Pkg]
   procedure Show( Puzzle : Puzzle_Type ) is
   begin
      for D in Puzzle.all'Range
      loop
         Put(Digit_Type'Image(Puzzle(D))) ; Put(" ,") ;
      end loop ;
      New_Line ;
   end Show ;

   Gen : Gnat.Random_Numbers.Generator ;
   function Create(Size : Integer := 4) return Puzzle_Type is
      Result : Puzzle_Type := new Puzzle_Digits_Type(1..Size);
      Used : array( Digit_Type'Range ) of Boolean := (others => False );
      procedure Choose_Digit( D : Digit_Type ) is
         Candidate : Interfaces.Unsigned_32 ;
         Nextdig : Digit_Type ;
      begin
         loop
            Candidate := Gnat.Random_Numbers.Random( Gen ) ;
            Nextdig := Digit_Type( Candidate mod 10 ) ;
            if not Used(Nextdig)
            then
               Result(D) := Nextdig ;
               Used(Nextdig) := True ;
               return ;
            end if ;
         end loop ;
      end Choose_Digit ;
   begin
      GNAT.Random_Numbers.Reset( Gen ) ;
      Result.all := ( others => Digit_Type'First) ;
      for D in 1..Size
      loop
         Choose_Digit( D ) ;
      end loop;
      if Bullsncows_Cli.Verbose
      then
         Put("Puzzle generated : ");
         Show(Result) ;
      end if ;
      return Result ;
   end Create ;

   function Check_Solution( Puzzle : Puzzle_Type ;
                            Guess : Puzzle_Digits_Type )
                          return Score_Type is
      Result : Score_Type ;
      function Check_Cow( D : Digit_Type ; excIdx : Integer ) return Boolean is
      begin
         for Idx in Puzzle.all'Range
         loop
            if Idx = Digit_Type(Excidx)
            then
               null ;
            else
               if Puzzle.all(Idx) = D
               then
                  return True ;
               end if ;
            end if ;
         end loop ;
         return False ;
      end Check_Cow ;
   begin
      if Guess'Length /= Puzzle.all'Length
      then
         raise BAD_SOLUTION ;
      end if ;
      Result := ( Cows => 0 , Bulls => 0) ;
      for Idx in Puzzle.all'Range
      loop
         if Puzzle.all(Idx) = Guess(Idx)
         then
            Result.Bulls := Result.Bulls + 1 ;
         elsif Check_Cow( Guess(Idx) , Idx )
         then
            Result.Cows := Result.Cows + 1 ;
         end if ;
      end loop ;

      return Result ;
   end Check_Solution ;
   function Check_Solution( Puzzle : Puzzle_Type ;
                            Guess : Unsigned )
                          return Score_Type is
      Actualguess : Puzzle_Digits_Type (Puzzle.all'Range) ;
      Used : array( Digit_Type'Range ) of Boolean := (others => False) ;
      Guessleft : Unsigned := Guess ;
   begin
      for D in reverse Actualguess'Range
      loop
         Actualguess(D) := Digit_Type(Guessleft mod 10) ;
         if Used( Actualguess(D) )
         then
            raise BAD_SOLUTION ;
         end if ;
         Used(Actualguess(D)) := True ;
         Guessleft := Guessleft / 10 ;
      end loop ;
      return Check_Solution(Puzzle,Actualguess) ;
   end Check_Solution ;
end bullsncows_Pkg ;                  -- [Pkg/$_Pkg]
