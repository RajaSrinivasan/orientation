with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with Ada.IO_Exceptions ;
with GNAT.Source_Info ;
with Interfaces.C ; use Interfaces.C ;

with bullsncows_cli ;                            -- [cli/$_cli]
with bullsncows_Pkg ;                            -- [Pkg/$_Pkg]
with Getcmdline ;

procedure bullsncows is                  -- [clitest/$]
   P : Bullsncows_Pkg.Puzzle_Type ;
begin
   bullsncows_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if bullsncows_cli.Verbose                     -- [cli/$_cli]
   then
      bullsncows_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;

   <<MAIN_LOOP>>
   loop
      P := Bullsncows_Pkg.Create( Bullsncows_Cli.Puzzlesize )  ;
      loop
         declare
            guessstr : String := Getcmdline.Get_Line ("Guess > ");
            Myguess : Unsigned ;
            Score : Bullsncows_Pkg.Score_Type ;
         begin
            Myguess := Unsigned'Value(Guessstr) ;
            if Myguess = 0
            then
               exit;
            end if ;
            Score := Bullsncows_Pkg.Check_Solution(P,Myguess) ;
            Put("Bulls : "); Put( Integer'Image(Score.Bulls) ) ;
            Put(" Cows : "); Put( Integer'Image(Score.Cows)) ;
            New_Line ;
         exception
            when Bullsncows_Pkg.BAD_SOLUTION =>
               null ;
         end ;
      end loop ;
   end loop ;
exception
   when Ada.IO_EXCEPTIONS.END_ERROR =>
      return ;
end bullsncows ;                         -- [clitest/$]
