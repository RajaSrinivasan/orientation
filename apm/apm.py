#!/usr/bin/env python2

import os
import os.path
import sys
import platform
import uuid
import ConfigParser
import shutil
import glob
import hashlib
from datetime import datetime

VERBOSE=True
PASSWORD="apm@abiomed"
HOSTNAME=platform.node()
UUID=uuid.uuid1()
WORKROOT="/tmp/apm"
WORKAREA = "%s/%s" % (WORKROOT,HOSTNAME)
MANIFESTFILE="%s/Manifest" % WORKAREA
ZIPFILE="%s/Package.zip" % WORKAREA
PACKAGEFILE="%s/Package.apm" % WORKAREA
FILESINPACKAGE=[]

print UUID.hex

def Log(msg):
    if VERBOSE:
        print msg

def Help():
    print "usage:"
    print "\tapm.py operation [arguments"
    print "\t    capture\t-t create the personality file for this RLM"
    print "\t    extract\t- extract the apm file"
    print "\t            \t  <package file>"
    print "\t    service\t- create service pack"
    print "\t           \t  <packid> <configfile>"
    print "\t    apply\t- apply the service pack"
    print "\t         \t  <packid>"

def CreateWorkArea():
    if os.path.exists(WORKAREA):
        shutil.rmtree(WORKAREA,True)
    if not os.path.exists(WORKAREA):
        os.makedirs(WORKAREA)
    Log("Workarea %s created" % WORKAREA)

def UpdateManifest(fns):
    global FILESINPACKAGE
    #Log("Updating the Manifest %s files %s" % (MANIFESTFILE,fns))
    filenum=0
    manifestfile=open(MANIFESTFILE,'a')
    manifestfile.write("[files]\n")
    Log("Adding %s " % fns)
    for fn in fns:
        Log("Adding %s" % fn)
        for name in glob.glob(fn):
            attr=os.stat(name)
            md5hash=hashlib.md5(open(name, 'rb').read()).hexdigest()
            manifestfile.write("file%d=%s,%d,%s\n" % (filenum,name,attr.st_size,md5hash))
            filenum = filenum + 1
    manifestfile.flush()
    manifestfile.close()
    AddFile(MANIFESTFILE)

def AddFile(filename,zipfile=ZIPFILE):
    cmd="zip -u -j %s %s" % (ZIPFILE,filename)
    os.system(cmd)

def CreateDefaultPackage(pkgtype="personality",zipfile=ZIPFILE):
    with open(MANIFESTFILE,"w") as manifestfile:
        manifestfile.write("[package]\n")
        manifestfile.write("type=%s\n" % pkgtype)
        manifestfile.write("host=%s\n" % HOSTNAME)
        manifestfile.write("id=%s\n" % (UUID))
        manifestfile.write("created=%s\n" % datetime.ctime(datetime.utcnow()) )
    zipcmd="zip -j %s %s" % (zipfile,MANIFESTFILE)
    os.system(zipcmd)

def EncryptFile(inp,out,password=PASSWORD):
    cmd="openssl aes-256-cbc -pass pass:%s -in %s -out %s" % (password,inp,out)
    Log("Encrypt command : %s" % cmd)
    os.system(cmd)

def DecryptFile(inp,out,password=PASSWORD):
    cmd="openssl aes-256-cbc -d -pass pass:%s -in %s -out %s" % (password,inp,out)
    Log("Decrypt command : %s" % cmd) 
    os.system(cmd)

def ExtractPackage(spfile,target):
    Log("Extract package %s to $s" % (spfile,target))

def CapturePersonality():
    PERSFILES=["/etc/rlm" , 
               "/home/root/.ssh/id_rsa" , 
               "/etc/NetworkManager/system-connections/*" , 
               "/opt/abiomed/rlm/certs/*"]
    PERSFILES=["/Users/RajaSrinivasan/login.sh" , "/Users/RajaSrinivasan/bin/*"]
    Log("Capture personality")
    Log("files: %s" % PERSFILES)
    CreateDefaultPackage()
    for fn in PERSFILES:
        AddFile(fn)
    Log("files: %s" % PERSFILES)
    UpdateManifest(PERSFILES)
    EncryptFile(ZIPFILE,PACKAGEFILE)

def CreateServicePack(spid,config):
    Log("Create Service Pack Id %s from configuration %s" % (spid , config))

def ApplyPackage(spfile):
    Log("Apply Package %s" % spfile )

def AbiomedPackageManager():

    if len(sys.argv) < 2:
        Help()
        return

    operation = sys.argv[1]
    CreateWorkArea()

    if operation == "capture":
        CapturePersonality()
    
    if operation == "service":
        CreateServicePack(sys.argv[2],sys.argv[3])

    if operation == "apply":
        ApplyPackage(sys.argv[2])

    if operation == "extract":
        ExtractPackage(sys.argv[2],sys.argv[3])

    if operation == "help":
        Help()

if __name__ == "__main__":
    AbiomedPackageManager()
