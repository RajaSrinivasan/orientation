# Project: apm

## Synopsis

Abiomed Package Manager (apm) is a utility that performs the following functions

### Personality capture

In this mode, apm captures the "personality" of a system; essentially any configurations that have been performed to the system to render it functional. In the case of the RLM device for example the personality manifests itself in 
the primary configuration file /etc/rlm and a series of wifi configuration files in found in /etc/NetworkManager/system-connections.

The personality is captured with the intention to restore when the software is
upgraded for instance with a new release.

Maintenance mode of the RLM will provide a way to capture the personality and download the "personality" file.

### Personalize

In this mode, a user can upload a personality file using a maintenance mode option and have the personality be applied.

### Service Pack Create

In this mode, a service pack is created that can be applied to many RLMs. Typically this would include key executables such as rlc/rt/rlmadmin. This may also include a script execution.

### Service Pack Install

In this mode, the service pack create above can be applied.

## Usage:

   python apm.py [app] [options] [argments]

### Examples

python apm.py capture

    the above command captures the personality of an RLM. Creates a file:
    [SerialNo].apm

    when integrated with the maintenane mode, this will be "downloaded"

python apm.py apply [personalityfile].apm

    this command applies the personality from the personality file to the current RLM. essentially the files will be installed on the RLM

python apm.py service [servicepackid] [config]

    this command creates a service pack. Creates the Service Pack file
        RLM<sp id>.apm
    
    <config> is the configuration for this service pack. Essentially the standard template file customized for this service pack. Default config will have the rlc and rt executables. Additional sections such as applyscripts will be empty. The applyscripts section can have additional steps in the form of either shell scripts or python scripts. the script files themselves can be packed as part of the service pack

python apm.py apply [servicepackid].apm 

    In this example, a service pack is applied to an rlm. After verifying the integrity of the apm file, the files are restored to the target directories. After the restoration, the applyscripts section is processed by executing each command.

python apm.py extract [-p password] apmfile.apm destination

    unpacks the contents to the destination folder

## Design Details

The apm file is essentially a zip file that has been encrypted using the aes-256-cbc algorithm.

encryption password within the RLM will be retrieved from:

/opt/abiomed/rlm/config
apm.conf
[package]
encpass=

default: apm@abiomed

apply password will be retrieved from the above file but with key=decpass

password can always be altered via the command line using the switch -p

This file can be uploaded and applied using the maintenance mode.

In the case of Service Packs, the RLM will poll a file distribution site. The file will be automatically downloaded but can only be applied using the maintenance mode.

the online distribution will use the https port (443).

## Service Pack Configuration

type=servicepack

### Section Prerequisite

Prerequisites before applying this service pack
```
[prerequisites]
rlmid=all
rlmid=RL00046, RL00047, RL00049
rlmdevice=min version
firewall=minversion
cloudprotocol=min spec
firewallprotocol=min spec
aicprotocol=min

Scripts (.sh or .py) to execute. assumed to be in /opt/abiomed/rlm/bin

[scripts]
preinstall=
postinstall=

[packfiles]
file0=source-full-name
file2=source-full-name


[installfile]
filename=targetfolder
filename=targetfolder

```

