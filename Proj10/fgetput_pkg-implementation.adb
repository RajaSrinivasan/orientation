with System.Storage_Elements ;
with Ada.Text_Io; use Ada.Text_Io;
with ada.integer_text_io; use ada.integer_text_io;
with Ada.Directories ; use Ada.Directories ;
with ada.strings.unbounded; use ada.strings.unbounded ;
with Ada.Streams ;
with Ada.Streams.Stream_Io ;
with GNAT.Sockets ;
with GNATCOLL.JSON ;

with digest_pkg ;
with base64 ;
with fgetput_pkg.config ;
with fgetput_pkg.messages ;
with fgetput_pkg.tools ;

package body fgetput_pkg.Implementation is
    package ADIR renames Ada.Directories ;

    Mysocket : GNAT.Sockets.Socket_Type ;
    config : fgetput_pkg.config.Configuration_Type ;

    procedure ShowConfig is
    begin
      Put("Configuration loaded from ");
      Put_Line( fgetput_cli.ConfigFile.all );
      Put("Port Number ");
      Put(Integer(config.port));
      New_Line;
      put("Root ");
      Put(to_string(config.root));
      new_line ;
    end ShowConfig ;

    procedure PackFile( msg : in out fgetput_pkg.messages.Request_Packet_Type ;
                        filename : string ) is
      use ada.streams ;
    begin
      msg.status := to_unbounded_String("");
      msg.FileName := to_unbounded_String(ADIR.Simple_Name(filename));
      msg.FileSize := Integer(ADIR.Size( filename ));
      msg.Signature := to_unbounded_String(digest_pkg.digest_sha( filename , 256 ));
      msg.Path := to_unbounded_string(fgetput_cli.Path.all );
      declare
         encoded : string := base64.base64_encode( filename );
      begin
         msg.Contents := to_unbounded_string(encoded) ;
      end ;
    end PackFile ;

    procedure UnpackFile( msg : in out fgetput_pkg.messages.Request_Packet_Type ;
                          output : String ) is
      filebytes : System.Storage_Elements.Storage_Array
         := base64.Base64_decode( to_string(Msg.Contents) );
      filestrbytes : Ada.Streams.Stream_Element_Array( 1 .. filebytes'length );
         for filestrbytes'address use filebytes'address ;
      outfile : ada.streams.stream_io.File_Type ;

    begin

      Ada.Streams.Stream_Io.Create( outfile ,
                                    ada.streams.stream_io.out_file ,
                                    output ) ;
      Ada.Streams.Stream_Io.Write( outfile , filestrbytes );
      Ada.Streams.Stream_Io.Close( outfile );
      if digest_pkg.digest_sha( output , 256 ) /=
         to_string( msg.Signature )
      then
         Put_Line("Signature match failed");
         msg.Status := to_unbounded_String("SignatureFailure");
         return ;
      else
         Put_Line("Signature matched");
       end if ;
       msg.Status := to_unbounded_String("Success");
    Exception
        when others =>
           msg.Status := to_unbounded_String("ExceptionCreatingFile") ;
    end UnpackFile ;

    procedure ProvideService (Client : GNAT.Sockets.Socket_Type) Is
       Msg : Request_Packet_Type ;
       Reply : Request_Packet_Type ;
       destdirname,
       destfilename : unbounded_string ;

       procedure MakeDestDir is
       begin
         destdirname := to_unbounded_string( ADIR.compose(
                                                     to_string(config.root) ,
                                                     to_string(msg.Path))) ;
         if ADIR.Exists( to_string(destdirname) )
            and then ADIR.kind( to_string(destdirname)) = ADIR.Directory
         then
           destfilename := to_unbounded_string(ADIR.compose( to_string(destdirname) , to_string(msg.FileName) ));
           return ;
         end if ;
          Reply.Req := fgetput_pkg.messages.Exception_Response ;
          reply.Status := to_unbounded_string("DestDirErr");
          raise PROGRAM_ERROR ;
       end MakeDestDir ;

    begin
       loop
          begin
             Receive( client , Msg ) ;
          Exception
             when others =>
                   exit ;
          end ;
          if fgetput_cli.verbose
          then
            fgetput_pkg.messages.show( msg );
          end if ;
          case Msg.Req is
            when fgetput_pkg.messages.Put_File =>
                 if msg.Path /= Null_Unbounded_String
                 then
                    MakeDestDir ;
                 else
                     destfilename := to_unbounded_string(
                       ADIR.compose( to_string(config.root) ,
                                                to_string(msg.FileName)));
                 end if ;
                 put("Creating "); Put_Line(to_string(destfilename));
                 UnpackFile( msg , to_string(destfilename));
                 Reply.Status := to_unbounded_String("Success");
                 Reply.req := fgetput_pkg.messages.Put_File_Response ;

            when fgetput_pkg.messages.Get_File =>
                 if msg.Path /= Null_Unbounded_String
                 then
                   destdirname := to_unbounded_string(
                                     ADIR.Compose( to_string(config.root) ,
                                                   to_string(msg.Path)));
                   destfilename := to_unbounded_string(
                                     ADIR.Compose( to_string(destdirname) ,
                                                   to_string(msg.FileName))) ;
                 else
                   destfilename := to_unbounded_string(
                                     ADIR.Compose( to_string(config.root) ,
                                                    to_string(msg.FileName)));
                 end if ;
                 put("Packing "); Put_Line(to_string(destfilename));
                 Reply.Req := fgetput_pkg.messages.Get_File_Response ;
                 PackFile (Reply,to_string(destfilename));
            when others =>
                 Reply.Status := to_unbounded_string("Unrecognized") ;
                 raise PROGRAM_ERROR ;
          end case ;
          Send( client , Reply );
       end loop ;
    exception
       when others =>
          Reply.Req := fgetput_pkg.messages.Exception_Response ;

          Send( client , Reply ) ;
    end ProvideService ;

    procedure StartService is
       myaddr   : GNAT.Sockets.Sock_Addr_Type;
    begin
       fgetput_pkg.config.Load( config , fgetput_cli.ConfigFile.all);
       if ADIR.Exists( to_string(config.root))
          and then ADIR.kind( to_string(config.root) ) = ADIR.Directory
       then
          if fgetput_cli.verbose
          then
            Put("Root Directory set to : ");
            Put_Line( to_string(config.root));
          end if ;
      else
          Put(to_string(config.root));
          Put_Line(" does not exist or is not a directory");
          raise PROGRAM_ERROR ;
      end if ;

       if fgetput_cli.Verbose
       then
         ShowConfig ;
       end if ;

       GNAT.Sockets.Create_Socket( Mysocket ) ;
       myaddr.Addr := GNAT.Sockets.Any_Inet_Addr;
       myaddr.Port := config.port ;
       GNAT.Sockets.Bind_Socket (mysocket, myaddr) ;
       loop
          if fgetput_cli.verbose
          then
            Put("Waiting for a connect request"); New_Line ;
          end if ;
          GNAT.Sockets.Listen_Socket (mysocket);
          declare
             use GNAT.Sockets;
             clientsocket : GNAT.Sockets.Socket_Type;
             clientaddr   : GNAT.Sockets.Sock_Addr_Type;
             accstatus    : GNAT.Sockets.Selector_Status;
          begin
             GNAT.Sockets.Accept_Socket
               (mysocket,
                clientsocket,
                clientaddr,
                60.0 * fgetput_cli.BreathingInterval ,
                Status => accstatus);
             if accstatus = GNAT.Sockets.Completed then
                Put("Received a connection Client: ") ;
                Put( GNAT.Sockets.Image( Clientaddr ) ) ;
                New_Line ;
                ProvideService (Clientsocket) ;
                GNAT.Sockets.Close_Socket (clientsocket);
             end if;
          end;
       end loop ;
    end StartService ;

    procedure FileTransfer is
       Serversocket : GNAT.Sockets.Sock_Addr_Type ;
       NodeName : string := fgetput_cli.Server.all ;
       Hostentry : GNAT.Sockets.Host_Entry_Type := GNAT.Sockets.Get_Host_By_Name(Nodename) ;
       Reqp : fgetput_pkg.messages.Request_Packet_Type ;
       Recvp : fgetput_pkg.messages.Request_Packet_Type ;
       filename : string := fgetput_cli.GetNextArgument ;
       procedure Prepare is
         use ada.streams ;
       begin
         reqp.status := to_unbounded_String("");
         reqp.FileName := to_unbounded_String(ADIR.Simple_Name( filename ));
         reqp.FileSize := Integer(ADIR.Size( filename ));
         reqp.Signature := to_unbounded_String(digest_pkg.digest_sha( filename , 256 ));
         reqp.Path := to_unbounded_string(fgetput_cli.Path.all );
         declare
            encoded : string := base64.base64_encode( filename );
         begin
            reqp.Contents := to_unbounded_string(encoded) ;
         end ;
       end Prepare ;
    begin
      if fgetput_cli.GetFile
      then
        Reqp.req := Get_File ;
      elsif fgetput_cli.PutFile
      then
        Reqp.req := Put_File ;
      else
         Put_Line("specify putfile or get file");
         return ;
      end if ;

        GNAT.Sockets.Create_Socket( Mysocket ) ;
        Put( "Client Host: ");
        Put( GNAT.Sockets.Host_Name ) ;
        New_Line;

       fgetput_pkg.config.Load( config , fgetput_cli.ConfigFile.all);
       if fgetput_cli.Verbose
       then
         ShowConfig ;
       end if ;
       Serversocket.Port := config.port ;
       if fgetput_Cli.Verbose
       then
          Put("Server ");
          Put(NodeName) ;
          Put(" Host Entries");
          New_Line ;
          for Ha in 1..GNAT.Sockets.Addresses_Length(Hostentry)
          loop
             Put( GNAT.Sockets.Image(GNAT.Sockets.Addresses(Hostentry,Ha))) ;
             New_Line ;
          end loop ;
       end if ;
       ServerSocket.Addr := GNAT.Sockets.Addresses(Hostentry,1) ;
       GNAT.Sockets.Connect_Socket( Mysocket , ServerSocket ) ;
       if fgetput_Cli.Verbose
       then
          Put("Connected to ") ;
          Put( GNAT.Sockets.Image(GNAT.Sockets.Get_Peer_Name( Mysocket ))) ;
          New_Line ;
       end if ;
       case reqp.req is
         when fgetput_pkg.messages.Put_File =>
              PackFile( reqp , filename ) ;
         when fgetput_pkg.messages.Get_File =>
              reqp.FileName := to_unbounded_string(ADIR.Simple_Name( filename ));
              reqp.Path := to_unbounded_string(fgetput_cli.Path.all) ;
         when others =>
              raise PROGRAM_ERROR ;
       end case ;
       Send( Mysocket , reqp );
       Receive( Mysocket , recvp );
       Put("Response Status :");
       Put(to_string(recvp.Status));
       New_Line;
       if recvp.Req = fgetput_pkg.messages.Get_File_Response
       then
           UnpackFile( recvp , fgetput_cli.OutputFileName.all );
           Put("File unpack status ");
           Put_Line( to_string( recvp.Status ));
       end if ;
    exception
       when GNAT.Sockets.Host_Error =>
          Put("Unknown Server ");
          Put_Line(NodeName) ;
       when GNAT.Sockets.Socket_Error =>
          Put("Connection refused: ");
          Put("Node = "); Put(NodeName) ;
          Put(" Port = ") ; Put(Integer(config.port)) ;
          New_Line ;
    end FileTransfer ;

    procedure Receive( Source : GNAT.Sockets.Socket_Type ;
                       Msg : out Request_Packet_Type ) is
       Channel : GNAT.Sockets.Stream_Access := GNAT.Sockets.Stream( Source ) ;
       Strmsg : String := String'Input( Channel ) ;
       status : boolean ;
       msgval : GNATCOLL.JSON.json_value ;
    begin
       if fgetput_cli.Verbose
       then
          Put("Receive: ");
          Put(GNAT.Sockets.Image(GNAT.Sockets.Get_Peer_Name(source))) ;
          Put_Line("< ");
          Put_Line (strmsg) ;
       end if ;
       msgval := fgetput_pkg.tools.Load( Strmsg );
       fgetput_pkg.messages.Unpack(status , fnPacket , msgval , msg );
       if fgetput_cli.verbose
       then
          fgetput_pkg.messages.show(msg);
       end if;
    end Receive ;

    procedure Send( Destination : GNAT.Sockets.Socket_Type ;
                    Msg : Request_Packet_Type ) is
       Channel : GNAT.Sockets.Stream_Access := GNAT.Sockets.Stream( Destination ) ;
       status : boolean ;
       value : GNATCOLL.JSON.json_value ;
    begin
       fgetput_pkg.messages.Pack( status , fnPacket , value , Msg );
       String'Output( Channel ,
                      fgetput_pkg.tools.Save(value) ) ;
       if fgetput_cli.Verbose
       then
          Put("Send: ");
          Put(GNAT.Sockets.Image(GNAT.Sockets.Get_Peer_Name(destination))) ;
          Put_line("> ");
          Put_Line(fgetput_pkg.tools.Save(value));
          fgetput_pkg.messages.show( Msg );
       end if ;
    end Send ;

end fgetput_pkg.Implementation ;
