with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package fgetput_cli is                                       -- [cli/$_cli]

   VERSION : string := "V01" ;
   NAME : String := "fgetput" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose

   ServerMode : aliased boolean ;
   GetFile : aliased boolean ;
   PutFile : aliased boolean ;
   MaxFileSize : aliased Integer ;
   BreathingInterval : aliased Integer ;

   Path : aliased GNAT.strings.string_access;
   ConfigFile : aliased GNAT.strings.string_access := new String'("config.json");
   Server : aliased GNAT.strings.string_access ;
   OutputFileName : aliased GNAT.strings.string_access ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

end fgetput_cli ;                                            -- [cli/$_cli]
