# Project : fgetput

## Objective :

      Explore Network programming
      Explore subpackages
      Explore JSON processing
      Explore the gnatcoll library

## Usage :

```
../bin/fgetput -h
fgetput V01 2017-12-18 05:46:41
Usage: fgetput Command Line utility
fgetput -p -r <remote> -c node localfilename --- to put a file
fgetput -g -r <remote> -c node localfilename --- to get a file

 -v, --verbose             Output extra verbose information
 -s, --server              provide service
 -c, --connect ARG         node to connect for accessing the service
 -g, --get-file            get a file
 -p, --put-file            put a file
 -r, --remote-path ARG     remote path
 -M, --Max-filesize ARG    Maximum file size in MB
 -o, --output-filename ARG outputfile name for get

```

### Implementation Notes

The transfer is structured as a JSON packet. The data file itself is converted into
base64 format to handle binary files. A signature with SHA256 hash is generated to
verify the contents.

The above is not an ideal solution for all file transfer needs. Particularly huge files
will not transfer very well using this approach.

The implementation uses an arbitrary sized buffer for the file data. The size
can be modified using the -M switch.

The server handles only one client at a time. While it waits for a connection request
it wakes up periodically and announces it is still functioning.

## Uploading a file

Following command uploads the file fgetput.adb to the server on "localhost". File will be stored in the relative directory tmp under the root.

```
../bin/fgetput -p -rtmp -c localhost fgetput.adb
```

## Downloading a file


Following command downloads the file fgetput.adb from the relative directory tmp to the local
file: abc.txt
```
../bin/fgetput -g -rtmp -c localhost -o abc.txt fgetput.adb
```
