with GNAT.Sockets ;
with fgetput_pkg.messages ; use fgetput_pkg.messages ;
package fgetput_pkg.Implementation is
  procedure StartService ;
  procedure FileTransfer ;

private

    procedure Send( Destination : GNAT.Sockets.Socket_Type ;
                    Msg : Request_Packet_Type ) ;

    procedure Receive( Source : GNAT.Sockets.Socket_Type ;
                       Msg : out Request_Packet_Type ) ;

end fgetput_pkg.Implementation ;
