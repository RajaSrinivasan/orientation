with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body fgetput_cli is                          -- [cli/$_cli]

    package GCL renames GNAT.Command_Line ;

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
        GCL.Set_Usage( Config ,
                       Help => NAME & " " &
                                      VERSION & " " &
                                      Compilation_ISO_Date & " " &
                                      Compilation_Time ,
                                      Usage => "Command Line utility" & ASCII.LF &
                                      "fgetput -p -r <remote> -c node localfilename --- to put a file" & ASCII.LF &
                                      "fgetput -g -r <remote> -c node localfilename --- to get a file");
        GCL.Define_Switch (Config,
                       verbose'access ,
                       Switch => "-v",
                       Long_Switch => "--verbose",
                       Help => "Output extra verbose information");
       GCL.Define_Switch (Config,
                      ServerMode'access ,
                      Switch => "-s",
                      Long_Switch => "--server",
                      Help => "provide service");
       GCL.Define_Switch (Config,
                       Server'access ,
                       Switch => "-c:",
                       Long_Switch => "--connect:",
                       Help => "node to connect for accessing the service");
       GCL.Define_Switch (Config,
                      GetFile'access ,
                      Switch => "-g",
                      Long_Switch => "--get-file",
                      Help => "get a file");
       GCL.Define_Switch (Config,
                     PutFile'access ,
                     Switch => "-p",
                     Long_Switch => "--put-file",
                     Help => "put a file");
       GCL.Define_Switch (Config,
                     Path'access ,
                     Switch  => "-r:",
                     Long_Switch => "--remote-path:",
                     Help => "remote path");
       GCL.Define_Switch (Config,
                          MaxFileSize'access ,
                          Switch => "-M:",
                          Long_Switch => "--Max-filesize:",
                          Initial => 1000 ,
                          Default => 1000 ,
                          Help => "Maximum file size in MB" );
      GCL.Define_Switch (Config,
                         BreathingInterval'access ,
                         Switch => "-B?",
                         Long_Switch => "--Breathing-interval?",
                         Initial => 1 ,
                         Default => 10 ,
                         Help => "breathing interval in minutes" );

      GCL.Define_Switch (Config,
                         OutputFileName'access ,
                         Switch  => "-o:",
                         Long_Switch => "--output-filename:",
                         Help => "outputfile name for get");
       GCL.Getopt(config,SwitchHandler'access);

    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GCL.Get_Argument(Do_Expansion => True) ;
    end GetNextArgument ;

    procedure ShowCommandLineArguments is
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
    end ShowCommandLineArguments ;

end fgetput_cli ;                                   -- [cli/$_cli]
