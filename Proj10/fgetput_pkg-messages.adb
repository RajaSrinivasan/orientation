with Ada.Text_Io; use Ada.Text_Io;
with GNATCOLL.JSON ;

with fgetput_pkg.tools ;

package body fgetput_pkg.Messages is
     package JSON renames GNATCOLL.JSON ;
     procedure Pack( status : out boolean ;
                     objname : string ;
                     packed : out GNATCOLL.json.json_value ;
                     data : Request_Packet_Type ) is
        result : GNATCOLL.json.json_value
                        := JSON.Create_Object ;
     begin
       JSON.Set_Field(result,fnreq,JSON.Create(Integer(Request_Type'pos(data.req))));
       JSON.Set_Field(result,fnstatus,JSON.Create(data.Status));
       JSON.Set_Field(result,fnFileName,JSON.Create(data.FileName)) ;
       JSON.Set_Field(result,fnFileSize,JSON.Create(data.FileSize)) ;
       JSON.Set_Field(result,fnSignature,JSON.Create(data.Signature)) ;
       JSON.Set_Field(result,fnPath,JSON.Create(data.Path)) ;
       JSON.Set_Field(result,fnContents,
                           JSON.Create(data.Contents));
       packed := JSON.Create_Object ;
       JSON.Set_Field(packed,objname,result);
       status := true ;
     end Pack;

     procedure UnPack( status : out boolean ;
                       objname : string ;
                       packed : GNATCOLL.json.json_value ;
                       data : out Request_Packet_Type ) is
        reqval : JSON.json_value ;
        function Get( name : string ) return Integer is
        begin
           return JSON.Get( reqval , name );
         end Get ;
     begin

       reqval := JSON.Get( packed , objname );
       put("Req => "); Put_Line( fgetput_pkg.tools.Save(reqval));
       data.req := Request_Type'val(Get(fnReq)) ;
       put_line(fnReq);
       --put(integer'image(JSON.Get(reqval,fnReq))); new_line ;

       data.status := JSON.Get(reqval,fnStatus);
       put_line(fnStatus);
       data.FileName := JSON.Get(reqval,fnFileName);
       put_line(fnFileName);
       data.FileSize := JSON.Get(reqval,fnFileSize);
       put_line(fnFileSize);
       data.Path := JSON.Get(reqval,fnPath);
       put_line(fnPath);
       data.Signature := JSON.Get(reqval,fnSignature);
       put_line(fnSignature);
       data.Contents := JSON.Get(reqval,fnContents);
       put_line(fnContents);
       status := true ;
     end Unpack ;

     procedure Show( data : Request_Packet_Type ) is
     begin
         Put("Request_Type : "); Put_Line(Request_Type'image(data.req)) ;
         Put("Status : "); Put_Line( to_string(data.status));
         Put("FileName : "); Put_Line( to_string(data.FileName));
         Put("FileSize : "); Put_Line( Integer'Image(data.FileSize));
         put("Signature : "); Put_Line( to_string(data.Signature));
         Put("Path : "); Put_Line( to_string(data.Path));
     end Show ;

end fgetput_pkg.Messages ;
