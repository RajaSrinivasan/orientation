with System.Storage_Elements ;
with ada.strings.unbounded ; use ada.strings.unbounded ;
with GNATCOLL.JSON ;

package fgetput_pkg.Messages is

      type Request_Type is
        (
           Get_File ,
           Put_File ,
           Get_File_Response ,
           Put_File_Response ,
           Exception_Response
        ) ;
      type Request_Packet_Type is
        record
           req : Request_Type := Exception_Response ;
           status : Unbounded_String := Null_Unbounded_String ;
           FileName : Unbounded_String := Null_Unbounded_String ;
           FileSize : Integer := 0 ;
           Path : Unbounded_String := Null_Unbounded_String ;
           Signature : Unbounded_String := Null_Unbounded_String ;
           Contents : unbounded_string := Null_Unbounded_String ;
         end record ;

         fnreq : constant string := "req" ;
         fnstatus : constant string := "status" ;
         fnFileName : constant string := "filename" ;
         fnFileSize : constant string := "filesize" ;
         fnPath : constant String := "path" ;
         fnSignature : constant String := "signature" ;
         fnContents : constant String := "contents" ;

      fnPacket : constant String := "fgetput" ;
      procedure Pack( status : out boolean ;
                      objname : string ;
                      packed : out GNATCOLL.json.json_value ;
                      data : Request_Packet_Type );

      procedure UnPack( status : out boolean ;
                        objname : string ;
                        packed : GNATCOLL.json.json_value ;
                        data : out Request_Packet_Type ) ;
     procedure Show( data : Request_Packet_Type );
end fgetput_pkg.Messages ;
