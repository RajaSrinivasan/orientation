
with GNATCOLL.json ;

package fgetput_pkg.tools is

    function Save( value : GNATCOLL.json.json_value ) return String ;
    function Load( value : String ) return GNATCOLL.JSON.json_value ;

    procedure Save( value : GNATCOLL.json.json_value ;
                    filename : string );
    procedure Load( value : out GNATCOLL.json.json_value ;
                    filename : string ) ;

end fgetput_pkg.tools ;
