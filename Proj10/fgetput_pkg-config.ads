with ada.strings.unbounded ; use ada.strings.unbounded ;
with GNAT.Sockets ;
with GNATCOLL.json ;

package fgetput_pkg.config is

     type Configuration_Type is
     record
        port : GNAT.Sockets.port_type ;
        root : unbounded_string ;
     end record ;

     fnport : constant string := "port" ;
     fnroot : constant string := "root" ;

      procedure Save( value : Configuration_Type ;
                      filename : string );
      procedure Load( value : out Configuration_Type ;
                      filename : string ) ;
      procedure Pack( status : out boolean ;
                      packed : out GNATCOLL.json.json_value ;
                      data : Configuration_Type );
      procedure UnPack( status : out boolean ;
                        packed : GNATCOLL.json.json_value ;
                        data : out Configuration_Type ) ;

end fgetput_pkg.config ;
