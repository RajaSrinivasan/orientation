with System.Storage_Elements ;
with Ada.Text_Io ; use Ada.Text_Io ;
with Ada.Directories ;
with Ada.Streams.Stream_IO ;

with base64;

package body fgetput_pkg.tools is
  use GNATCOLL ;

  function Save( value : GNATCOLL.json.json_value ) return String is
  begin
     return JSON.Write( value );
   end Save ;

  function Load( value : String ) return GNATCOLL.JSON.json_value is
  begin
     return JSON.Read( value );
   end Load ;

  procedure Save( value : GNATCOLL.json.json_value ;
                  filename : string ) is
     outfile : file_type ;
  begin
      Create(outfile,out_file,filename);
      Put(outfile,GNATCOLL.json.Write(value));
      close(outfile);
  end Save ;
  procedure Load( value : out GNATCOLL.json.json_value ;
                  filename : string ) is

     filesize : Natural := Natural (Ada.Directories.Size (filename));
     file   : Ada.Streams.Stream_IO.File_Type;
     stream : Ada.Streams.Stream_IO.Stream_Access;
     buffer : Ada.Streams
       .Stream_Element_Array (1 .. Ada.Streams.Stream_Element_Offset (filesize));
     bufferlen    : Ada.Streams.Stream_Element_Offset;
     filecontents : String (1 .. filesize);
     for filecontents'Address use buffer'Address;
  begin
    Ada.Streams.Stream_IO.Open
      (file,
       Ada.Streams.Stream_IO.In_File,
       filename);
    stream := Ada.Streams.Stream_IO.Stream (file);
    stream.Read (buffer, bufferlen);
    Ada.Streams.Stream_IO.Close (file);
    value := GNATCOLL.json.Read( filecontents );
  end Load ;

end fgetput_pkg.tools ;
