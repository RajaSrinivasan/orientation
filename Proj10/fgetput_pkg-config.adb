with Ada.Text_Io; use Ada.Text_Io;

with fgetput_pkg.tools ; use fgetput_pkg.tools ;
package body fgetput_pkg.config is
  procedure Save( value : Configuration_Type ;
                  filename : string ) is
     packed : GNATCOLL.json.json_value ;
     status : boolean ;
  begin
     Pack( status , packed , value ) ;
     Save( packed , filename );
  end Save ;
  procedure Load( value : out Configuration_Type ;
                  filename : string ) is
     status : boolean ;
     unpacked : GNATCOLL.json.json_value ;
  begin
     Load( unpacked , filename );
     Unpack( status , unpacked , value );
  end Load ;

  procedure Pack( status : out boolean ;
                  packed : out GNATCOLL.json.json_value ;
                  data : Configuration_Type ) is
     result : GNATCOLL.json.json_value := GNATCOLL.json.Create_Object ;
  begin
    GNATCOLL.json.Set_Field( result , fnport , GNATCOLL.json.Create(integer(data.port))) ;
    GNATCOLL.json.Set_Field( result , fnroot , GNATCOLL.json.Create(data.root));
    packed := result ;
    put_line("Packed a config");
    status := true ;
  end Pack;

  procedure UnPack( status : out boolean ;
                    packed : GNATCOLL.json.json_value ;
                    data : out Configuration_Type ) is
     packedportno : integer ;
  begin
     packedportno := GNATCOLL.json.Get( packed , fnport);
     data.port := GNAT.Sockets.Port_Type(packedportno) ;
     data.root := GNATCOLL.json.Get( packed , fnroot);
     status := true ;
  end Unpack ;

end fgetput_pkg.config ;
