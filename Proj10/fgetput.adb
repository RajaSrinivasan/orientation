with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with Ada.Directories ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;

with GNAT.Source_Info ;
with GNAT.Sockets ;
with GNATCOLL.JSON ;

with fgetput_cli ;                            -- [cli/$_cli]
with fgetput_Pkg ;                            -- [Pkg/$_Pkg]
with fgetput_pkg.Implementation ;

procedure fgetput is                  -- [clitest/$]
   -- config : fgetput_pkg.Configuration_Type ;
   -- Req : fgetput_pkg.Request_Packet_Type ;
begin
   fgetput_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if fgetput_cli.Verbose                     -- [cli/$_cli]
   then
      fgetput_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;

   if fgetput_cli.ServerMode
   then
     fgetput_pkg.Implementation.StartService ;
   end if ;
   fgetput_pkg.Implementation.FileTransfer ;
end fgetput ;                         -- [clitest/$]
