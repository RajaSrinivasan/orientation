# Project: fedit
## Objective
Explore: Text processing to process comma separated variable files
         Regular expressions
         Object Orientation

## Usage:

```

../bin/fedit -h
Files Editor V01 2017-12-07 05:13:10
Usage: fedit Command Line utility

 -v, --verbose[ARG]          Output extra verbose information
 -c, --command-file ARG      command file to apply to files
 -i, --input-files[ARG]      input files
 -p, --filename-pattern[ARG] input files name pattern
 -D, --define-macro ARG      macro to use in substitution

```