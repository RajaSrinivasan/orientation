with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with fedit_cli ;                                          -- [cli/$_cli] ;
package fedit_pkg is                                       -- [pkg/$_pkg]
   procedure Implementation ( Cmdfilename : String ;
                              Inputfilepattern : String ) ;
end fedit_pkg ;                                            -- [pkg/$_pkg]
