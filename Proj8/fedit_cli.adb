with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body fedit_cli is                          -- [cli/$_cli]

    package Cl renames GNAT.Command_Line ;
    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : CL.Command_Line_Configuration;
    begin
       CL.Set_Usage( Config ,
                     Help => NAME & " " &
                       VERSION & " " &
                       Compilation_ISO_Date & " " &
                       Compilation_Time ,
                     Usage => "Command Line utility");
       CL.Define_Switch (Config,
                         verbose'access ,
                         Switch => "-v?",
                         Long_Switch => "--verbose?",
                         Help => "Output extra verbose information");
       CL.Define_Switch (Config,
                         output => Cmdfilename'access,
                         Switch => "-c:",
                         Long_Switch => "--command-file:",
                         Help => "command file to apply to files");
        CL.Define_Switch (Config,
                          output => Inputfilepattern'access,
                          Switch => "-p?",
                          Long_Switch => "--filename-pattern?",
                          Help => "input files name pattern");
        CL.Define_Section(Config,
                          "defines") ;
        CL.Getopt(config,SwitchHandler'access);
    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return CL.Get_Argument(Do_Expansion => False) ;
    end GetNextArgument ;

    procedure ShowDefines is
    begin
      CL.Goto_Section("defines");
      for i in 1..3
      loop
        Put_Line(CL.Get_Argument);
      end loop;
      CL.Goto_Section("");
    end ShowDefines ;

    procedure ShowCommandLineArguments is
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
       Put("Command File name "); Put_Line( Cmdfilename.all ) ;
       Put("Inputfiles pattern "); Put_Line(Inputfilepattern.all) ;
    end ShowCommandLineArguments ;

end fedit_cli ;                                   -- [cli/$_cli]
