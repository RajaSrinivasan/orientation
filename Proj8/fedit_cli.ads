with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package fedit_cli is                                       -- [cli/$_cli]

   VERSION : string := "V01" ;
   NAME : String := "Files Editor" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose

   cmdfilename : aliased gnat.strings.string_access ;
   Inputfilepattern : aliased Gnat.Strings.String_Access ;
   Macrodefine : aliased Gnat.Strings.String_Access ;
   Macroname : Unbounded_String := Null_Unbounded_String ;
   Value : Unbounded_String := Null_Unbounded_String ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowDefines ;
   procedure ShowCommandLineArguments ;

end fedit_cli ;                                            -- [cli/$_cli]
