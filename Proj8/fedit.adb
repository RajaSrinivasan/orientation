with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with GNAT.Source_Info ;

with fedit_cli ;                            -- [cli/$_cli]
with fedit_Pkg ;                            -- [Pkg/$_Pkg]
procedure fedit is                  -- [clitest/$]
begin
   fedit_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if fedit_cli.Verbose                     -- [cli/$_cli]
   then
      fedit_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
      fedit_cli.ShowDefines ;
   end if ;
   Fedit_Pkg.Implementation( Fedit_Cli.Cmdfilename.all,
                             Fedit_Cli.Inputfilepattern.all ) ;

end fedit ;                         -- [clitest/$]
