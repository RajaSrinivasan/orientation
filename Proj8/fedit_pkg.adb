with Text_Io; use Text_Io ;
with Ada.Containers.Doubly_Linked_Lists ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;

with Csv ;

package body fedit_Pkg is        -- [Pkg/$_Pkg]

   package CommandList_Pkg is new Ada.Containers.Doubly_Linked_Lists (
                                                                      Element_Type => Unbounded_String ) ;
   package CLP renames CommandList_Pkg ;

   CommandList : CLP.List ;

   procedure Implementation ( Cmdfilename : String ;
                              Inputfilepattern : String ) is
      Cmdfile : Csv.File_Type ;
      Cols : Integer ;
   begin
      if cmdfilename'length < 1
      then
        Put_Line("Please provide a command file");
        return ;
      end if ;
      Cmdfile := Csv.Open( Cmdfilename , "/" );
      Cols := Csv.No_Columns(Cmdfile) ;
      while not Csv.End_Of_File( Cmdfile )
      loop
         Csv.Get_Line( Cmdfile ) ;
         if Fedit_Cli.Verbose
         then
            Put( "Line " );
            Put( Integer'Image( Csv.Line_No(Cmdfile) )) ; New_Line ;
            for Field in 1..Cols
            loop
               Put( Csv.Field_Name( Cmdfile , Field ) );
               Put_Line( Csv.Field( Cmdfile , Field ) ) ;
            end loop ;
         end if ;
         CLP.Append( CommandList , To_Unbounded_String(Csv.Field( Cmdfile , 1 )) ) ;
      end loop ;
      Csv.Close(cmdfile) ;
      if Inputfilepattern'length < 1
      then
        loop
          declare
            arg : string := fedit_cli.GetNextArgument ;
          begin
            if arg'length < 1
            then
              exit;
            end if ;
            put_line( arg );
          end ;
        end loop;
      else
        put_line("Will search for files in the pattern " & Inputfilepattern);
      end if ;
   end Implementation ;
end fedit_Pkg ;                  -- [Pkg/$_Pkg]
