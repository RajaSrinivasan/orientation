with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body exec_cli is                          -- [cli/$_cli]

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
       GNAT.Command_Line.Set_Usage( Config ,
                                    Help => NAME & " " &
                                      DESCRIPTION & " " &
                                      VERSION & " " &
                                      Compilation_ISO_Date & " " &
                                      Compilation_Time ,
                                      Usage => "[flags] <command>");
        GNAT.Command_Line.Define_Switch (Config,
                                         verbose'access ,
                                         Switch => "-v?",
                                         Long_Switch => "--verbose?",
                                         Help => "Output extra verbose information");

        GNAT.Command_Line.Define_Switch (Config,
                                         output => logfilename'access,
                                         Switch => "-l:",
                                         Long_Switch => "--log-file:",
                                         Help => "log file name");

        GNAT.Command_Line.Define_Switch (Config,
                                      output => cmdfilename'access,
                                      Switch => "-c:",
                                      Long_Switch => "--cmd-file:",
                                      Help => "command file name");

        GNAT.Command_Line.Define_Switch (Config,
                                         output => repeatgap'access,
                                         Switch => "-r:",
                                         Long_Switch => "--repeat-gap:",
                                         Default => 0 ,
                                         Help => "repeat gap in seconds");

        GNAT.Command_Line.Define_Switch (Config,
                                         output => repeattimes'access,
                                         Switch => "-n:",
                                         Long_Switch => "--repeat-count:",
                                         Default => 1 ,
                                         Help => "no of times to repeat");

        GNAT.Command_Line.Getopt(config,SwitchHandler'Access,Concatenate=>false);

    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GNAT.Command_Line.Get_Argument(Do_Expansion => False) ;
    end GetNextArgument ;

    function Gather_Arguments return String is
       Result : Ada.Strings.Unbounded.Unbounded_String ;
    begin
       loop
          declare
             Nextarg : String := GNAT.Command_Line.Get_Argument(Do_Expansion => False) ;
          begin
             if Verbose
             then
                Put("Gathering ");
                Put_Line(Nextarg) ;
             end if ;
             if Nextarg'Length >= 1
             then
                Ada.Strings.Unbounded.Append(Result," ") ;
                Ada.Strings.Unbounded.Append(Result,Nextarg) ;
             else
                exit ;
             end if ;
          end ;
       end loop ;
       return Ada.Strings.Unbounded.To_String(Result) ;
    end Gather_Arguments ;
    
    Function Gather_Arguments return Gnat.Os_Lib.Argument_List is
       Arglist : GNAT.Os_Lib.Argument_List(1..16) := (others => null) ;
       Numargs : Integer := 0 ;
    begin
       while Numargs < Arglist'Length 
       loop
          declare
             Nextarg : String := GNAT.Command_Line.Get_Argument(Do_Expansion => False) ;
          begin
             if Verbose
             then
                Put("Gathering ");
                Put_Line(Nextarg) ;
             end if ;
             if Nextarg'Length >= 1
             then
		Arglist(Arglist'Last - Numargs) := new String(Nextarg'Range) ;
		Arglist(Arglist'Last - Numargs).all := Nextarg ;
		Numargs := Numargs + 1 ;
             else
                exit ;
             end if ;
          end ;
       end loop ;
       if Numargs > 0
       then
	  return Arglist(Arglist'Last-Numargs+1 .. Arglist'Last) ;
       else
	  return Arglist(1..1);
       end if ;
    end Gather_Arguments ;
    
    procedure ShowCommandLineArguments is
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
       Put("Repeat gap (seconds) ");
       Put(Integer'Image(Repeatgap));
       New_Line ;
       Put("Command File : "); Put_Line(Cmdfilename.all);
       Put("Log File : "); Put_Line(Logfilename.all);
    end ShowCommandLineArguments ;

end exec_cli ;                                   -- [cli/$_cli]
