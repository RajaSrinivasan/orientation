with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with GNAT.Source_Info ;
with GNAT.Command_Line ;

with exec_cli ;                            -- [cli/$_cli]
with exec_Pkg ;                            -- [Pkg/$_Pkg]
procedure exec is                  -- [clitest/$]
begin
   exec_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if exec_cli.Verbose                     -- [cli/$_cli]
   then
      exec_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;
   declare
      Cmd : String := Exec_Cli.GetNextArgument ;
   begin
      Exec_Pkg.Implementation (Cmd) ;
   end ;
exception
   when GNAT.Command_Line.Exit_From_Command_Line =>
      null ;
end exec ;                         -- [clitest/$]
