with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with exec_cli ;                                          -- [cli/$_cli] ;
package exec_pkg is                                       -- [pkg/$_pkg]
    procedure Implementation (Cmd : String := "" ) ;
end exec_pkg ;                                            -- [pkg/$_pkg]
