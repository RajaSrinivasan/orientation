# Project: exec
## Objective
Explore: Execute system commands capturing output

## Usage:
```
../bin/exec -h
exec Command executor V01 2017-11-18 04:44:38
Usage: exec [flags] <command>

 -v, --verbose[ARG]     Output extra verbose information
 -l, --log-file ARG     log file name
 -c, --cmd-file ARG     command file name
 -r, --repeat-gap ARG   repeat gap in seconds
 -n, --repeat-count ARG no of times to repeat

```