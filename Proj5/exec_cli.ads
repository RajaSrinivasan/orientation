with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;
with Gnat.Os_Lib ;

package exec_cli is                                       -- [cli/$_cli]
   VERSION : string := "V01" ;
   NAME : String := "exec" ;
   DESCRIPTION : String := "Command executor" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose

   Logfilename : aliased Gnat.Strings.String_Access ;
   Cmdfilename : aliased Gnat.Strings.String_Access ;
   Repeatgap : aliased Integer ;
   Repeattimes : aliased Integer ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   function Gather_Arguments return String ;
   function Gather_Arguments return GNAT.Os_Lib.Argument_List ;
   procedure ShowCommandLineArguments ;
end exec_cli ;                                            -- [cli/$_cli]
