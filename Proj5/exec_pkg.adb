with Text_Io; use Text_Io ;
with Calendar ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded ;
with Ada.Strings.Maps ;
with GNAT.Time_Stamp ;
with GNAT.Expect ;
with GNAT.Os_Lib ;
with GNAT.Strings ;

with Exec_Cli ; use Exec_Cli ;

package body exec_Pkg is        -- [Pkg/$_Pkg]

   procedure Implementation (Cmd : String := "") is
      Execution_No : Integer := 0;
      Logfile : Text_Io.File_Type ;
      Status : aliased Integer ;
      Arglist : Gnat.Os_Lib.Argument_List
        := Exec_Cli.Gather_Arguments ;
   begin
      if Cmd'Length < 1 and
        Exec_Cli.Cmdfilename.all'Length < 1
      then
         Put_line("No command to execute");
         return ;
      end if ;
      if Exec_Cli.Logfilename.all'Length > 0
      then
         Text_Io.Create( Logfile , Text_Io.Out_File , Exec_Cli.Logfilename.all ) ;
         Text_Io.Set_Output( Logfile ) ;
      end if ;
      if Exec_Cli.Verbose
      then
         Put("Command ");
         Put_Line(Cmd);
         Put("Arglist ");
         Put_Line(Integer'Image(Arglist'Length));
      end if ;

      while Exec_Cli.Repeattimes > 0
      loop
         Execution_No := Execution_No + 1 ;

         Put("-----");
         Put(GNAT.Time_Stamp.Current_Time) ;
         Put(" Execution ");
         Put(Execution_No) ;
         Put("-----");
         New_Line ;

         if Cmd'Length >=1
         then
            declare
               Result : String :=
                 GNAT.Expect.Get_Command_Output( Cmd ,
                                                 Arglist ,
                                                 "" ,
                                                 Status'Access ,
                                                 True ) ;
            begin
               Put_Line(Result);
            end ;
         else
            null ;
         end if;
         Repeattimes := Repeattimes - 1 ;
         if Exec_Cli.Repeattimes > 0
         then
            delay Duration(Exec_Cli.Repeatgap) ;
         else
            exit ;
         end if ;
      end loop ;
      if Exec_Cli.Logfilename.all'Length > 0
      then
         Text_Io.Close( Logfile ) ;
      end if ;
   end Implementation ;
end exec_Pkg ;                  -- [Pkg/$_Pkg]
