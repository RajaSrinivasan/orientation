with Text_Io; use Text_Io ;
with Ada.Exceptions ;

with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;

with Gnoga.Application.Multi_Connect;
with Gnoga.Gui.Window;

with Gnoga.Gui.View;
with Gnoga.Gui.View.Grid ;
with Gnoga.Gui.View.Console ;

with Gnoga.Types ;
with Gnoga.Gui.Base;
with Gnoga.Gui.Element.Common;
with Gnoga.Gui.Element.Form;
with Gnoga.Gui.Element.Canvas ;
with Gnoga.Gui.Element.Canvas.Context_2d;
with Gnoga.Gui.Document ;
with Gnoga.Types.Colors ;

package body hanoi.view is                                                -- [App/$]

    type Panel_Type is
    record
        name : unbounded_string := null_unbounded_string ;
        pole : access hanoi.model.pole_Type ;
        view : Gnoga.Gui.element.Canvas.Canvas_Type ;
        color : Gnoga.Types.Colors.Color_Enumeration ;
    end record ;

    type Disks_Type is array (positive range <>) of
                            Gnoga.Gui.element.Common.DIV_Type ;
    type Disks_Ptr_Type is access all Disks_Type ;

    type Data_Type is new Gnoga.Types.Connection_Data_Type with
    record
        Window      : Gnoga.Gui.Window.Pointer_To_Window_Class;
        Grid        : Gnoga.Gui.View.Grid.Grid_View_Type;
        model       : hanoi.Model.Data_Type ;                             -- [App/$]

        inpView     : Gnoga.Gui.View.View_Type;
        inpForm     : Gnoga.Gui.Element.Form.Form_Type;
        inpNumDisks : Gnoga.gui.element.Form.Text_Type ;

        CmdView     : Gnoga.Gui.View.View_Type ;
        CmdForm     : Gnoga.Gui.Element.Form.Form_Type ;
        CmdBtnStart : Gnoga.Gui.Element.Common.Button_Type ;
        CmdBtnReset : Gnoga.Gui.Element.Common.Button_Type ;
        CmdBtnQuit  : Gnoga.Gui.Element.Common.Button_Type ;

        SourcePanel : Panel_Type ;
        TargetPanel : Panel_Type ;
        BufferPanel : Panel_Type ;

        disks       : Disks_Ptr_Type ;
        log         : Gnoga.Gui.View.Console.Console_View_Type ;
   end record;
   type Data_Ptr_Type is access all Data_Type ;
    type Handle_Type is new hanoi.model.CallbackHandle_Type with
    record
        data : Data_Ptr_Type ;
    end record ;
    type handle_ptr_type is access Handle_Type ;
   Default_Model : hanoi.Model.Data_Type ;                                -- [App/$]
   procedure DrawPanel(panel : in out Panel_Type);
   procedure ClearPanel(panel : in out Panel_Type) ;
   procedure On_Disk_Moved(from : hanoi.Model.Pole_Type ;
                           to : hanoi.Model.Pole_Type ;
                           handle : hanoi.model.CallbackHandle_Type'class ) is
     myhandle : Handle_Type := Handle_Type(handle);
   begin
     Put_Line("On_Disk_Moved disk size=" & integer'image(hanoi.Model.DiskStack_Pkg.Top(to.disks).size));
     ClearPanel( myhandle.data.SourcePanel);
     DrawPanel( myhandle.data.SourcePanel);
     ClearPanel( myhandle.data.TargetPanel);
     DrawPanel( myhandle.data.TargetPanel);
     ClearPanel( myhandle.data.BufferPanel);
     DrawPanel( myhandle.data.BufferPanel);
     delay 1.0 ;
   end On_Disk_Moved ;

   procedure On_Start (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
       data : constant Data_Ptr_Type := Data_Ptr_Type (Object.Connection_Data);
   begin -- On_Quit
       Data.Log.Put_Line("On_Start");
       hanoi.Model.Solve( data.Model ) ;
   exception -- On_Quit
   when E : others =>
       Gnoga.Log (Message => "On_Start: " & Ada.Exceptions.Exception_Information (E) );
   end On_Start;

   PANEL_HEIGHT : constant := 600;
   PANEL_WIDTH : constant := 400;
   DISKHEIGHT : constant := 40 ;
   DISKGAP : constant := 3 ;
   DISKUNITWIDTH : constant := 40 ;

   function DiskWidth( disk : hanoi.Model.Disk_Type ) return integer is
   begin
     return DISKUNITWIDTH * disk.size ;
   end DiskWidth ;

   procedure CalcRect( rect : out Gnoga.Types.Rectangle_Type ;
                       diskpos : integer ;
                       disk : hanoi.Model.Disk_Type ) is
   begin
       rect.height := diskheight ;
       rect.width := DiskWidth(disk) ;
       rect.x := (PANEL_WIDTH - rect.width) / 2  ;
       rect.y := PANEL_HEIGHT - diskpos * (diskheight + diskgap) ;
   end CalcRect ;

   procedure ClearPanel(panel : in out Panel_Type) is
     ctx : Gnoga.gui.element.Canvas.Context_2D.Context_2D_Type ;
     rect : Gnoga.Types.Rectangle_Type ;
   begin
     Gnoga.Gui.Element.Canvas.Context_2D.Get_Drawing_Context_2D( ctx , panel.view );
     rect.X := 0; rect.Y :=0; rect.Height := PANEL_HEIGHT ; rect.Width := PANEL_WIDTH ;
     ctx.Clear_Rectangle(rect) ;
   end ClearPanel ;

   procedure DrawPanel(panel : in out Panel_Type) is
     ctx : Gnoga.gui.element.Canvas.Context_2D.Context_2D_Type ;
     rect : Gnoga.Types.Rectangle_Type ;
     numdisks : integer := hanoi.model.DiskStack_Pkg.Depth( panel.pole.disks );
   begin
     Gnoga.Gui.Element.Canvas.Context_2D.Get_Drawing_Context_2D( ctx , panel.view );
     gnoga.gui.element.Canvas.Context_2d.Fill_Color( ctx , panel.color );
     for dpos in 1..numDisks
     loop
       CalcRect( rect , dpos , hanoi.Model.DiskStack_Pkg.Peek( panel.pole.disks,dpos).all);
       gnoga.gui.element.Canvas.Context_2d.Fill_Rectangle( ctx , rect );
     end loop;
   end DrawPanel ;

   procedure On_Reset (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
       data : constant Data_Ptr_Type := Data_Ptr_Type (Object.Connection_Data);
       h : handle_ptr_type := new Handle_Type ;
       numdisks : positive := positive'value( Data.inpNumDisks.Value );
       ctx : Gnoga.gui.element.Canvas.Context_2D.Context_2D_Type;
       diskrect : Gnoga.Types.Rectangle_Type ;
   begin -- On_Quit
      Data.Log.Put_Line("On_Reset");
      Data.Log.Put_Line( Data.inpNumDisks.Value );
      h.data := data ;
      Data.Model := hanoi.Model.Create( "Test" , numDisks , On_Disk_Moved'access , h );
      Data.SourcePanel.Pole := Data.Model.SourcePole'access ;
      Data.TargetPanel.Pole := Data.Model.TargetPole'access ;
      Data.BufferPanel.Pole := Data.Model.BufferPole'access ;
      ClearPanel( Data.sourcepanel );
      ClearPanel( Data.targetpanel );
      ClearPanel( Data.bufferpanel );
      DrawPanel( Data.sourcepanel );
   exception -- On_Quit
   when E : others =>
       Gnoga.Log (Message => "On_Reset: " & Ada.Exceptions.Exception_Information (E) );
   end On_Reset ;

     procedure On_Quit (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
        data : constant Data_Ptr_Type := Data_Ptr_Type (Object.Connection_Data);
        View : Gnoga.Gui.View.View_Type;
    begin -- On_Quit
      Data.Grid.Remove;
      View.Create (Parent => Data.Window.all);
      View.Put_Line (Message => "Bye");
      Data.Window.Close;
      Data.Window.Close_Connection;
    exception -- On_Quit
    when E : others =>
        Gnoga.Log (Message => "On_Quit: " & Ada.Exceptions.Exception_Information (E) );
    end On_Quit;

    procedure On_Connect (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
                        Connection  : access Gnoga.Application.Multi_Connect.Connection_Holder_Type)
    is
        Data : Data_Ptr_Type := new Data_Type ;
        panels   : Gnoga.Gui.View.Grid.Grid_Rows_Type (1..3,1..3) := ( 1 => (Gnoga.Gui.View.Grid.Col,
                                                                               Gnoga.Gui.View.Grid.Col,
                                                                               Gnoga.Gui.View.Grid.Col)  ,
                                                                       2 => (Gnoga.Gui.View.Grid.Col,
                                                                             Gnoga.Gui.View.Grid.Col,
                                                                             Gnoga.Gui.View.Grid.Col) ,
                                                                       3 => (Gnoga.Gui.View.Grid.Col ,
                                                                             Gnoga.Gui.View.Grid.SPN ,
                                                                             Gnoga.Gui.View.Grid.SPN) ) ;
       procedure CreatePanel( name : string ;
                              panel : in out Panel_Type;
                              parent : in out Gnoga.Gui.View.View_Base_Type'Class ;
                              color : Gnoga.Types.Colors.Color_Enumeration ) is
       begin
         put_line("Creating Panel " & name );
         panel.name := to_unbounded_string( name );
         panel.view.Create( parent => parent , height => PANEL_HEIGHT , width => PANEL_WIDTH );
         panel.color := color ;
         panel.view.color(color);
         panel.view.Border ( color => color );
       end CreatePanel ;
    begin -- On_Connect
        Main_Window.Connection_Data (Data => Data);
        Data.Window := Main_Window'Unchecked_Access;
        Data.Grid.Create (Parent => Main_Window, Layout => panels );
        Data.Model := Default_Model ;

        Data.inpView.Create (Parent => Data.Grid.Panel (1, 1).all);
        Data.inpView.Text_Alignment (Value => Gnoga.Gui.Element.Left);
        Data.inpForm.Create (Parent => Data.inpView);

        Data.inpForm.Put_HTML (HTML => "<h3>Number Of Disks</h3>");
        Data.inpNumDisks.Create(form => Data.inpForm , Size => 5 , Value => "3" ) ;

        -----------------Command Panel--------------------
        Data.CmdView.Create( Parent => Data.Grid.Panel(1,2).all);
        Data.CmdView.Text_Alignment (Value => Gnoga.Gui.Element.Center);
        Data.CmdForm.Create( Parent => Data.CmdView );
        Data.CmdBtnStart.Create( Parent => Data.CmdForm ,
                                     Content => "Start" ,
                                     ID => "start_btn") ;
        Data.CmdBtnStart.On_Click_Handler( Handler => On_Start'access );

        Data.CmdForm.Put_HTML("<br>");
        Data.CmdBtnReset.Create ( Parent => Data.CmdForm ,
                                 Content => "Reset" ,
                                 ID => "reset_btn");
        Data.CmdBtnReset.On_Click_Handler( Handler => On_Reset'access );

        Data.CmdForm.Put_HTML("<br>");
        Data.CmdBtnQuit.Create ( Parent => Data.CmdForm ,
                                Content => "Quit" ,
                                ID => "quit_btn");

        Data.CmdBtnQuit.On_Click_Handler (Handler => On_Quit'Access);

        -- Panels for the Towers
        CreatePanel( "Source" , Data.SourcePanel , Data.Grid.Panel (2, 1).all , Gnoga.Types.Colors.Red );
        CreatePanel( "Target" , Data.TargetPanel , Data.Grid.Panel (2, 3).all , Gnoga.Types.Colors.Green);
        CreatePanel( "Buffer" , Data.BufferPanel , Data.Grid.Panel (2, 2).all , Gnoga.Types.Colors.Blue) ;

        Data.Log.Create( parent => Data.Grid.Panel(3,1).all );
        Data.Log.Put_Line("Starting the Towers of Hanoi Game");
    exception
    when E : others =>
        Gnoga.Log (Message => "On_Connect: " & Ada.Exceptions.Exception_Information (E) );
    end On_Connect;

    procedure Initialize (model : access hanoi.Model.Data_Type ) is        -- [App/$]
    begin
        Gnoga.Application.Title (Name => "Application");
        Gnoga.Application.HTML_On_Close (HTML => "Bye. Please come again.");
        Gnoga.Application.Multi_Connect.Initialize;
        Gnoga.Application.Multi_Connect.On_Connect_Handler (Event => On_Connect'Access);

        Default_Model := model.all ;
        Text_Io.Put_Line("Model name " & to_string(Default_Model.name));

        Gnoga.Application.Multi_Connect.Message_Loop;

    exception
    when E : others =>
        Gnoga.Log (Message => Ada.Exceptions.Exception_Information (E) );
    end Initialize ;

end hanoi.view ;                                                             -- [App/$]
