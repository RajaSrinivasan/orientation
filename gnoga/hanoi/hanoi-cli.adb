with Text_Io; use Text_Io ;
with Ada.Command_Line ;

with hanoi ;                                -- [App/$]
with hanoi.Model ;                          -- [App/$]
with hanoi.View ;                           -- [App/$]
procedure hanoi.cli is                     -- [App/$]
    model : aliased hanoi.model.Data_Type ;  -- [App/$]
    NumDisks : integer := Integer'Value( Ada.Command_Line.Argument(1));
begin
    model := hanoi.Model.Create("Srinivasan" , NumDisks ) ;  -- [App/$]
    hanoi.Model.Show( model );
    hanoi.Model.Solve( model );
    hanoi.Model.Show( model );
    Put("Solved in ") ;
    Put(integer'image( hanoi.Model.MoveCount(model) )) ;
    New_Line ;
end hanoi.cli ;                            -- [App/$]
