with hanoi ;                                -- [App/$]
with hanoi.Model ;                          -- [App/$]
with hanoi.View ;                           -- [App/$]
procedure hanoi.main is                     -- [App/$]
begin
   hanoi.Initialize ( true ) ;                       -- [App/$]
end hanoi.main ;                            -- [App/$]
