with hanoi.Model ;                             -- [App/$]
package hanoi.View is                          -- [App/$]
    procedure Initialize (model : access hanoi.Model.Data_TYPE) ;    -- [App/$]
end hanoi.View ;                               -- [App/$]
