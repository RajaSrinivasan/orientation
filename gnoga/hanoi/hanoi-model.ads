with Ada.Strings.Unbounded ; use ada.strings.unbounded ;

with Stacks ;

package hanoi.Model is                                       -- [App/$]

    MAX_DISKS : constant := 12 ;
    type disk_type is
    record
        size : integer ;
    end record ;
    type disk_ptr_type is access disk_type ;

    package DiskStack_Pkg is new Stacks ( disk_ptr_type ) ;

    type Pole_Type is
    record
        name : unbounded_String := null_unbounded_string ;
        disks : DiskStack_Pkg.Stk_Pkg.Vector ;
    end record ;

    type CallbackHandle_Type is abstract tagged null record ;
    type BasicCallbackHandle_Type is new CallbackHandle_Type with
      record
          null ;
      end record ;
    null_handle : aliased BasicCallbackHandle_Type ;
    type DiskMoveCallback is access
         procedure ( from : Pole_Type ;
                     to : Pole_Type ;
                     handle : CallbackHandle_Type'Class );

    type Data_Type is
    record
        initialized : boolean := false;
        name : ada.strings.unbounded.unbounded_string ;
        numdisks : integer ;
        movecount : integer ;
        SourcePole : aliased Pole_Type ;
        TargetPole : aliased Pole_Type ;
        BufferPole : aliased Pole_Type ;
        handle : access CallbackHandle_Type'class ;
    end record;

    ILLEGALMOVE : exception ;

    procedure ShowMoved( from : Pole_Type ;
                         to : Pole_Type ;
                         handle : CallbackHandle_Type'Class );
    function Create(name : string;
                    numdisks : integer := 3 ;
                    callback : DiskMoveCallback := ShowMoved'access ;
                    handle : access CallbackHandle_Type'class := null_handle'access )
                    return Data_Type ;
    procedure Solve( data : in out Data_Type );
    function Solved( data : Data_Type) return boolean ;
    procedure Move( data : in out Data_Type ;
                    from : in out pole_Type ;
                    to : in out pole_type );
    function MoveCount( data : Data_Type ) return integer ;
    procedure Show( data : Data_Type );
    procedure Show( pole : Pole_Type );

end hanoi.Model ;                                            -- [App/$]
