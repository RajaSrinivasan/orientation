with hanoi.Model ;            -- [App/$]
with hanoi.View ;             -- [App/$]
package body hanoi is         -- [App/$]

    procedure Initialize (gui : boolean := false) is
       model : aliased hanoi.model.Data_Type ;  -- [App/$]
    begin
        model := hanoi.Model.Create("Srinivasan") ;  -- [App/$]
        if gui
        then
           hanoi.View.Initialize (model'access) ;       -- [App/$]
        end if ;
    end Initialize ;
end hanoi ;                                         -- [App/$]
