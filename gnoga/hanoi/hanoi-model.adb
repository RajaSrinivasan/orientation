with Text_Io; use Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;
package body hanoi.Model is                               -- [App/$]

    MovedCallback : DiskMovecallback  ;
    function Create (name : string ;
                    numdisks : integer := 3 ;
                    callback : DiskMoveCallback := ShowMoved'access ;
                    handle : access CallbackHandle_Type'class := null_handle'access ) return Data_Type is
       obj : Data_Type ;
       diskptr : disk_ptr_type ;
    begin
       MovedCallback := callback ;
       obj.Initialized := true ;
       obj.name := ada.strings.unbounded.to_unbounded_string(name);
       obj.numdisks := numdisks ;
       obj.handle := handle ;
       obj.movecount := 0 ;
       obj.SourcePole.disks := DiskStack_Pkg.Create;
       obj.SourcePole.name := to_unbounded_string("A");
       obj.TargetPole.disks := DiskStack_Pkg.Create;
       obj.TargetPole.name := to_unbounded_string("C");
       obj.BufferPole.disks:= DiskStack_Pkg.Create;
       obj.BufferPole.name := to_unbounded_string("B");

       for disksize in reverse 1..numdisks
       loop
           diskptr := new disk_type ;
           diskptr.size := disksize ;
           DiskStack_Pkg.Push( obj.SourcePole.disks, diskptr );
       end loop;
       return obj ;
    end Create ;

    procedure ShowMoved( from : Pole_Type ;
                         to : Pole_Type ;
                         handle : CallbackHandle_Type'Class ) is
    begin
        Put("Moved disk size=");
        put(DiskStack_Pkg.Top(to.disks).size);
        put(" from "); put( to_string(from.name) );
        put(" to "); put( to_string(to.name) );
        new_line ;
    end ShowMoved ;

    procedure Move( data : in out Data_Type ;
                    from : in out pole_Type ;
                    to : in out pole_type ) is
        disk : disk_ptr_type ;
    begin
        if DiskStack_Pkg.Empty( from.disks )
        then
           raise ILLEGALMOVE ;
        end if ;
        disk := DiskStack_Pkg.Top(from.disks);
        if not DiskStack_Pkg.Empty( to.disks )
        then
            declare
               desttop : disk_ptr_type ;
            begin
               desttop := DiskStack_Pkg.Top( to.disks );
               if desttop.size < disk.size
               then
                  raise ILLEGALMOVE ;
               end if ;
            end ;
        end if ;
        DiskStack_Pkg.Pop( from.disks , disk ) ;
        DiskStack_Pkg.Push( to.disks , disk ) ;
        data.movecount := data.movecount + 1 ;
        MovedCallback( from , to , data.handle.all );
    end Move ;

    procedure Move( data : in out Data_Type ;
                    count : natural ;
                    source : in out Pole_Type ;
                    target : in out Pole_Type ;
                    buffer : in out Pole_Type) is
    begin

        if count > 1
        then
           Move( data , count-1 , source , buffer , target ) ;
           Move( data , 1 , source , target , buffer );
           Move( data , count-1 , buffer , target , source ) ;
        else
           Move( data , source , target );
        end if ;
    end Move ;

    function Solved( data : Data_Type) return boolean is
    begin
        if DiskStack_Pkg.Empty( data.sourcePole.disks ) and then
           DiskStack_Pkg.Empty( data.bufferPole.disks ) and then
           DiskStack_Pkg.Depth( data.targetPole.disks ) = data.numdisks
        then
           return true ;
        end if ;
        return false ;
    end Solved ;

    procedure Solve( data : in out Data_Type ) is
    begin
       Move( data , DiskStack_Pkg.Depth(data.sourcePole.disks) , data.sourcePole , data.targetPole , data.bufferPole );
    end Solve ;

    function MoveCount( data : Data_Type ) return integer is
    begin
      return data.movecount ;
    end MoveCount ;

    procedure Show( data : Data_Type ) is
    begin
        Put( "----------------------------------");
        Put( to_string(data.name));
        New_Line;
        Show( data.SourcePole );
        Show( data.BufferPole );
        Show( data.TargetPole );
    end Show ;

    procedure Show( pole : Pole_Type ) is
    begin
        Put(".....");
        Put(to_string(pole.name));
        Put(" has "); put( integer( DiskStack_Pkg.Depth(pole.disks))) ; put(" disks ");
        new_line ;
    end Show ;
end hanoi.Model ;                                        -- [App/$]
