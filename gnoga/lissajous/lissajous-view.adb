with Text_Io; use Text_Io ;
with Ada.Exceptions ;

with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with Ada.Numerics ;

with Gnoga.Application.Multi_Connect;
with Gnoga.Gui.Window;
with Gnoga.Gui.View;
with Gnoga.Gui.View.Grid ;
with Gnoga.Types ;
with Gnoga.Gui.Base;
with Gnoga.Gui.Element.Common;
with Gnoga.Gui.Element.Form;
with Gnoga.Gui.Document ;
with Gnoga.Gui.Element.Canvas.Context_2d.Plotting ;
with Gnoga.Types.Colors ;

package body lissajous.view is                                                -- [App/$]

    type coordinate_config_type is
    record
        amp_slider : Gnoga.Gui.Element.Element_Type ;
        freq_slider : Gnoga.Gui.Element.Element_Type ;
        phase_slider : Gnoga.Gui.Element.Element_Type ;
    end record ;

    type Data_Type is new Gnoga.Types.Connection_Data_Type with record
        Window      : Gnoga.Gui.Window.Pointer_To_Window_Class;
        Grid        : Gnoga.Gui.View.Grid.Grid_View_Type;
        View        : Gnoga.Gui.View.View_Type;

        xConfigPanel : Gnoga.Gui.View.View_Type ;
        xConfigForm  : Gnoga.Gui.Element.Form.Form_Type ;
        xconfig     : coordinate_config_type ;

        yConfigPanel : Gnoga.Gui.View.View_Type ;
        yConfigForm  : Gnoga.Gui.Element.Form.Form_Type ;
        yconfig     : coordinate_config_type ;

        Control     : Gnoga.Gui.Element.Form.Form_Type;
        Chart       : Gnoga.Gui.Element.Common.Button_Type;
        Quit        : Gnoga.Gui.Element.Common.Button_Type;
        model       : lissajous.Model.Data_Type ;                             -- [App/$]
        polargraphic : Gnoga.Gui.Element.Canvas.Context_2d.Plotting.Plot_Info ;    
   end record;
   type Data_Ptr_Type is access all Data_Type ;

   Default_Model : lissajous.Model.Data_Type ;                                -- [App/$]

    procedure On_Quit (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
        data : constant Data_Ptr_Type := Data_Ptr_Type (Object.Connection_Data);
        View : Gnoga.Gui.View.View_Type;
    begin -- On_Quit
        Data.Grid.Remove;
        View.Create (Parent => Data.Window.all);
        View.Put_Line (Message => "Bye");
        Data.Window.Close;
        Data.Window.Close_Connection;
    exception -- On_Quit
    when E : others =>
        Gnoga.Log (Message => "On_Quit: " & Ada.Exceptions.Exception_Information (E) );
    end On_Quit;
  procedure On_Chart (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
      Data : constant Data_Ptr_Type := Data_Ptr_Type (Object.Connection_Data);
      xspec,
      yspec : lissajous.Model.Coordinate_Spec_Type ;
      val : float ;
      Context : Gnoga.Gui.Element.Canvas.Context_2d.Context_2D_Type; 
      rect : Gnoga.Types.Rectangle_Type ;
   begin
       put_line("On_Chart");

       Context.Get_Drawing_Context_2D( Data.polargraphic );
       rect.X := 0; rect.Y :=0; rect.Height := 600 ; rect.Width := 600;
       Context.Clear_Rectangle(rect) ;

       val := float'value( Data.xConfig.amp_slider.Property("value") ) ;
       xspec.Amplitude := val/10.0 ;
       xspec.Frequency := float'value(Data.xConfig.freq_slider.Property("value")) / 20.0 ;
       val := 2.0 * Ada.Numerics.Pi * float'Value(Data.xConfig.phase_slider.Property("value")) / 360.0;
       xspec.Phase := val ;

       val := float'value( Data.yConfig.amp_slider.Property("value") ) ;
       yspec.Amplitude := val/10.0 ;
       yspec.Frequency := float'value(Data.yConfig.freq_slider.Property("value"))/20.0 ;
       val := 2.0 * Ada.Numerics.Pi * float'Value(Data.yConfig.phase_slider.Property("value")) / 360.0;
       yspec.Phase := val ;

       Data.polargraphic.X_Axis(1.0,10,Color=>Gnoga.Types.Colors.Red);
       Data.polargraphic.Y_Axis(1.0,10,Color=>Gnoga.Types.Colors.Red);

       lissajous.model.generate( data.model , xspec , yspec );
       declare
            points : Gnoga.Gui.Element.Canvas.Context_2d.Plotting.Point_List ( data.model.curve.all'range ) ;
       begin
         for p in points'range
         loop
             points(p).X := data.model.curve(p).x ;
             points(p).Y := data.model.curve(p).y ;
         end loop ;
         Data.polargraphic.graph( points , Gnoga.Types.Colors.Green );
        end ;
    exception -- On_Quit
        when E : others =>
            Gnoga.Log (Message => "On_Chart: " & Ada.Exceptions.Exception_Information (E) );
   end On_Chart ;

    procedure On_Connect (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
                        Connection  : access Gnoga.Application.Multi_Connect.Connection_Holder_Type)
    is
        Data : Data_Ptr_Type := new Data_Type ;
        rows   : Gnoga.Gui.View.Grid.Grid_Rows_Type (1..2,1..2) := (others => (Gnoga.Gui.View.Grid.Col,
                                                                               Gnoga.Gui.View.Grid.Col));
    begin -- On_Connect
        Main_Window.Connection_Data (Data => Data);
        Data.Window := Main_Window'Unchecked_Access;
        rows(2,2) := Gnoga.Gui.View.Grid.SPN ;
        Data.Grid.Create (Parent => Main_Window, Layout => rows );
        Data.Grid.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);

        Data.xConfigPanel.Create( Parent => Data.Grid.Panel(1,1).all );
        Data.xConfigForm.Create( Parent => Data.xConfigPanel);
        Data.xConfigForm.Put_Line("Amplitude");
        Data.xConfig.amp_slider.Create_from_HTML( Parent => Data.xConfigForm,
                                 ID => "xamp_slider" ,
                                 html => "<input type='range' min='1' max='60' class='slider'>" );
        Data.xConfigForm.Put_Line( "Frequency");
        Data.xConfig.freq_slider.Create_from_HTML( Parent => Data.xConfigForm,
                                 ID => "xfreq_slider" ,
                                 html => "<input type='range' min='1' max='120' class='slider'>" );
        Data.xConfigForm.Put_Line("Phase");
        Data.xConfig.phase_slider.Create_from_HTML( Parent => Data.xConfigForm,
                                 ID => "xphase_slider" ,
                                 html => "<input type='range' min='1' max='360' value='0' class='slider'>" );

        Data.yConfigPanel.Create( Parent => Data.Grid.Panel(1,2).all );
        Data.yConfigForm.Create( Parent => Data.yConfigPanel);
        Data.yConfigForm.Put_Line("Amplitude");
        Data.yConfig.amp_slider.Create_from_HTML( Parent => Data.yConfigForm,
                                 ID => "yamp_slider" ,
                                 html => "<input type='range' min='1' max='60' class='slider'>" );
        Data.yConfigForm.Put_Line( "Frequency");
        Data.yConfig.freq_slider.Create_from_HTML( Parent => Data.yConfigForm,
                                 ID => "yfreq_slider" ,
                                 html => "<input type='range' min='1' max='120' class='slider'>" );
        Data.yConfigForm.Put_Line("Phase");
        Data.yConfig.phase_slider.Create_from_HTML( Parent => Data.yConfigForm,
                                 ID => "yphase_slider" ,
                                 html => "<input type='range' min='1' max='360' value='0' class='slider'>" );

        Data.View.Create (Parent => Data.Grid.Panel (2, 1).all);
        Data.View.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);
        Data.View.Text_Alignment (Value => Gnoga.Gui.Element.Center);
        Data.Control.Create (Parent => Data.View);

        Data.Model := Default_Model ;
        -- Data.Control.Put_HTML (HTML => "<h1>" &
        --                                Ada.Strings.Unbounded.To_String( Data.Model.name ) &
        --                                "</h1>");

        Data.Chart.Create( parent => Data.Control , Content => "Chart" , ID => "chart_button");
        Data.Chart.On_Click_Handler( Handler => On_Chart'Access );
        Data.Control.Put_HTML (HTML => "<br />");
        Data.polargraphic.Create( parent => Data.Control , 
                                  Width => 600 , Height => 600 ,
                                  x_Min => -5.0 , X_Max => 5.0,
                                  Y_Min => -5.0 , Y_Max => 5.0 ,
                                  ID => "polargraphic")  ;
        Data.polargraphic.Background_Color( Enum => Gnoga.Types.Colors.Black);
        Data.Control.Put_HTML (HTML => "<br />");
        Data.Quit.Create (parent => Data.Control, Content => "Quit" , ID => "quit_button");
        Data.Quit.On_Click_Handler (Handler => On_Quit'Access);

    exception 
    when E : others =>
        Gnoga.Log (Message => "On_Connect: " & Ada.Exceptions.Exception_Information (E) );
    end On_Connect;

    procedure Initialize (model : access lissajous.Model.Data_Type ) is        -- [App/$]
    begin
        Gnoga.Application.Title (Name => "Application");
        Gnoga.Application.HTML_On_Close (HTML => "Bye. Please come again.");
        Gnoga.Application.Multi_Connect.Initialize;
        Gnoga.Application.Multi_Connect.On_Connect_Handler (Event => On_Connect'Access);
 
        Default_Model := model.all ;
        Text_Io.Put_Line("Model name " & to_string(Default_Model.name));

        Gnoga.Application.Multi_Connect.Message_Loop;
        
    exception
    when E : others =>
        Gnoga.Log (Message => Ada.Exceptions.Exception_Information (E) );
    end Initialize ;

end lissajous.view ;                                                             -- [App/$]
