with Ada.Strings.Unbounded ;
package lissajous.Model is                                       -- [App/$]

    type coordinate_spec_type is
    record
       amplitude : float ;
       frequency : float ;
       phase : float ;
    end record ;
    
    type point_type is
    record
       x : float ;
       y : float ;
    end record ;

    type curve_type is array (natural range <>) of point_type ;
    type curve_ptr_type is access curve_type ;

    type Data_Type is
    record
        initialized : boolean := false;
        name : ada.strings.unbounded.unbounded_string ;
        curve : curve_ptr_type ;
    end record;
    function Create(name : string) return Data_Type ;
    procedure Generate( data : in out data_type ;
                       xspec,
                       yspec : coordinate_spec_type ;
                       sampling : positive := 100 ;
                       multiplier : positive := 16 ) ;
end lissajous.Model ;                                            -- [App/$]
