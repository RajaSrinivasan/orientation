# Project: Lissajous

## Objective

Explore some options for the layout of the pages. By utilizing the grid construct, this project divides the page into 3 parts, As an application we explore lissajous figures. 
(Ref: http://mathworld.wolfram.com/LissajousCurve.html). Two of the cells in this grid configure each of the coordinates of the curve. The third grid actually draws the diagram.

## Design concepts


This project is also one of the instances of the gnoga template project. The model of the project is knowledgeable about the data. The view of the project just presents the data.

The code itself is generated utilizing the template as follows:

```
newutil -R .. -c ../template/gnogaapp.json lissajous
```

## Example

<img src="ScreenShot_lissajous.png">

By varying the Amplitude, Frequency and/or phase of the x and y coordinates, different curves can be charted.