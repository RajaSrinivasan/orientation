with lissajous.Model ;            -- [App/$]
with lissajous.View ;             -- [App/$]
package body lissajous is         -- [App/$]
    procedure Initialize is
       model : aliased lissajous.model.Data_Type ;  -- [App/$]
    begin
       model := lissajous.Model.Create("Srinivasan") ;  -- [App/$]
       lissajous.View.Initialize (model'access) ;       -- [App/$]
    end Initialize ;
end lissajous ;                                         -- [App/$]
