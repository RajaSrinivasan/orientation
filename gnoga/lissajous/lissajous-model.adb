with Text_Io; use Text_Io;
with Ada.Numerics ;
with Ada.Numerics.Elementary_Functions ;

with natural_numbers_pkg ;
package body lissajous.Model is                               -- [App/$]
    function Create (name : string ) return Data_Type is
       obj : Data_Type ;
    begin
       obj.Initialized := true ;
       obj.name := ada.strings.unbounded.to_unbounded_string(name);
       return obj ;
    end Create ;
    procedure Generate( data : in out data_type ;
                    xspec,
                    yspec : coordinate_spec_type ;
                    sampling : positive := 100 ;
                    multiplier : positive := 16 ) is
        use Ada.Numerics ;
        use Ada.Numerics.Elementary_Functions ;
        reqcycles : natural  ;
        deltat : float ;
        t : float := 0.0 ;
    begin
        reqcycles := natural_numbers_pkg.lcm( positive(xspec.frequency) + 1, positive(yspec.frequency)+1 )  ;
        deltat := 1.0 / float(sampling);
        data.curve := new curve_type( 1 .. reqcycles * sampling );
        for n in 1..reqcycles * sampling
        loop
            data.curve(n).x := xspec.amplitude * cos( t * xspec.frequency + xspec.Phase ) ;
            data.curve(n).y := yspec.amplitude * cos( t * yspec.frequency + yspec.Phase ) ;
            t := t + deltat;
        end loop;
    end Generate ;
end lissajous.Model ;                                        -- [App/$]
