with lissajous ;                                -- [App/$]
with lissajous.Model ;                          -- [App/$]
with lissajous.View ;                           -- [App/$]
procedure lissajous.main is                     -- [App/$]
begin
   lissajous.Initialize ;                       -- [App/$]
end lissajous.main ;                            -- [App/$]
