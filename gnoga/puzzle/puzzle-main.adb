with Text_IO; use Text_IO;
with Getcmdline ;

with puzzle ;
procedure puzzle.main is
   mypuz : puzzle.puzzle_type := puzzle.initialize(5) ;
   tiletomove : puzzle.tile_type ;
begin
   -- puzzle.show(mypuz);
   puzzle.shuffle(mypuz);
   puzzle.show(mypuz);
   loop
       tiletomove := integer'value(getcmdline.get_line("Tile to Move > ") );
       puzzle.move( mypuz , tiletomove );
       puzzle.show(mypuz);
   end loop ;
end puzzle.main;