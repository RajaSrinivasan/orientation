with Text_IO; use Text_IO ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
with ada.strings.unbounded ; use ada.strings.unbounded ;
with ctools ;
with GNAT.Random_Numbers ;

package body puzzle is

    puzgenerator : GNAT.Random_Numbers.generator ;

    function initialize( psize : size_type ) return puzzle_type is
       newpuz : puzzle_type (psize) ;
    begin
       for row in 1..psize
       loop
          for col in 1..psize
          loop
             newpuz.tiles(row,col) := (row - 1) * psize + col ;
          end loop ;
        end loop ;
        newpuz.tiles(psize,psize) := 0 ;
        newpuz.blank_row := psize ;
        newpuz.blank_col := psize ;
        GNAT.Random_Numbers.Reset( puzgenerator );
        return newpuz ;
    end initialize ;
    procedure initialize( puz : in out puzzle_type ) is
    begin
        puz := initialize( puz.psize );
    end initialize ;

    procedure shuffle( puz : in out puzzle_type ; rating : integer := 4 ) is
       moves : constant integer := puz.psize ** rating ;
       nextdir : float ;
    begin
       for move in 1 .. moves
       loop
          nextdir := GNAT.Random_Numbers.Random( puzgenerator );
          if nextdir in 0.0 .. 0.25
          then
              -- Put("North");
              if puz.blank_row > 1
              then
                puz.tiles( puz.blank_row , puz.blank_col ) := puz.tiles( puz.blank_row - 1 , puz.blank_col ); 
                puz.tiles( puz.blank_row - 1 , puz.blank_col ) := 0;
                puz.blank_row := puz.blank_row - 1 ;
              end if ;
          elsif nextdir in 0.25 .. 0.5
          then
              -- Put("South");
              if puz.blank_row < puz.psize 
              then
                puz.tiles( puz.blank_row , puz.blank_col ) := puz.tiles( puz.blank_row + 1 , puz.blank_col ); 
                puz.tiles( puz.blank_row + 1 , puz.blank_col ) := 0;
                puz.blank_row := puz.blank_row +  1 ;
              end if ;
          elsif nextdir in 0.5 .. 0.75
          then
              -- Put("East");
              if puz.blank_col < puz.psize 
              then
                puz.tiles( puz.blank_row , puz.blank_col ) := puz.tiles( puz.blank_row  , puz.blank_col + 1 ); 
                puz.tiles( puz.blank_row , puz.blank_col + 1 ) := 0;
                puz.blank_col := puz.blank_col +  1 ;
              end if ;              
          else
              -- Put("West");
              if puz.blank_col > 1
              then
                puz.tiles( puz.blank_row , puz.blank_col ) := puz.tiles( puz.blank_row  , puz.blank_col - 1 ); 
                puz.tiles( puz.blank_row , puz.blank_col - 1 ) := 0;
                puz.blank_col := puz.blank_col - 1 ;
              end if ;                  
          end if ;
       end loop ;
    end shuffle ;

    procedure show( puz : puzzle_type ) is
        procedure hr is
        begin
            Put("+");
            for col in 1..puz.psize
            loop
                Put("----+");
            end loop ;
            New_Line ;
        end hr ;
    begin
       hr ;
       for row in 1..puz.psize
       loop
          Put("|");
          for col in 1..puz.psize
          loop
              if puz.tiles(row,col) = 0
              then
                  Put("    ");
              else
                 Put( puz.tiles(row,col) , width => 4 );
              end if ;
              Put( "|");
          end loop ;
          New_Line ;
          hr ;
        end loop ;
    end show ;

    procedure move( puz : in out puzzle_type ; tile : tile_type ) is
    begin
        if tile in 1..puz.psize**2-1
        then
            if puz.blank_row > 1 and then puz.tiles( puz.blank_row - 1 , puz.blank_col ) = tile
            then
                puz.tiles( puz.blank_row - 1 , puz.blank_col ) := 0 ;
                puz.tiles( puz.blank_row , puz.blank_col ) := tile ;
                puz.blank_row := puz.blank_row - 1 ;
            elsif puz.blank_row < puz.psize and then puz.tiles( puz.blank_row + 1 , puz.blank_col ) = tile
            then
                puz.tiles( puz.blank_row + 1 , puz.blank_col ) := 0 ;
                puz.tiles( puz.blank_row , puz.blank_col ) := tile ;
                puz.blank_row := puz.blank_row + 1 ;
            elsif puz.blank_col > 1 and then puz.tiles( puz.blank_row , puz.blank_col - 1 ) = tile
            then
                puz.tiles( puz.blank_row , puz.blank_col - 1 ) := 0 ;
                puz.tiles( puz.blank_row , puz.blank_col ) := tile ;
                puz.blank_col := puz.blank_col - 1 ;
            elsif puz.blank_col < puz.psize and then puz.tiles( puz.blank_row , puz.blank_col + 1 ) = tile
            then
                puz.tiles( puz.blank_row , puz.blank_col + 1) := 0 ;
                puz.tiles( puz.blank_row , puz.blank_col ) := tile ;
                puz.blank_col := puz.blank_col + 1 ;
            end if ;
        end if ;
    exception
        when others =>
             null ;
    end move ;
end puzzle ;