package puzzle is
    subtype size_type is Integer range 3 .. 8;
    subtype tile_type is Integer range 0 .. 63;
    type tiles_type is array ( Integer range <> , Integer range <> ) 
                            of tile_type;
    type puzzle_type (psize : size_type) is
    record
        tiles : tiles_type ( 1..psize , 1..psize ) 
             := (others => (others => tile_type'first));
        blank_row, blank_col : Integer := 0 ;
    end record;
    function initialize( psize : size_type ) return puzzle_type ;
    procedure initialize( puz : in out puzzle_type );
    procedure shuffle( puz : in out puzzle_type ; rating : integer := 4 );
    procedure show( puz : puzzle_type );
    procedure move( puz : in out puzzle_type ; tile : tile_type );
end puzzle;