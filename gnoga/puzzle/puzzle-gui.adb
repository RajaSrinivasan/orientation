with Ada.Exceptions ;

with Gnoga.Application.Multi_Connect;
with Gnoga.Gui.Window;
with Gnoga.Gui.View;
with Gnoga.Types ;
with Gnoga.Gui.Base;
with Gnoga.Gui.Element.Common;
with Gnoga.Gui.Element.Form;

with Gnoga.Types.Colors ;
with Gnoga.Gui.Element.Form;
with Gnoga.Gui.View.Grid ;

with Text_Io; use Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

package body puzzle.gui is

   type guitiles_type is array (integer range <>,integer range <>) of Gnoga.Gui.Element.Common.Button_Type ;
   type puzgui_type (psize : size_type) is
    record
        tiles : guitiles_type (1..psize,1..psize) ;
    end record ;

   type App_Info is new Gnoga.Types.Connection_Data_Type with record
      Window      : Gnoga.Gui.Window.Pointer_To_Window_Class;
      Grid        : Gnoga.Gui.View.Grid.Grid_View_Type;
      View        : Gnoga.Gui.View.View_Type;
      Control     : Gnoga.Gui.Element.Form.Form_Type;

      NewPuzzle   : Gnoga.Gui.Element.Common.Button_Type;
      Quit        : Gnoga.Gui.Element.Common.Button_Type;
      rating_label : gnoga.gui.element.form.label_type ;
      rating      : Gnoga.Gui.Element.Form.Selection_Type;
      puz         : access puzzle_type := new puzzle_type(4) ;
      puzgui      : access puzgui_type := new puzgui_type(4) ;

   end record;
   type App_Ptr is access all App_Info ;

    procedure RepaintPuzzle( app : App_Ptr ) is
    begin
        for row in 1..App.puz.psize
        loop
            for column in 1..App.puz.psize
            loop
                if App.puz.tiles(row,column) /= 0
                then
                    App.puzgui.tiles(row,column).Text(integer'image(App.puz.tiles(row,column))) ;
                else
                    App.puzgui.tiles(row,column).Text(" ");
                end if;
            end loop ;
        end loop;
    end RepaintPuzzle ;

    procedure NewPuzzle_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
      App : constant App_Ptr := App_Ptr (Object.Connection_Data);
      rating : integer := integer'value(App.rating.value) ;
   begin 
      put_line("Rating " & integer'image(rating));
      puzzle.Initialize( App.puz.all );
      puzzle.shuffle( App.puz.all , rating );
      puzzle.show( App.puz.all );
      RepaintPuzzle( App ) ;
   end NewPuzzle_Click;

procedure Tile_Click (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
    Name : String := Object.ID;
    App : constant App_Ptr := App_Ptr (Object.Connection_Data) ;
    id : integer := integer'value(Name);
    row : integer := id / 256 ;
    col : integer := id - row * 256 ;
begin 
    puzzle.move( App.puz.all , App.puz.tiles(row,col) );
    RepaintPuzzle( App );
end Tile_Click;

   procedure On_Quit (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
      App : constant App_Ptr := App_Ptr (Object.Connection_Data);

      View : Gnoga.Gui.View.View_Type;
   begin -- On_Quit
        App.Grid.Remove;
        View.Create (Parent => App.Window.all);
        View.Put_Line (Message => "Bye");
      App.Window.Close;
      App.Window.Close_Connection;
   exception -- On_Quit
   when E : others =>
      Gnoga.Log (Message => "On_Quit: " & Ada.Exceptions.Exception_Information (E) );
   end On_Quit;

  procedure On_Connect (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
                       Connection  : access Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      App : App_Ptr := new App_Info;
      tile_size : constant := 100 ;
      function generate_id( r : integer ; c : integer ) return string is
      begin
          return integer'image(r*256 + c) ;
      end generate_id ;
   begin -- On_Connect
      Main_Window.Connection_Data (Data => App);
      App.Window := Main_Window'Unchecked_Access;
      App.Grid.Create (Parent => Main_Window, Layout => Gnoga.Gui.View.Grid.Horizontal_Split);
      App.Grid.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);
      App.View.Create (Parent => App.Grid.Panel (1, 1).all);
      App.View.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);
      App.View.Text_Alignment (Value => Gnoga.Gui.Element.Center);
      App.Control.Create (Parent => App.View);

      App.Rating.Create( form => App.Control , Name => "Rating" , ID => "rating_selector") ;
      for r in 4..8
      loop
          App.Rating.Add_Option( integer'image(r) , integer'image(r));
      end loop ;
      App.rating_label.Create( form => App.Control , label_for => App.Rating , Content => "Rating" , ID => "rs_label");
  
      App.NewPuzzle.Create (Parent => App.Control, Content => "New Puzzle" , ID => "new_puzzle");
      App.NewPuzzle.On_Click_Handler (Handler => NewPuzzle_Click'Access);

      App.Quit.Create (parent => App.Control, Content => "Quit" , ID => "quit_button");
      App.Quit.On_Click_Handler (Handler => On_Quit'Access);

    
      puzzle.Initialize( App.puz.all );
      puzzle.shuffle( App.puz.all );
    
      for row in 1..App.puz.psize
      loop
          for col in 1..App.puz.psize
          loop

            if App.puz.tiles(row,col) /= 0
            then
                App.puzgui.tiles (Row, Col).Create (Parent => App.View , 
                                                    Content => integer'image(App.puz.tiles(row,col)) , 
                                                    ID => generate_id(row,col) );
            else
                App.puzgui.tiles (Row, Col).Create (Parent => App.View , 
                                    Content => " " , 
                                    ID => generate_id(row,col) );
            end if ;

            App.puzgui.tiles(Row, Col).Vertical_Align (Value => Gnoga.Gui.Element.Middle);

            App.puzgui.tiles(Row, Col).Minimum_Width  (Value => tile_size);
            App.puzgui.tiles (Row, Col).Maximum_Width  (Value => tile_size);
            App.puzgui.tiles(Row, Col).Minimum_Height (Value => tile_size);
            App.puzgui.tiles (Row, Col).Maximum_Height (Value => tile_size);

            App.puzgui.tiles(Row, Col).Text_Alignment (Value => Gnoga.Gui.Element.Center);
            App.puzgui.tiles(Row, Col).Margin (Top => "1px", Right => "1px", Bottom => "1px", Left => "1px");
            App.puzgui.tiles(Row, Col).Border (Width => "thin");
            App.puzgui.tiles(Row, Col).Font (Height => "xx-large");
            App.puzgui.tiles(Row, Col).Background_Color (Enum => Gnoga.Types.Colors.Yellow);
            App.puzgui.tiles(Row, Col).On_Click_Handler (Handler => Tile_Click'Access);

          end loop ;
          App.View.Put_HTML (HTML => "<br />");
      end loop ;

   exception -- On_Connect
   when E : others =>
      Gnoga.Log (Message => "On_Connect: " & Ada.Exceptions.Exception_Information (E) );
   end On_Connect;

    procedure Initialize is
    begin
        Gnoga.Application.Title (Name => "Square Puzzle");
        Gnoga.Application.HTML_On_Close (HTML => "Puzzle ended. Bye.");
        Gnoga.Application.Multi_Connect.Initialize;
        Gnoga.Application.Multi_Connect.On_Connect_Handler (Event => On_Connect'Access);
        Gnoga.Application.Multi_Connect.Message_Loop;
        exception -- Tic_Tac_Toe.UI
    when E : others =>
        Gnoga.Log (Message => Ada.Exceptions.Exception_Information (E) );
    end Initialize ;

end puzzle.gui;