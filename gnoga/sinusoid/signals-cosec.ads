package signals.cosec is
    type Cosec_Signal_Type is new Signal_Type with
    record
       frequency : float ;
       phase : float := 0.0 ;
       amplitude : float := 1.0 ;
    end record ;
    function create( frequency : float ;
                     phase : float := 0.0 ;
                     amplitude : float := 1.0 ) return SignalPtr_Type ;
    function Sample( signal : Cosec_Signal_Type ;
                     at_time : float ) return float ;
end signals.cosec ;