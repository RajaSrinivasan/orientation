with Ada.Numerics ;
with Ada.Numerics.Elementary_Functions ;
package body signals.cosec is
    function Create( frequency : float ;
                     phase : float := 0.0 ;
                     amplitude : float := 1.0 ) return SignalPtr_Type is
        cosecsig : SignalPtr_Type := new Cosec_Signal_Type'( frequency => frequency , phase => phase , amplitude => amplitude );
    begin
        return cosecsig ;
    end Create ;

    function Sample( signal : Cosec_Signal_Type ;
                     at_time : float ) return float is
        sin_value : float ;
    begin
       sin_value := Ada.Numerics.Elementary_Functions.sin( (2.0 * Ada.Numerics.PI * signal.frequency) * at_time 
                                                           + signal.phase );
       if abs(sin_value) < 0.00001
       then
          return signal.amplitude ;
       else 
          return 1.0 / sin_value ;
       end if ;
    end Sample ;
end signals.cosec ;