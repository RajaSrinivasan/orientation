package body signals is
    procedure Sample( samples : out Discrete_Samples_Ptr_Type ;
                      signal : Signal_Type'Class ;
                      start_time : float ;
                      end_time : float ;
                      sampling_rate : positive ) is
        delta_time : float := 1.0 / float(sampling_rate);
        intervals : positive := positive((end_time - start_time)/delta_time) ;
    begin
        samples := new Discrete_Samples_Type( 0 .. intervals );
        for n in 0..intervals
        loop
            samples(n).at_time := start_time + float(n)*delta_time ;
            samples(n).value := Sample(signal,start_time + float(n) * delta_time);
        end loop ;
    end Sample ;
end signals ;