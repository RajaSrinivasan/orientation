with Ada.Numerics ;
with Ada.Numerics.Elementary_Functions ;
package body signals.sinusoid is
    function Create( frequency : float ;
                     phase : float := 0.0 ;
                     amplitude : float := 1.0 ) return SignalPtr_Type is
        sinsig : SignalPtr_Type := new Sinusoid_Signal_Type'( frequency => frequency , phase => phase , amplitude => amplitude );
    begin
        --sinsig.frequency := frequency ;
        --sinsig.phase := phase ;
        --sinsig.amplitude := amplitude ;
        return sinsig ;
    end Create ;

    function Sample( signal : Sinusoid_Signal_Type ;
                     at_time : float ) return float is
    begin
       return signal.amplitude * 
              Ada.Numerics.Elementary_Functions.sin( (2.0 * Ada.Numerics.PI * signal.frequency) * at_time +
                                                     signal.phase );
    end Sample ;
end signals.sinusoid ;