with ada.Text_Io; use Ada.Text_Io;
with Ada.Float_Text_Io ; use Ada.Float_Text_Io ;
with Ada.Command_Line ; use Ada.Command_Line ;

with Signals.Sinusoid ;
with Signals.Cosec ;

procedure signals.cli is
    sig : Signals.SignalPtr_Type := Signals.Sinusoid.Create( 1.0 );
    dsamples_ptr : Signals.Discrete_Samples_Ptr_Type ;
begin
    if Argument_Count < 1
    then
       Signals.Sample( dsamples_ptr , sig.all , 0.0 , 1.0 , 1000 );
    elsif Argument_Count < 2
    then
       Signals.Sample( dsamples_ptr , sig.all , 0.0 , 1.0 , Positive'Value(Argument(1)));
    else
       Signals.Sample( dsamples_ptr , sig.all , Float'Value( Argument(2)) 
                                              , Float'Value(Argument(3)) 
                                              , Positive'Value( Argument(1)));
    end if ;
    for n in dsamples_ptr'range
    loop
        Put( dsamples_ptr(n).at_time); Put(" , ");
        Put( dsamples_ptr(n).value);
        new_line ;
    end loop ;
end signals.cli ;