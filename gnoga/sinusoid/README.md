# Project : signals

## Objective :

      Beginning foundations of Digital Signal Processing
      Explore charting using Gnoga

## Usage :

```
../bin/signals

```

Point your browser to localost:8080 and enter the signal frequency and Sampling rate. 
The button "Chart" results in painting the Sin and Cosec functions of the specified 
parameters.

X axis is time and the charts are generated for 1 sec.

### Sample execution

The following is a screenshot that shows Sin and Cosec signals. The gui allows changing
the signal frequency and sampling frequency and regenerate the charts.

<img src="ScreenShot.png">

Replacing the text input for Frequency of the signal and the sampling rate with sliders:

<img src="ScreenShot_Sliders.png">

