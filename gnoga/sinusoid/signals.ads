package signals is
    type Signal_Type is abstract tagged limited
        record
            null ;
        end record ;
    type SignalPtr_Type is access all Signal_Type'Class ;

    type Sample_Type is
    record
       at_time : float ;
       value : float ;
    end record ;
    type Discrete_Samples_Type is array (integer range <>) of Sample_Type ;
    type Discrete_Samples_Ptr_Type is access all Discrete_Samples_Type ;
    function Sample( signal : Signal_Type ;
                     at_time : float ) return float is abstract ;
    procedure Sample( samples : out Discrete_Samples_Ptr_Type ;
                      signal : Signal_Type'Class ;
                      start_time : float ;
                      end_time : float ;
                      sampling_rate : positive ) ;
end signals ;