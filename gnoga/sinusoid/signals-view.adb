with Ada.Exceptions ;

with Gnoga.Application.Multi_Connect;
with Gnoga.Gui.Window;
with Gnoga.Gui.View;
with Gnoga.Types ;
with Gnoga.Gui.Base;
with Gnoga.Gui.Element.Common;
with Gnoga.Gui.Element.Form;
with Gnoga.Gui.Document ;
with Gnoga.Types.Colors ;

with Gnoga.Gui.View.Grid ;
with Gnoga.Gui.Element.Canvas.Context_2d.Plotting ;

with Text_Io; use Text_Io ;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;

with Ada.Numerics ;
with Ada.Numerics.Elementary_Functions ;

with signals.sinusoid ;
with signals.cosec ;

package body signals.view is
    type App_Info is new Gnoga.Types.Connection_Data_Type with record
        Window      : Gnoga.Gui.Window.Pointer_To_Window_Class;
        Grid        : Gnoga.Gui.View.Grid.Grid_View_Type;
        View        : Gnoga.Gui.View.View_Type;
        Control     : Gnoga.Gui.Element.Form.Form_Type;
        Frequency   : Gnoga.Gui.Element.Form.Text_Type ;
        freq_slider : Gnoga.Gui.Element.Element_Type ;
        freq_label : Gnoga.Gui.Element.Form.Label_Type ;
        Sampling    : Gnoga.Gui.Element.Form.Text_Type ;
        sampling_label : Gnoga.Gui.Element.Form.Label_Type ;
        Sampling_Slider : Gnoga.Gui.Element.Element_Type ;
        Chart : Gnoga.Gui.Element.Common.Button_Type;
        Quit  : Gnoga.Gui.Element.Common.Button_Type;
        chartgraphic : Gnoga.Gui.Element.Canvas.Context_2d.Plotting.Plot_Info ;
        polargraphic : Gnoga.Gui.Element.Canvas.Context_2d.Plotting.Plot_Info ;       
   end record;

   type App_Ptr is access all App_Info ;

   procedure On_Chart (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
      App : constant App_Ptr := App_Ptr (Object.Connection_Data);
      View : Gnoga.Gui.View.View_Type;
      sin : Signals.SignalPtr_Type  
                          := signals.sinusoid.Create( float'value( App.freq_slider.Property("value") ));
      csc : Signals.SignalPtr_Type
                    := signals.cosec.Create( float'value( App.freq_slider.Property("value")  ));

      samples : signals.Discrete_Samples_Ptr_Type ;
      cscsamples : signals.Discrete_Samples_Ptr_Type ;

      Context : Gnoga.Gui.Element.Canvas.Context_2d.Context_2D_Type; 
      rect : Gnoga.Types.Rectangle_Type ;
      --connid : Gnoga.Types.Connection_Id := App.freq_slider.Connection_Id ;
      --slider : Gnoga.Gui.Base.Base_Type ;
   begin 
      --Put("Frequency "); Put_Line( App.frequency.Value );
      --Put("Sampling ");  Put_Line( App.Sampling_slider.Property("value") );
      --Put("Frequency "); Put_Line(App.freq_slider.Property("value"));

      Context.Get_Drawing_Context_2D( App.chartgraphic );
      rect.X := 0; rect.Y :=0; rect.Height := 400 ; rect.Width := 800;
      Context.Clear_Rectangle(rect) ;
      App.chartgraphic.X_Axis(0.1,10,Color=>Gnoga.Types.Colors.Red);
      App.chartgraphic.Y_Axis(1.0,10,Color=>Gnoga.Types.Colors.Red);
      signals.Sample( samples , sin.all , 0.0 , 1.0 , positive'value( App.Sampling_Slider.Property("value")));
      declare
        points : Gnoga.Gui.Element.Canvas.Context_2d.Plotting.Point_List ( 1..samples.all'last) ;
      begin
         for p in points'range
         loop
             points(p).X := samples(p).at_time ;
             points(p).Y := samples(p).value ;
         end loop ;
         App.chartgraphic.graph( points, Gnoga.Types.Colors.Green );
      end ;

      signals.Sample( cscsamples , csc.all , 0.0 , 1.0 , positive'value( App.Sampling_Slider.Property("value")));
      declare
        points : Gnoga.Gui.Element.Canvas.Context_2d.Plotting.Point_List ( 1..cscsamples.all'last) ;
      begin
         for p in points'range
         loop
             points(p).X := cscsamples(p).at_time ;
             points(p).Y := cscsamples(p).value ;
         end loop ;
        App.chartgraphic.graph( points, Gnoga.Types.Colors.Cyan );
      end ;

      Context.Get_Drawing_Context_2D( App.polargraphic );
      rect.X := 0; rect.Y :=0; rect.Height := 300 ; rect.Width := 300;
      Context.Clear_Rectangle(rect) ;
      App.polargraphic.X_Axis(0.1,10,Color=>Gnoga.Types.Colors.Red);
      App.polargraphic.Y_Axis(0.1,10,Color=>Gnoga.Types.Colors.Red);

      declare
        points : Gnoga.Gui.Element.Canvas.Context_2d.Plotting.Point_List ( 1..samples.all'last) ;
        deltaangle : float := 2.0 * Ada.Numerics.Pi / float(positive'value( App.Sampling_Slider.Property("value")));
        angle : float := 0.0 ;
      begin
         for p in points'range
         loop
             points(p).X := samples(p).value * Ada.Numerics.Elementary_Functions.Cos(angle) ;
             points(p).Y := samples(p).value * Ada.Numerics.Elementary_Functions.Sin(angle) ;
             angle := angle + deltaangle ;
         end loop ;
        App.polargraphic.graph( points, Gnoga.Types.Colors.Cyan );
      end ;

   exception 
   when E : others =>
      Gnoga.Log (Message => "On_Chart: " & Ada.Exceptions.Exception_Information (E) );
   end On_Chart ;

   procedure On_Quit (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
      App : constant App_Ptr := App_Ptr (Object.Connection_Data);
      View : Gnoga.Gui.View.View_Type;
   begin -- On_Quit
        App.Grid.Remove;
        View.Create (Parent => App.Window.all);
        View.Put_Line (Message => "Bye");
      App.Window.Close;
      App.Window.Close_Connection;
   exception -- On_Quit
   when E : others =>
      Gnoga.Log (Message => "On_Quit: " & Ada.Exceptions.Exception_Information (E) );
   end On_Quit;
  procedure On_Connect (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
                       Connection  : access Gnoga.Application.Multi_Connect.Connection_Holder_Type)
   is
      App : App_Ptr := new App_Info;
   begin -- On_Connect
      Main_Window.Connection_Data (Data => App);
      App.Window := Main_Window'Unchecked_Access;
      App.Grid.Create (Parent => Main_Window, Layout => Gnoga.Gui.View.Grid.Horizontal_Split);
      App.Grid.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);

      App.View.Create (Parent => App.Grid.Panel (1, 1).all);
      App.View.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);
      App.View.Text_Alignment (Value => Gnoga.Gui.Element.Center);
      App.Control.Create (Parent => App.View);

      App.Control.Put_HTML (HTML => "<h1>Sinusoid Exploration</h1>");

     App.Freq_slider.Create_from_HTML( parent => App.Control , 
                                       ID => "freq_slider" ,
                                       html => "<input type='range' min='1' max='100' class='slider'>" );
 
      --App.Frequency.Create( form => App.Control , Name => "Frequency" , ID => "freq");
      App.freq_label.Create( form => App.Control , label_for => App.freq_slider , Content => "Frequency" , ID => "freq_label");
      App.Control.Put_HTML (HTML => "<br />");
      App.Sampling_slider.Create_from_HTML( parent => App.Control , 
                                       ID => "sampling_slider" ,
                                       html => "<input type='range' min='1' max='100' class='slider'>" );
      --App.Sampling.Create( form => App.Control , Name => "Sampling"  , ID => "sampling");
      App.sampling_label.Create( form => App.Control , label_for => App.Sampling_slider , Content => "Sampling" , ID => "sampling_label");
      App.Control.Put_HTML (HTML => "<br />");
      App.Chart.Create( parent => App.Control , Content => "Chart" , ID => "chart_button");
      App.Chart.On_Click_Handler( Handler => On_Chart'Access );

      App.chartgraphic.Create( parent => App.Control , Width => 600 , Height => 300 ,
                   x_Min=>0.0, X_Max=>1.0,
                   Y_Min => -5.0 , Y_Max  => 5.0 ,
                   ID => "chartgraphic")  ;
      App.chartgraphic.Background_Color( Enum => Gnoga.Types.Colors.Black);

      App.polargraphic.Create( parent => App.Control , Width => 300 , Height => 300 ,
                   x_Min=>-1.0, X_Max=>1.0,
                   Y_Min => -1.0 , Y_Max  => 1.0 ,
                   ID => "polargraphic")  ;
      App.polargraphic.Background_Color( Enum => Gnoga.Types.Colors.Black);

      App.Quit.Create (parent => App.Control, Content => "Quit" , ID => "quit_button");
      App.Quit.On_Click_Handler (Handler => On_Quit'Access);

   exception -- On_Connect
   when E : others =>
      Gnoga.Log (Message => "On_Connect: " & Ada.Exceptions.Exception_Information (E) );
   end On_Connect;

    procedure Initialize is
    begin
        Gnoga.Application.Title (Name => "Sinusoid");
        Gnoga.Application.HTML_On_Close (HTML => "Experiments ended. Bye.");
        Gnoga.Application.Multi_Connect.Initialize;
        Gnoga.Application.Multi_Connect.On_Connect_Handler (Event => On_Connect'Access);
        Gnoga.Application.Multi_Connect.Message_Loop;
        exception 
    when E : others =>
        Gnoga.Log (Message => Ada.Exceptions.Exception_Information (E) );
    end Initialize ;

end signals.view ;
