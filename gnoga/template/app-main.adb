with App ;                                -- [App/$]
with App.Model ;                          -- [App/$]
with App.View ;                           -- [App/$]
procedure App.main is                     -- [App/$]
begin
   App.Initialize ( true ) ;                       -- [App/$]
end App.main ;                            -- [App/$]