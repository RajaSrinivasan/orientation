with App ;                                -- [App/$]
with App.Model ;                          -- [App/$]
with App.View ;                           -- [App/$]
procedure App.cli is                     -- [App/$]
begin
   App.Initialize ;                       -- [App/$]
end App.cli ;                            -- [App/$]