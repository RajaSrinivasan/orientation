with Text_Io; use Text_Io ;
with Ada.Exceptions ;

with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;

with Gnoga.Application.Multi_Connect;
with Gnoga.Gui.Window;
with Gnoga.Gui.View;
with Gnoga.Gui.View.Grid ;
with Gnoga.Types ;
with Gnoga.Gui.Base;
with Gnoga.Gui.Element.Common;
with Gnoga.Gui.Element.Form;
with Gnoga.Gui.Document ;
with Gnoga.Types.Colors ;

package body App.view is                                                -- [App/$]

    type Data_Type is new Gnoga.Types.Connection_Data_Type with record
        Window      : Gnoga.Gui.Window.Pointer_To_Window_Class;
        Grid        : Gnoga.Gui.View.Grid.Grid_View_Type;
        View        : Gnoga.Gui.View.View_Type;
        Control     : Gnoga.Gui.Element.Form.Form_Type;
        Quit        : Gnoga.Gui.Element.Common.Button_Type;
        model       : App.Model.Data_Type ;                             -- [App/$]
   end record;
   type Data_Ptr_Type is access all Data_Type ;

   Default_Model : App.Model.Data_Type ;                                -- [App/$]

    procedure On_Quit (Object : in out Gnoga.Gui.Base.Base_Type'Class) is
        data : constant Data_Ptr_Type := Data_Ptr_Type (Object.Connection_Data);
        View : Gnoga.Gui.View.View_Type;
    begin -- On_Quit
        Data.Grid.Remove;
        View.Create (Parent => Data.Window.all);
        View.Put_Line (Message => "Bye");
        Data.Window.Close;
        Data.Window.Close_Connection;
    exception -- On_Quit
    when E : others =>
        Gnoga.Log (Message => "On_Quit: " & Ada.Exceptions.Exception_Information (E) );
    end On_Quit;

    procedure On_Connect (Main_Window : in out Gnoga.Gui.Window.Window_Type'Class;
                        Connection  : access Gnoga.Application.Multi_Connect.Connection_Holder_Type)
    is
        Data : Data_Ptr_Type := new Data_Type ;
    begin -- On_Connect
        Main_Window.Connection_Data (Data => Data);
        Data.Window := Main_Window'Unchecked_Access;
        Data.Grid.Create (Parent => Main_Window, Layout => Gnoga.Gui.View.Grid.Horizontal_Split);
        Data.Grid.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);

        Data.View.Create (Parent => Data.Grid.Panel (1, 1).all);
        Data.View.Background_Color (Enum => Gnoga.Types.Colors.Light_Blue);
        Data.View.Text_Alignment (Value => Gnoga.Gui.Element.Center);
        Data.Control.Create (Parent => Data.View);
        Data.Model := Default_Model ;
        Data.Control.Put_HTML (HTML => "<h1>" &
                                       Ada.Strings.Unbounded.To_String( Data.Model.name ) &
                                       "</h1>");

        Data.Quit.Create (parent => Data.Control, Content => "Quit" , ID => "quit_button");
        Data.Quit.On_Click_Handler (Handler => On_Quit'Access);

    exception 
    when E : others =>
        Gnoga.Log (Message => "On_Connect: " & Ada.Exceptions.Exception_Information (E) );
    end On_Connect;

    procedure Initialize (model : access App.Model.Data_Type ) is        -- [App/$]
    begin
        Gnoga.Application.Title (Name => "Application");
        Gnoga.Application.HTML_On_Close (HTML => "Bye. Please come again.");
        Gnoga.Application.Multi_Connect.Initialize;
        Gnoga.Application.Multi_Connect.On_Connect_Handler (Event => On_Connect'Access);
 
        Default_Model := model.all ;
        Text_Io.Put_Line("Model name " & to_string(Default_Model.name));

        Gnoga.Application.Multi_Connect.Message_Loop;
        
    exception
    when E : others =>
        Gnoga.Log (Message => Ada.Exceptions.Exception_Information (E) );
    end Initialize ;

end App.view ;                                                             -- [App/$]