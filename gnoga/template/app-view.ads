with App.Model ;                             -- [App/$]
package App.View is                          -- [App/$]
    procedure Initialize (model : access App.Model.Data_TYPE) ;    -- [App/$]
end App.View ;                               -- [App/$]