with App.Model ;            -- [App/$]
with App.View ;             -- [App/$]
package body App is         -- [App/$]
    procedure Initialize (gui : boolean := false) is
       model : aliased App.model.Data_Type ;  -- [App/$]
    begin
        model := App.Model.Create("Srinivasan") ;  -- [App/$]
        if gui
        then
           App.View.Initialize (model'access) ;       -- [App/$]
        end if ;
    end Initialize ;
end App ;                                         -- [App/$]