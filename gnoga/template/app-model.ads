with Ada.Strings.Unbounded ;
package App.Model is                                       -- [App/$]
    type Data_Type is
    record
        initialized : boolean := false;
        name : ada.strings.unbounded.unbounded_string ;
    end record;
    function Create(name : string) return Data_Type ;
end App.Model ;                                            -- [App/$]