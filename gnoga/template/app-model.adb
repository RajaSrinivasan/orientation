with Text_Io; use Text_Io;
package body App.Model is                               -- [App/$]
    function Create (name : string ) return Data_Type is
       obj : Data_Type ;
    begin
       obj.Initialized := true ;
       obj.name := ada.strings.unbounded.to_unbounded_string(name);
       return obj ;
    end Create ;
end App.Model ;                                        -- [App/$]