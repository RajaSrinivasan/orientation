with Interfaces ; use Interfaces ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with system.Storage_Elements ;
with Text_Io ; use Text_Io ;

with GNAT.Random_Numbers ;
with GNAT.SHA256 ;

with Hex ;
with ConfigParser ;
with base64 ;

package body password_Pkg is        -- [Pkg/$_Pkg]

   SALT_LENGTH : constant := 32 ;

   USERNAME_KEY : constant String := "username" ;
   PASSWORD_KEY : constant String := "password" ;
   USERNAMEB64_KEY : constant String := "usernameb64" ;

   USERNAMESALT_KEY : constant String := "usernamesalt" ;
   PASSWORDSALT_KEY : constant String := "passwordsalt" ;

   Gen : GNAT.Random_Numbers.Generator ;
   function Generate_Salt return String is
      SaltDigits : array(1..SALT_LENGTH) of Interfaces.Unsigned_8 ;
      Nextdig : Interfaces.Unsigned_32 ;
   begin
      GNAT.Random_Numbers.Reset(Gen) ;
      for D in 1..SALT_LENGTH
      loop
         Nextdig := GNAT.Random_Numbers.Random(Gen) ;
         SaltDigits(D) := Interfaces.Unsigned_8(Nextdig and 16#Ff#) ;
      end loop ;
      return Hex.Image( SaltDigits'Address , SALT_LENGTH ) ;
   end Generate_Salt ;

   function Digest( Salt : String ; Value : String ) return String is
      Csha256 : Gnat.SHA256.Context := Gnat.SHA256.Initial_Context ;
   begin
      GNAT.SHA256.Update(Csha256,Salt) ;
      GNAT.SHA256.Update(Csha256,Value) ;
      return GNAT.SHA256.Digest(Csha256) ;
   end Digest ;

   Config : ConfigParser.Config_Type ;

   procedure Load( Filename : String ) is
   begin
      Config := ConfigParser.Create ;
      ConfigParser.Read_File( Filename , Config ) ;
   end Load ;
   procedure Save (Filename : String) is
   begin
      ConfigParser.Write( Filename , Config ) ;
   end Save ;

   procedure Set( Username : String ;
                  Password : String ;
                  Application : String ) is
      UsernameSalt : constant String := Generate_Salt ;
      PasswordSalt : constant String := Generate_Salt ;
      Usernameenc : constant String := Digest( UsernameSalt , Username ) ;
      Passwordenc : constant String := Digest( Passwordsalt , Password ) ;
   begin
      if not ConfigParser.Has_Section( Config , Application )
      then
         ConfigParser.Add_Section( Config , Application ) ;
      end if ;
      ConfigParser.Add_Option( Config , Application , USERNAMESALT_KEY , UsernameSalt ) ;
      ConfigParser.Add_Option( Config , Application , USERNAME_KEY , Usernameenc ) ;
      declare
        b64inp : System.Storage_Elements.Storage_Array(1..username'length) ;
        for b64inp'address use username'address ;
      begin
          ConfigParser.Add_Option( Config , Application , USERNAMEB64_KEY , base64.Base64_Encode(b64inp)) ;
      end ;
      ConfigParser.Add_Option( Config , Application , PASSWORDSALT_KEY , PasswordSalt ) ;
      ConfigParser.Add_Option( Config , Application , PASSWORD_KEY , Passwordenc ) ;
   end Set ;

   function Verify( Username : String ;
                    Password : String ;
                    Application : String ) return Boolean is
      Usernamesalt : Ada.Strings.Unbounded.Unbounded_String ;
      Passwordsalt : Ada.Strings.Unbounded.Unbounded_String ;
      UsernameVal : Ada.Strings.Unbounded.Unbounded_String ;
      PasswordVal : Ada.Strings.Unbounded.Unbounded_String ;
   begin
      if not ConfigParser.Has_Option(Config , Application , USERNAME_KEY ) or
        not ConfigParser.Has_Option(Config , Application , USERNAMESALT_KEY ) or
        not ConfigParser.Has_Option(Config , Application , PASSWORD_KEY) or
        not ConfigParser.Has_Option(Config , Application , PASSWORDSALT_KEY )
      then
         return False ;
      end if ;
      Usernamesalt := To_Unbounded_String( ConfigParser.Get(Config, Application, USERNAMESALT_KEY ) ) ;
      Passwordsalt := To_Unbounded_String( ConfigParser.Get(Config, Application, PASSWORDSALT_KEY ) ) ;
      Usernameval := To_Unbounded_String( ConfigParser.Get(Config, Application, USERNAME_KEY ) ) ;
      Passwordval := To_Unbounded_String( ConfigParser.Get(Config, Application, PASSWORD_KEY ) ) ;
      declare
         Verusername : constant String := Digest( To_String(Usernamesalt) , Username ) ;
         Verpassword : constant String := Digest( To_String(Passwordsalt) , Password ) ;
      begin
         if Verusername = UsernameVal and
           Verpassword = PasswordVal
         then
            return True ;
         end if ;
      end ;
      return False ;
   end Verify ;

   procedure List( sec : ConfigParser.Sections_Pkg.Cursor ) is
     use System.Storage_Elements ;
     use ConfigParser ;
      secname : unbounded_string := Sections_Pkg.Key(sec) ;
      values : Dictionary_Pkg.Map := Sections_Pkg.Element(sec) ;
   begin
     Put("Section ");
     Put( to_string( secname )) ;
     Put_Line(" -----------------------");
     declare
       sec : string := to_string(secname) ;
       username : string := ConfigParser.get( config , sec , USERNAME_KEY );
     begin
       put(USERNAME_KEY); Put(" "); Put_Line( username );
       if ConfigParser.Has_Option( Config , to_string(secname) , USERNAMEB64_KEY )
       then
         declare
            usernameb64 : string := ConfigParser.get( config , sec , USERNAMEB64_KEY );
            usernameb64_dec : system.Storage_Elements.Storage_Array
                       := base64.base64_decode( usernameb64 );
          begin
           Put(" Username b64 "); put_line(usernameb64) ;
            declare
               usernameb64_decstr : string (1..usernameb64_dec'Length);
                          for usernameb64_decstr'address use usernameb64_dec'address ;
            begin
               put(" decoded "); Put_Line(usernameb64_decstr);
            end ;
         end ;
       end if ;
     end ;
   end List ;

   procedure List is
   begin
     ConfigParser.Sections_Pkg.Iterate( ConfigParser.Sections_Pkg.Map(Config) , List'Access) ;
   end List ;

end password_Pkg ;                  -- [Pkg/$_Pkg]
