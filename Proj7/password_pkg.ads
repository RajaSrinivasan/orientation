with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with password_cli ;                                          -- [cli/$_cli] ;
package password_pkg is                                       -- [pkg/$_pkg]


   procedure Load( Filename : String ) ;
   procedure Save ( Filename : String ) ;
   procedure Set( Username : String ;
                  Password : String ;
                  Application : String ) ;
   function Verify( Username : String ;
                    Password : String ;
                    Application : String ) return Boolean ;
   procedure List ;

end password_pkg ;                                            -- [pkg/$_pkg]
