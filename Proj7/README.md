# Project: password
## Objective
Explore: Text processing to analyze config (ini) files
         Message digest algorithm (eg SHA256) to store/verify encoded passwords
         Passwords can be verified but not retrieved

## Usage:

```
password V01 2017-12-10 08:08:31
Usage: password Command Line utility

 -v, --verbose[ARG]          Output extra verbose information
 -s, --save                  Save/update username/password
 -c, --check-password        Check the username/password
 -l, --list                  Check the username/password
 -p, --password-filename ARG Password filename
 -a, --application ARG       Application name


The password file name example:
[app]
password = 6c2b93f8f9fe26d991039d4fd30d76b83b3bc0b372cea9bd8b5875679a7def73
passwordsalt = E2231D15343BD83B116ABAE1789E983116F3C2147B113C8E1A497D94FB22BBE4
username = 17dccbf46464af37fc90e128ebf0593462ac25a1dbbf7f19c574ccd9153b7d60
usernameb64 = cnNyaW5pdmFzYW4=
usernamesalt = 5E850103398458B927CE045E717B53BA5551F2D1C828890DACA1A6F2CC1C3999
[myapp]
password = 00ed1165c825efe7c938c4664f28d80b23e0c7f65e3a324be1b5b3ecf8cf433d
passwordsalt = 0700030609020000070208010808010903050302020902040901010009030002
username = 6fe12b70ee226619b3b2500c8b616c81f6bc752c78d375ff7c5d1c66cc704ebd
usernamesalt = 0906060007060609060407070304030303070200030203030106070603090904
[myapp1]
password = 5eb44146f7028a6941875e77ddf2cd80c7366efb7e73e5cfc40cd33fc80da2b1
passwordsalt = 0100040509050006000707090706030409030008020404080501070709080505
username = c2eaf026416e360f1030b06dfaf290dcc001a00ebdb008564ce1c4b5f2c8c0b1
usernamesalt = 0504000402070108040705040305070605020802040302070509050606090405
[myapp2]
password = e85f3b9ea82bcab69a80c19aff4c80ac36ccc9415cec62d43643d090f1f58270
passwordsalt = 9439B76D88A13D8398E80EAC1FA84EBF6BB7EBD9FB418BF0ABCD847C34A8B90C
username = 1a92ed0544273ae6d3e5c6e069448079c7006fb3badefac841af149bec104a79
usernamesalt = 42E1BB7E95759C91C5C8D5399FBF9929B093D48DC0DED8831FCEE87668177BAD

In the above, neither the username nor the password is stored in clear text.
The username is stored in Base64 notation.
The message digest uses the SHA256 algorithm

```
