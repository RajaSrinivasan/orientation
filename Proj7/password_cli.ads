with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package password_cli is                                       -- [cli/$_cli]
   VERSION : string := "V01" ;
   NAME : String := "password" ;
   verbose : aliased boolean := false ;              -- Option:     -v or --verbose
   SaveOption : aliased Boolean := False ;
   VerifyOption : aliased Boolean := False ;
   ListOption : aliased Boolean := False ;

   Passwdfilename : aliased Gnat.Strings.String_Access ;
   Application : aliased Gnat.Strings.String_Access ;

   Username : Unbounded_string := Null_Unbounded_String ;
   Password : Unbounded_String := Null_Unbounded_String ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

end password_cli ;                                            -- [cli/$_cli]
