with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with GNAT.Source_Info ;

with password_cli ;                            -- [cli/$_cli]
with password_Pkg ;                            -- [Pkg/$_Pkg]
procedure password is                  -- [clitest/$]
begin
   password_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if password_cli.Verbose                     -- [cli/$_cli]
   then
      password_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;
   Password_Pkg.Load( Password_Cli.Passwdfilename.all ) ;
   if Password_Cli.SaveOption
   then
      Password_Pkg.Set( To_String(Password_Cli.Username) ,
                        To_String(Password_Cli.Password) ,
                        Password_Cli.Application.all ) ;
      Password_Pkg.Save ( Password_Cli.Passwdfilename.all ) ;
   end if ;
   if Password_Cli.VerifyOption
   then
      if Password_Pkg.Verify( To_String(Password_Cli.Username) ,
                           To_String(Password_Cli.Password) ,
                              Password_Cli.Application.all )
      then
         Put_Line("Password verified");
      else
         Put_Line("Password not verified");
      end if ;
   end if ;
   if Password_Cli.ListOption
   then
     Password_Pkg.List ;
   end if ;
end password ;                         -- [clitest/$]
