with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body password_cli is                          -- [cli/$_cli]

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
    begin
       put ("SwitchHandler " & Switch ) ;
       put (" Parameter " & Parameter) ;
       put (" Section " & Section);
       new_line ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
       GNAT.Command_Line.Set_Usage( Config ,
                                   Help => NAME & " " &
                                      VERSION & " " &
                                      Compilation_ISO_Date & " " &
                                      Compilation_Time ,
                                    Usage => "Command Line utility");
        GNAT.Command_Line.Define_Switch (Config,
                                         verbose'access ,
                                         Switch => "-v?",
                                         Long_Switch => "--verbose?",
                                         Help => "Output extra verbose information");
        GNAT.Command_Line.Define_Switch (Config,
                                         SaveOption'access ,
                                         Switch => "-s",
                                         Long_Switch => "--save",
                                         Help => "Save/update username/password");
        GNAT.Command_Line.Define_Switch (Config,
                                         VerifyOption'access ,
                                         Switch => "-c",
                                         Long_Switch => "--check-password",
                                         Help => "Check the username/password");
        GNAT.Command_Line.Define_Switch (Config,
                                         ListOption'access ,
                                         Switch => "-l",
                                         Long_Switch => "--list",
                                         Help => "Check the username/password");

        GNAT.Command_Line.Define_Switch (Config,
                                         output => Passwdfilename'access,
                                         Switch => "-p:",
                                         Long_Switch => "--password-filename:",
                                         Help => "Password filename");
        GNAT.Command_Line.Define_Switch (Config,
                                         output => application'access,
                                         Switch => "-a:",
                                         Long_Switch => "--application:",
                                         Help => "Application name");

        GNAT.Command_Line.Getopt(config,SwitchHandler'access);

        Username := To_Unbounded_String(GetNextArgument) ;
        Password := To_Unbounded_String(GetNextArgument) ;
    exception
       when GNAT.Command_Line.EXIT_FROM_COMMAND_LINE => null ;
    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GNAT.Command_Line.Get_Argument(Do_Expansion => False) ;
    end GetNextArgument ;

    procedure ShowCommandLineArguments is
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
       Put("SaveOption "); Put(Boolean'Image(SaveOption)) ; New_Line ;
       Put("VerifyOption "); Put(Boolean'Image(VerifyOption)); New_Line ;
       Put("ListOption ") ; Put(Boolean'Image(ListOption)); New_Line ;
       Put("Passwordfilename "); Put(Passwdfilename.all); New_Line ;
       Put("Username "); Put(To_String(Username)); New_Line ;
       Put("Password "); Put(To_String(Password)); New_Line ;
    end ShowCommandLineArguments ;

end password_cli ;                                   -- [cli/$_cli]
