# Project: dump

## Usage:
   dump file1 file2 file3

**Output:**

**-------File : file1 Size: n bytes---------------------------**

Offset | Ascii | Hexadecimal
------ | ----- | -----------
0000 | 0123456789012345 | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
0016 | abcdefghijklmnop | xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
0032 | |
0048 | |
0064 | |


any non printable characters to be printed as "."

## Extra Credit:

      dump -b Blocklength file1 file2 ....

      where blocklength - default 16 would be similar to Output
      shown above in the example

      dump -o file1 file2 ....

      will provide octal output instead of Hexadecimal
