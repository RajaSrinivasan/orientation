with AUnit ; use AUnit ;
with AUnit.Test_Cases ; use AUnit.Test_Cases ;

package numbers_Tests is                              -- [numbers/$]

    type numbers_Test is new Test_Cases.Test_Case with null record ;  -- [numbers/$]
    procedure Register_Tests( T : in out numbers_Test );              -- [numbers/$]
    function Name( T : numbers_Test ) return Message_String ;         -- [numbers/$]
    -- procedure Test_Prototype( T : in out Test_Cases.Test_Case'Class ) ;

end numbers_Tests ;                                    -- [numbers/$]
