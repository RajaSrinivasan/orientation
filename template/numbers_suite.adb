with numbers_tests ;                                                 -- [numbers/$]

package body numbers_suite is                                         -- [numbers/$]
    t : aliased numbers_Tests.numbers_Test ;                         -- [numbers/$]
    function suite return Access_Test_Suite is
        Ret : constant Access_Test_Suite := new Test_Suite;

    begin
        numbers_Tests.Register_Tests( T );                            -- [numbers/$]
        Ret.Add_Test( T'access );
        return Ret ;
    end suite ;
end numbers_suite ;                                                    -- [numbers/$]
