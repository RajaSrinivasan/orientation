#!/bin/bash
#
#  generate a new unit test framework
#
#  usage:
#    generate_utframework.sh <name of facility>
#
~/bin/newutil -R .. -c ../template/utconfig.json $1
