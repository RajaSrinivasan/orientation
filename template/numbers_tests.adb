
with Ada.Numerics.Elementary_Functions ;

with Ada.Text_Io ; use Ada.Text_Io ;
with Ada.Integer_Text_Io ; use Ada.Integer_Text_Io ;
with Ada.Float_Text_Io ; use Ada.Float_Text_Io ;

with AUnit.Assertions ; use AUnit.Assertions ;

with numbers ;                                                      -- [numbers/$]

package body numbers_tests is                                       -- [numbers/$]

    procedure Register_Tests( T : in out numbers_Test ) is          -- [numbers/$]
       use AUnit.Test_Cases.Registration ;
    begin
      null ;
      -- Register_Routine( T , Test_Prototype'access , "Test Prototype");
    end Register_Tests ;

    function Name( T : numbers_Test ) return Message_String is      -- [numbers/$]
    begin
        return Format("numbers Tests") ;                     -- [numbers/$]
    end Name ;

    -- procedure Test_Prototype( T : in out Test_Cases.Test_Case'Class ) is
    --begin
      --null ;
      --Assert( outlen = 4 , "1056 to 4 chars");
    --end Test_Prototype ;

end numbers_tests ;                                           -- [numbers/$]
