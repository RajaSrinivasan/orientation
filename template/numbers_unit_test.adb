with numbers_Tests ;                         -- [numbers/$]
with numbers_Suite ;                         -- [numbers/$]
with AUnit.Test_Suites ; use AUnit.Test_Suites ;
with AUnit.Run ;
with AUnit.Reporter.Text ;

procedure numbers_Unit_Test is               -- [numbers/$]
    procedure Run is new AUnit.Run.Test_Runner( numbers_Suite.Suite ); -- [numbers/$]
    Reporter : AUnit.Reporter.Text.Text_Reporter ;
begin
    Run(Reporter);
end numbers_Unit_Test ;                     -- [numbers/$]
