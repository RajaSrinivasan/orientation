with Ada.Text_Io; use Ada.Text_Io;
with gnat.command_line ;
with GNAT.Source_Info ; use GNAT.Source_Info ;

package body ipadr_cli is                          -- [cli/$_cli]

    package GCL renames GNAT.Command_Line ;

    procedure SwitchHandler
      (Switch    : String;
       Parameter : String;
       Section   : String) is
         use GNAT.Strings ;
    begin
      if Switch /= "-d"
      then
        return ;
      end if ;
      if dnsprimary = null
      then
        dnsprimary := new String( Parameter'Range );
        dnsprimary.all := Parameter ;
      elsif dnssecondary = null
      then
        dnssecondary := new String( Parameter'Range );
        dnssecondary.all := Parameter ;
      else
        Put("Too many DNS addresses");
        New_Line ;
      end if ;
      if Verbose
      then
         put ("SwitchHandler " & Switch ) ;
         put (" Parameter " & Parameter) ;
         put (" Section " & Section);
         new_line ;
      end if ;
    end SwitchHandler ;

    procedure ProcessCommandLine is
        Config : GNAT.Command_Line.Command_Line_Configuration;
    begin
        GCL.Set_Usage( Config ,
                       Help => NAME & " " &
                                      VERSION & " " &
                                      Compilation_ISO_Date & " " &
                                      Compilation_Time ,
                                      Usage => "Command Line utility");

        GCL.Define_Switch( Config , Output => verbose'access ,
                           Switch => "-v" , Help => "be verbose");
        GCL.Define_Prefix(Config,"-check");
        GCL.Define_Alias(Config,"-checkA","-checkm -checkg -checkp -checkb -checkr") ;
        GCL.Define_Switch (Config,
                           output => CheckMask'access,
                           Switch => "-checkm",
                           Help => "valid mask check");
       GCL.Define_Switch (Config,
                          output => CheckGateway'access,
                          Switch => "-checkg",
                          Help => "gateway check");
       GCL.Define_Switch (Config,
                          output => CheckPrivate'access,
                          Switch => "-checkp",
                          Help => "private address - routability - check");
       GCL.Define_Switch (Config,
                          output => CheckPrivate'access,
                          Switch => "-checkr",
                          Help => "reserved address check");
       GCL.Define_Switch (Config,
                          output => CheckBroadcast'access,
                          Switch => "-checkb",
                          Help => "broadcast address for subnet");

      GCL.Define_Prefix(Config,"-show");
      GCL.Define_Alias(Config,"-showA","-showb -showc -shows");
      GCL.Define_Switch(Config, output => ShowBroadcast'access,
                        Switch => "-showb" ,
                        Help => "show broadcast address");
      GCL.Define_Switch(Config, output => ShowClass'access,
                        Switch => "-showc" ,
                        Help => "show address class");
      GCL.Define_Switch(Config, output => ShowSubnet'access,
                        Switch => "-shows" ,
                        Help => "show subnet") ;

     GCL.Define_Switch( Config, output => netmask'access ,
                        Switch => "-m:" , Long_Switch => "--mask:" ,
                        Help => "address mask") ;
     GCL.Define_Switch( Config , output => gateway'access ,
                        Switch => "-g:" , Long_Switch => "--gateway:" ,
                        Help => "default gateway");
      GCL.Define_Switch( Config ,
                         Switch => "-d:" , Long_Switch => "--dns:" ,
                         Help => "DNS server") ;

      GCL.Getopt(config,SwitchHandler'access);

    end ProcessCommandLine;

    function GetNextArgument return String is
    begin
        return GCL.Get_Argument(Do_Expansion => True) ;
    end GetNextArgument ;

    procedure ShowCommandLineArguments is
      procedure Show(name : string ; val : boolean ) is
      begin
         Put(name) ; Put( " => ");
         put(boolean'image(val));
         new_line ;
       end Show ;
       procedure Show(name : string ; val : GNAT.strings.string_access) is
         use GNAT.Strings ;
       begin

         put(name) ;
         put(" => ");
         if val /= null
         then
            put(val.all) ;
         else
            put("unspecified");
         end if ;
         new_line ;
       end Show ;
    begin
       Put("Verbose ") ;
       Put(Boolean'Image( Verbose ) );
       New_Line ;
       Show("CheckMask" , CheckMask);
       Show("CheckGateway" , CheckGateway);
       Show("CheckBroadcast" , CheckBroadcast);
       Show("CheckPrivate" , CheckPrivate);
       Show("CheckReserved" , CheckReserved);
       Show("ShowClass" , ShowClass) ;
       Show("ShowSubnet" , ShowSubnet) ;
       Show("ShowBroadcast" , ShowBroadcast);
       Show("netmask" , netmask );
       Show("gateway" , gateway );
       Show("dnsprimary" , dnsprimary);
       Show("dnssecondary" , dnssecondary);
    end ShowCommandLineArguments ;

end ipadr_cli ;                                   -- [cli/$_cli]
