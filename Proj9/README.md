# Project: ipadr

## Objective

Explore: TCP/IP Addresses

## Usage:

```
../bin/ipadr -h
ipadr V01 2017-12-12 05:25:39
Usage: ipadr Command Line utility

 -v                be verbose
 -checkm           valid mask check
 -checkg           gateway check
 -checkp           private address - routability - check
 -checkr           reserved address check
 -checkb           broadcast address for subnet
 -showb            show broadcast address
 -showc            show address class
 -shows            show subnet
 -m, --mask ARG    address mask
 -g, --gateway ARG default gateway
 -d, --dns ARG     DNS server
 -checkA           Equivalent to -checkm -checkg -checkp -checkb -checkr
 -showA            Equivalent to -showb -showc -shows

```
