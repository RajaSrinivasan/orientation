with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

with ipadr_cli ;                                          -- [cli/$_cli] ;
package ipadr_pkg is                                       -- [pkg/$_pkg]
    type octet_type is range 0..255 ;
    for octet_type'Size use 8 ;

    type IP_Addr_Type is array (1..4) of octet_type ;
    for ip_addr_type'size use 32 ;
    pragma pack(IP_Addr_Type);

    type addr_bit_type is range 0..1;
    type IP_Addr_Bits_Type is array (1..32) of addr_bit_type ;
    for IP_Addr_Bits_Type'size use 32 ;
    pragma pack( IP_Addr_Bits_Type );

    Null_IP_Addr : constant IP_Addr_Type := (others => 0);
    type IP_Configuration_Type is
      record
          node : IP_Addr_Type := Null_IP_Addr;
          mask : IP_Addr_Type := Null_IP_Addr;
          gateway : IP_Addr_Type := Null_IP_Addr;
          dnsprimary : IP_Addr_Type := Null_IP_Addr;
          dnssecondary : IP_Addr_Type := Null_IP_Addr;
      end record ;

    ADDRESS_ERROR : exception ;
    MASK_ERROR : exception ;

    function Value(image : string) return IP_Addr_Type ;
    function Image(value : IP_Addr_Type) return string ;
    procedure CheckMask( ipadr : IP_Addr_Type;
                         status : out boolean ;
                         netbits : out integer );
    function CheckMask( ipadr : IP_Addr_Type ) return boolean ;

    -- Includes APIPA - Automatic Private IP Addressing
    --    Ref: https://www.lifewire.com/automatic-private-internet-protocol-addressing-816437
    -- Excludes Carrier Grade NAT
    --    Ref: https://en.wikipedia.org/wiki/Carrier-grade_NAT
    -- Excludes Reserved addresses (CheckReserved)
    function CheckPrivate( ipadr : IP_Addr_Type ) return boolean ;

    -- Includes reserved Addresses
    --   Ref: https://en.wikipedia.org/wiki/IPv4#Special-use_addresses
    function CheckReserved( ipadr : IP_Addr_Type ) return boolean ;

    procedure ShowSubnet( config : IP_Configuration_Type );
    procedure ShowClass( config : IP_Configuration_Type );
    procedure ShowBroadcast( config : IP_Configuration_Type );
    
    procedure Implementation (ipadr : string) ;
end ipadr_pkg ;                                            -- [pkg/$_pkg]
