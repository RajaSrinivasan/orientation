with ipadr_tests ;                                                 -- [numbers/$]

package body ipadr_suite is                                         -- [numbers/$]
    t : aliased ipadr_Tests.ipadr_Test ;                         -- [numbers/$]
    function suite return Access_Test_Suite is
        Ret : constant Access_Test_Suite := new Test_Suite;

    begin
        ipadr_Tests.Register_Tests( T );                            -- [numbers/$]
        Ret.Add_Test( T'access );
        return Ret ;
    end suite ;
end ipadr_suite ;                                                    -- [numbers/$]
