
with Ada.Numerics.Elementary_Functions ;

with Ada.Text_Io ; use Ada.Text_Io ;
with Ada.Integer_Text_Io ; use Ada.Integer_Text_Io ;
with Ada.Float_Text_Io ; use Ada.Float_Text_Io ;

with AUnit.Assertions ; use AUnit.Assertions ;
with ipadr_pkg ;        use ipadr_pkg ;

with ipadr ;                                                      -- [numbers/$]

package body ipadr_tests is                                       -- [numbers/$]

    procedure Register_Tests( T : in out ipadr_Test ) is          -- [numbers/$]
       use AUnit.Test_Cases.Registration ;
    begin
       Register_Routine( T , Test_Value'access , "Test Value ") ;
       Register_Routine( T , Test_Image'access , "Test Image ") ;
    end Register_Tests ;

    function Name( T : ipadr_Test ) return Message_String is      -- [numbers/$]
    begin
        return Format("ipadr Tests") ;                     -- [numbers/$]
    end Name ;

    procedure Test_Value_Failure_1 is
      adr : ipadr_pkg.IP_Addr_Type := ipadr_pkg.Value("      8.9.10.29") ;
    begin
    null ;
    end Test_Value_Failure_1 ;

    procedure Test_Value_Failure_2 is
      adr : ipadr_pkg.IP_Addr_Type := ipadr_pkg.Value("8.99999.10.29") ;
    begin
    null ;
    end Test_Value_Failure_2 ;

    procedure Test_Value_Failure_3 is
      adr : ipadr_pkg.IP_Addr_Type := ipadr_pkg.Value("8888.9.10.29") ;
    begin
    null ;
    end Test_Value_Failure_3  ;

    procedure Test_Value( T : in out Test_Cases.Test_Case'Class ) is
    begin
       Assert( ipadr_pkg.Value("0.0.0.0") = ipadr_pkg.Null_IP_Addr
                          , "Null IP Address conversion #1" );
       Assert( ipadr_pkg.Value("0.0.0.000") = ipadr_pkg.Null_IP_Addr
                         , "Null IP Address conversion #2" );
       Assert( ipadr_pkg.Value("0.0.000.0") = ipadr_pkg.Null_IP_Addr
                                            , "Null IP Address conversion #3" );
       Assert( ipadr_pkg.Value("0.0.008.08") /= ipadr_pkg.Null_IP_Addr
                                           , "Null IP Address conversion #4" );
       Assert( ipadr_pkg.Value("8.09.010.11") = (11,10,9,8)
                                           , "IP Address conversion #5" );
       Assert( ipadr_pkg.Value("255.255.255.064") = (64,255,255,255)
                                          , "IP Address conversion #6" );
       Assert( ipadr_pkg.Value("255.255.55.3") = (3,55,255,255)
                                          , "IP Address conversion #7" );
       Assert_Exception( Test_Value_Failure_1'access
                              , "IP Address conversion failure #1" );
       Assert_Exception( Test_Value_Failure_2'access
                             , "IP Address conversion failure #2" );
       Assert_Exception( Test_Value_Failure_3'access
                            , "IP Address conversion failure #3" );
   end Test_Value ;

    procedure Test_Image( T : in out Test_Cases.Test_Case'Class ) is
    begin
      Assert( Image((8,8,8,8)) = "8.8.8.8" , "IP_Address Image #1");
    end Test_Image ;

end ipadr_tests ;                                           -- [numbers/$]
