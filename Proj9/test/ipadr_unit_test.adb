with ipadr_Tests ;                         -- [numbers/$]
with ipadr_Suite ;                         -- [numbers/$]
with AUnit.Test_Suites ; use AUnit.Test_Suites ;
with AUnit.Run ;
with AUnit.Reporter.Text ;

procedure ipadr_Unit_Test is               -- [numbers/$]
    procedure Run is new AUnit.Run.Test_Runner( ipadr_Suite.Suite ); -- [numbers/$]
    Reporter : AUnit.Reporter.Text.Text_Reporter ;
begin
    Run(Reporter);
end ipadr_Unit_Test ;                     -- [numbers/$]
