with AUnit ; use AUnit ;
with AUnit.Test_Cases ; use AUnit.Test_Cases ;

package ipadr_Tests is                              -- [numbers/$]

    type ipadr_Test is new Test_Cases.Test_Case with null record ;  -- [numbers/$]
    procedure Register_Tests( T : in out ipadr_Test );              -- [numbers/$]
    function Name( T : ipadr_Test ) return Message_String ;         -- [numbers/$]

    procedure Test_Value( T : in out Test_Cases.Test_Case'Class ) ;
    procedure Test_Image( T : in out Test_Cases.Test_Case'Class ) ;

end ipadr_Tests ;                                    -- [numbers/$]
