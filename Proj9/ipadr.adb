with Ada.Text_Io; use Ada.Text_Io ;
with Ada.Exceptions ;
with GNAT.Source_Info ;
with GNAT.Command_Line ;

with ipadr_cli ;                            -- [cli/$_cli]
with ipadr_Pkg ;                            -- [Pkg/$_Pkg]
procedure ipadr is                  -- [clitest/$]
begin
   ipadr_cli.ProcessCommandLine ;           -- [cli/$_cli]
   if ipadr_cli.Verbose                     -- [cli/$_cli]
   then
      ipadr_cli.ShowCommandLineArguments ;        -- [cli/$_cli]
   end if ;
   ipadr_pkg.Implementation( ipadr_cli.GetNextArgument );
Exception
  when GNAT.Command_Line.EXIT_FROM_COMMAND_LINE =>
       null ;
end ipadr ;                         -- [clitest/$]
