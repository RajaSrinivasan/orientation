with Ada.Text_Io; use Ada.Text_Io;
with Ada.Integer_Text_Io; use Ada.Integer_Text_Io ;
with interfaces ;

with GNAT.Regpat ;
with GNAT.Sockets ;

package body ipadr_Pkg is        -- [Pkg/$_Pkg]

   package UBS renames Ada.Strings.Unbounded ;
   package RegPat renames GNAT.Regpat ;

   octets : string := "([0-9]{1,3})" ;
   cidrmask : string := "([0-9]{1,2})" ;

   octetp : RegPat.Pattern_Matcher
         := Regpat.Compile(octets);
   ipadrp : RegPat.Pattern_Matcher
         := RegPat.Compile( octets & "." & octets & "." & octets & "." & octets );
   cidr_ipadrp : RegPat.Pattern_Matcher
         := RegPat.Compile( octets & "." & octets & "." & octets & "." & octets & "/" & cidrmask );

   function Image( octet : octet_type ) return String is
     result : string := octet_type'image(octet) ;
   begin
     return result(2..result'last) ;
   end Image ;

   function Value( image : string ) return IP_Addr_Type is
     result : IP_Addr_Type := Null_IP_Addr ;
     matches : RegPat.Match_Array(0..RegPat.Paren_Count(ipadrp)) ;
   begin
     RegPat.Match( ipadrp , image , matches );
     if matches(0).first /= 0 and then image = image(matches(0).first .. matches(0).last)
     then
       result(4) := octet_type'value( image(matches(1).first .. matches(1).last ));
       result(3) := octet_type'value( image(matches(2).first .. matches(2).last ));
       result(2) := octet_type'value( image(matches(3).first .. matches(3).last ));
       result(1) := octet_type'value( image(matches(4).first .. matches(4).last ));
       return result ;
     end if ;
     raise ADDRESS_ERROR ;
   end Value ;

   function Image( value : IP_Addr_Type) return string is
   begin
        return Image(value(4)) & "." &
               image(value(3)) & "." &
               image(value(2)) & "." &
               image(value(1)) ;
   end Image ;

   mask_octets : array (1..8) of octet_type
   := (2#1111_1111#,
       2#1111_1110#,
       2#1111_1100#,
       2#1111_1000#,
       2#1111_0000#,
       2#1110_0000#,
       2#1100_0000#,
       2#1000_0000#);
   function CheckMask( ipadr : IP_Addr_Type ) return boolean is
     status : boolean ;
     netbits : integer ;
   begin
     CheckMask( ipadr , status , netbits );
     return status ;
   end CheckMask ;

   procedure CheckMask( ipadr : IP_Addr_Type;
                        status : out boolean ;
                        netbits : out integer ) is
   -- function CheckMask( ipadr : IP_Addr_Type ) return boolean is
      ipadrbits : IP_Addr_Bits_Type ;
      for ipadrbits'address use ipadr'address ;
      hostnobits : integer := 0 ;
   begin
      netbits := 0 ;
      status := false ;
      for bitno in IP_Addr_Bits_Type'range
      loop
        -- put("Bit "); put(integer'image(bitno)); new_line ;
        if ipadrbits(bitno) = 0
        then
          hostnobits := hostnobits + 1 ;
        else
          exit ;
        end if ;
      end loop;
      if hostnobits = 0
      then
        return ;
      end if ;
      for bitno in 1+hostnobits..32
      loop
        if ipadrbits(bitno) = 0
        then
          return ;
        end if ;
      end loop;
      netbits := 32 - hostnobits ;
      status := true ;
   end CheckMask ;

   function CheckPrivate( ipadr : IP_Addr_Type ) return boolean is
   begin
       if ipadr(4) = 10
       then
          return true ;
        end if ;
        if ipadr(4) = 192 and ipadr(3) = 168
        then
          return true ;
        end if ;
        if ipadr(4) = 172
        then
          if ipadr(3) >= 16 and ipadr(3) <= 31
          then
            return true ;
          end if ;
        end if ;
        if ipadr(4) = 169 and ipadr(3) = 254
        then
          return true ;
        end if ;
        return false ;
   end CheckPrivate ;

   current_network : IP_Addr_Type := (others => 0);
   loopback_address : IP_Addr_Type := (127 , 0 , 0 , 0) ;
   broadcast_address : IP_Addr_Type := (others => 255) ;

   function CheckReserved( ipadr : IP_Addr_Type ) return boolean is
   begin
      if ipadr = current_network or
         ipadr = loopback_address or
         ipadr = broadcast_address
      then
        return true ;
      end if ;
      return false ;
   end CheckReserved ;

   procedure ExtractAddress( addr : out IP_Addr_Type;
                             image : string ;
                             matches : RegPat.Match_Array ;
                             first : integer := 1 ;
                             last : integer := 4 ) is
   begin
     addr(4) := octet_type'value( image(matches(1).first .. matches(1).last ));
     addr(3) := octet_type'value( image(matches(2).first .. matches(2).last ));
     addr(2) := octet_type'value( image(matches(3).first .. matches(3).last ));
     addr(1) := octet_type'value( image(matches(4).first .. matches(4).last ));
   end ;

   procedure SetMask( mask : out IP_Addr_Type ; masksize : integer ) is
     maskbits : IP_Addr_Bits_Type ;
     for maskbits'address use mask'address ;
   begin
       if masksize not in 1..31
       then
          raise ADDRESS_ERROR;
       end if ;
       maskbits := (others => 0);
       for b in 32-masksize+1..32
       loop
          maskbits(b) := 1 ;
       end loop;
   end SetMask ;

   procedure ParseSubject( spec : string ; config : out IP_Configuration_Type ) is
     result : IP_Configuration_Type ;
     cidrmatches : RegPat.Match_Array(0..RegPat.Paren_Count(cidr_ipadrp)) ;
     matches : RegPat.Match_Array(0..RegPat.Paren_Count(ipadrp)) ;
     netmasksize : integer ;
   begin
      RegPat.Match( cidr_ipadrp , spec , cidrmatches) ;
      if cidrmatches(0).first /= 0
      and then spec = spec(cidrmatches(0).first .. cidrmatches(0).last)
      then
         ExtractAddress(result.node , spec , cidrmatches);
         netmasksize := integer'value(spec(cidrmatches(5).first .. cidrmatches(5).last)) ;
         SetMask( result.mask , netmasksize );

         config := result ;
         return ;
      end if ;

      RegPat.Match( ipadrp , spec , matches );
      if matches(0).first /= 0
         and then spec = spec(matches(0).first .. matches(0).last)
      then
        ExtractAddress( result.node , spec, matches );
        config := result ;
        return ;
      end if ;
      raise ADDRESS_ERROR ;
   end ParseSubject ;

   procedure ShowSubnet( config : IP_Configuration_Type ) is
     use Interfaces ;
     adr : interfaces.unsigned_32 ;
     for adr'address use config.node'address ;
     mask : interfaces.unsigned_32 ;
     for mask'address use config.mask'address ;
     subnet : interfaces.unsigned_32 ;
     subnetadr : IP_Addr_Type ;
     for subnetadr'address use subnet'address ;
   begin
      subnet := adr ;
      subnet := subnet and mask ;
      Put(" Subnet ");
      Put( Image(subnetadr) );
      New_line ;
   end ShowSubnet ;
   procedure ShowClass( config : IP_Configuration_Type ) is
     adr : IP_Addr_Bits_Type ;
     for adr'address use config.node'address ;
   begin
     if adr(1) = 0
     then
       Put(" Address Class A");
     elsif adr(2) = 0
     then
       Put(" Address Class B");
     elsif adr(3) = 0
     then
       Put(" Address Class C");
     elsif adr(4) = 0
     then
       Put(" Address Class D (multicast)");
     else
       Put(" Address Class E (reserved)");
     end if ;
     New_Line ;
   end ShowClass ;
   procedure ShowBroadcast( config : IP_Configuration_Type ) is
     use interfaces ;
     mask : interfaces.unsigned_32 ;
     for mask'address use config.mask'address ;
     bcastadr : IP_Addr_Type ;
     bcastadruns : interfaces.unsigned_32 ;
     for bcastadruns'address use bcastadr'address ;
   begin
     bcastadr := config.node ;
     bcastadruns := bcastadruns and mask ;
     bcastadruns := bcastadruns or not mask ;
     Put(" Broadcast address ");
     Put( Image(bcastadr) );
     New_Line ;
   end ShowBroadcast ;

   procedure Implementation (ipadr : string ) is
     use GNAT.strings ;
     config : IP_Configuration_Type ;
     candidate : IP_Addr_Type renames config.node;
   begin
      ParseSubject( ipadr , config );
      if ipadr_cli.Verbose
      then
        Put("IP Address :"); Put(Image(config.node)); New_Line;
      end if ;
      if ipadr_cli.netmask.all'Length /= 0
      then
        if config.mask /= Null_IP_Addr
        then
          Put_Line("Mask implied by CIDR notation!");
          raise ADDRESS_ERROR ;
        else
          declare
            status : boolean ;
            netbits : integer ;
            maskvalue : IP_Addr_Type ;
          begin
            maskvalue := Value(ipadr_cli.netmask.all) ;
            CheckMask( maskvalue , status , netbits);
            if not status
            then
              raise ADDRESS_ERROR ;
            end if ;
            config.mask := maskvalue ;
          end ;
        end if ;
      end if ;

      if ipadr_cli.gateway.all'Length /= 0
      then
        config.gateway := Value( ipadr_cli.gateway.all );
      end if ;
      if ipadr_cli.dnsprimary /= null
      then
        config.dnsprimary := Value( ipadr_cli.dnsprimary.all );
      end if ;
      if ipadr_cli.dnssecondary /= null
      then
        config.dnssecondary := Value( ipadr_cli.dnssecondary.all );
      end if ;
      if ipadr_cli.Verbose
      then
        Put( " Net mask set to ");
        Put(Image(config.mask));
        New_Line ;
        Put( " Gateway set to ");
        Put(Image(config.gateway));
        new_line ;
        Put( " Primary DNS server set to ");
        Put( Image(config.dnsprimary));
        New_Line ;
        Put( " Secondary DNS server set to ");
        Put( Image(config.dnssecondary));
        New_Line ;
      end if ;

      if ipadr_cli.CheckMask
      then
          if CheckMask(candidate)
          then
            Put(" Valid Mask");
          else
            Put(" Not a valid mask");
          end if ;
          New_line ;
      end if ;
      if ipadr_cli.CheckPrivate
      then
        if CheckPrivate(candidate)
        then
          Put(" is a private address");
        else
          Put(" is not a private address") ;
        end if ;
      end if ;
      if ipadr_cli.CheckReserved
      then
        if CheckReserved(candidate)
        then
          Put(" is a reserved address");
        else
          Put(" is not a reserved address");
        end if ;
      end if ;
      if ipadr_cli.ShowClass
      then
        ShowClass( config );
      end if ;
      if ipadr_cli.ShowSubnet
      then
        ShowSubnet( config );
      end if ;
      if ipadr_cli.ShowBroadcast
      then
        ShowBroadcast( config );
      end if ;
   end Implementation ;

end ipadr_Pkg ;                  -- [Pkg/$_Pkg]
