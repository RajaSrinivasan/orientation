with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package ipadr_cli is                                       -- [cli/$_cli]

   VERSION : string := "V01" ;
   NAME : String := "ipadr" ;

   verbose : aliased boolean := false ;              -- Option:     -v or --verbose

   CheckMask,
   CheckGateway,
   CheckPrivate,
   CheckReserved,
   CheckBroadcast : aliased boolean ;


   ShowSubnet,
   ShowBroadcast,
   ShowClass : aliased boolean ;

   netmask,
   gateway,
   dnsprimary,
   dnssecondary : aliased gnat.strings.string_access ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

end ipadr_cli ;                                            -- [cli/$_cli]
